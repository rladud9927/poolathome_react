package kr.co.poolathome.core.vo.email;

import kr.co.poolathome.core.utils.DateUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class SendEmail implements Serializable {

    private static final long serialVersionUID = -4171581644681636911L;

    private String to;
    private String subject;
    private String body;

    @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_TIME_UNIT)
    private LocalDateTime sendTime;
}
