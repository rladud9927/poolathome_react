package kr.co.poolathome.core.domain.board.qna;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

@NoArgsConstructor(staticName = "getInstance")
public class QnaPredicate {

  private static final QQna Q_ENTITY = QQna.qna;

  private BooleanBuilder builder = new BooleanBuilder();

  public Predicate values() {
    return builder.getValue() == null ? builder.and(Q_ENTITY.id.isNotNull()) : builder.getValue();
  }

  public QnaPredicate startDate(final LocalDateTime startDate) {

    if (startDate != null) {
      builder.and(Q_ENTITY.createdDate.goe(startDate));
    }
    return this;
  }

  public QnaPredicate endDate(final LocalDateTime endDate) {

    if (endDate != null) {
      builder.and(Q_ENTITY.createdDate.loe(endDate));
    }
    return this;
  }

  public QnaPredicate search(String value) {

    if (!StringUtils.isEmpty(value)) {
      value = value.trim();

//      builder.and(Q_ENTITY.title.containsIgnoreCase(value)
//          .or(Q_ENTITY.content.containsIgnoreCase(value)));
    }
    return this;
  }

  public QnaPredicate active(final Boolean isActive) {

    if (isActive != null) {
      builder.and(Q_ENTITY.active.eq(isActive));
    }
    return this;
  }

  public QnaPredicate delete(final Boolean delete) {

    if (delete != null) {
      builder.and(Q_ENTITY.delete.eq(delete));
    }
    return this;
  }
}
