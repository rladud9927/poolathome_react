package kr.co.poolathome.core.domain.email;


import kr.co.poolathome.core.domain.user.User;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmailHistoryRepository extends
    PagingAndSortingRepository<EmailHistory, Long>,
    QuerydslPredicateExecutor<EmailHistory>,
    EmailHistoryRepositoryCustom {

    List<EmailHistory> findAllByTypeAndSendAddressAndRelativeUserOrderBySendTimeDesc(EmailHistory.Type type, String sendAddress, User user);
}
