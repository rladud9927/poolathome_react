package kr.co.poolathome.core.service.send.email;

import kr.co.poolathome.core.model.Filter;
import kr.co.poolathome.core.service.DomainService;
import kr.co.poolathome.core.domain.email.EmailHistory;
import org.springframework.data.domain.Page;

public interface EmailHistoryService extends DomainService<EmailHistory, Long> {

    // Page
    Page<EmailHistory> page(Filter filter, EmailHistory.Type type);
}
