package kr.co.poolathome.core.model.resbody.board;

import kr.co.poolathome.core.model.BaseResponseBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

@Schema(description = "자주하는 질문")
@Relation(value = "faq", collectionRelation = "faqs")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FaqResBody extends BaseResponseBody {

  private static final long serialVersionUID = 8780396155649692018L;

  @Schema(description = "ID")
  private Long id;

  @Schema(description = "질문")
  private String question;

  @Schema(description = "답변")
  private String answer;

  @Schema(description = "카테고리")
  private String category;
}
