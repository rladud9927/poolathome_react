package kr.co.poolathome.core.service.map.kakao;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class KakaoAddress implements Serializable {

  private static final long serialVersionUID = -8919141363092244458L;

  @SerializedName("address_name")
  private String addressName;

  @SerializedName("b_code")
  private String bCode;

  @SerializedName("h_code")
  private String hCode;

  @SerializedName("main_address_no")
  private String mainAddressNo;

  @SerializedName("mountain_yn")
  private String mountainYn;

  @SerializedName("region_1depth_name")
  private String region1depthName;

  @SerializedName("region_2depth_name")
  private String region2depthName;

  @SerializedName("region_3depth_h_name")
  private String region3depthHName;

  @SerializedName("sub_address_no")
  private String subAddressNo;

  @SerializedName("x")
  private BigDecimal x;

  @SerializedName("y")
  private BigDecimal y;
}
