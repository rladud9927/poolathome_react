package kr.co.poolathome.core.service.map.juso;


import kr.co.poolathome.core.model.map.juso.JusoEntity;

import java.util.concurrent.Future;

public interface JusoService {

    Future<JusoEntity> geJusoInfo(String keyword, int currentPage, int countPerPage) throws InterruptedException;
    Future<JusoEntity> geJusoInfo(String keyword, int currentPage) throws InterruptedException;
}
