package kr.co.poolathome.core.config.okhttp;

import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OkHttpConfig {

  @Bean
  public OkHttpClient getOkHttpClient() {
    return new OkHttpClient();
  }
}
