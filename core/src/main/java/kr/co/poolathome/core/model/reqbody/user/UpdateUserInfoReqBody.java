package kr.co.poolathome.core.model.reqbody.user;

import kr.co.poolathome.core.model.BaseRequestBody;
import kr.co.poolathome.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Pattern;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateUserInfoReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -1567352085715794401L;

  @Pattern(regexp = ValidUtils.PATTERN_FULLNAME)
  @Schema(description = "fullName", example = "홍길동", minLength = 2, maxLength = 10)
  private String fullName;
  @Pattern(regexp = ValidUtils.PATTERN_PASSWORD_NEW)
  @Schema(description = "password", example = "abcd1234", minLength = 6, maxLength = 30)
  private String password;
  @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
  @Schema(description = "mobile", example = "01011112222", minLength = 10, maxLength = 11)
  private String mobile;

  @Schema(description = "Email receive", example = "test@poolathome.co.kr", maxLength = 255)
  private boolean emailRcv;
  @Schema(description = "sms receive", example = "sms", maxLength = 255)
  private boolean smsRcv;
}
