package kr.co.poolathome.core.model.reqbody.account;

import kr.co.poolathome.core.model.BaseRequestBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CheckAccount extends BaseRequestBody {

  private static final long serialVersionUID = -5909977159805769094L;

  private String email;
  private String mobile;
}
