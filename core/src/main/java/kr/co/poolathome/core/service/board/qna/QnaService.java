package kr.co.poolathome.core.service.board.qna;

import kr.co.poolathome.core.service.DomainService;
import kr.co.poolathome.core.domain.board.qna.Qna;

public interface QnaService extends DomainService<Qna, Long> {

}
