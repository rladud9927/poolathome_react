package kr.co.poolathome.core.model.map.juso;

import lombok.Data;

import java.util.List;

@Data
public class JusoEntity {

    private JusoEntityCommon common;

    private List<Juso> jusos;
}
