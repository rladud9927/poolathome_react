package kr.co.poolathome.core.service.document;

import kr.co.poolathome.core.model.Filter;
import kr.co.poolathome.core.service.DomainService;
// // import kr.co.poolathome.core.domain.b2b.ServiceType;
import kr.co.poolathome.core.domain.document.Document;
import org.springframework.data.domain.Page;

import java.util.Locale;

public interface DocumentService extends DomainService<Document, Long> {

    Page<Document> page(Locale locale, Filter filter, Document.Type type);

    Document latestDoc(Document.Type type);
}
