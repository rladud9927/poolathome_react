package kr.co.poolathome.core.service.authenticode;

import kr.co.poolathome.core.model.AuthentiCodeResBody;
import kr.co.poolathome.core.domain.authenticode.Authenticode;

/**
 * 인증 토큰
 * (문자 인증, 이메일 인증 등 사용)
 */
public interface AuthenticodeService {

    String tokenCode(Authenticode tokenStorage);

    AuthentiCodeResBody confirmByMobile(String token, String mobile);

    AuthentiCodeResBody getAuthCode(String token);
    AuthentiCodeResBody confirmByEmail(String token);

    String generateToken();

    void deleteByToken(String token);
    void deletePreviousValue(String mobile);

}
