package kr.co.poolathome.core.service.file;

import kr.co.poolathome.core.domain.file.FileEntity;
import kr.co.poolathome.core.service.DomainService;

public interface FileEntityService extends DomainService<FileEntity, Long> {
}
