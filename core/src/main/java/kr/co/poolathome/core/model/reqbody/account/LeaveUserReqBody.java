package kr.co.poolathome.core.model.reqbody.account;

import kr.co.poolathome.core.model.BaseRequestBody;
import kr.co.poolathome.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Pattern;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LeaveUserReqBody extends BaseRequestBody {

    private static final long serialVersionUID = -3422607201168752231L;

    @Pattern(regexp = ValidUtils.PATTERN_PASSWORD_NEW)
    @Schema(description = "password", example = "abcd1234", minLength = 6, maxLength = 30)
    private String password;
//    private boolean leaveTerms; // 탈퇴약관 동의 유/무

    @Schema(description = "leave reason", example = "reason", minLength = 2, maxLength = 255)
    private String leaveReason; // 탈퇴사유
}
