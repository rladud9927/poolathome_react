package kr.co.poolathome.core.domain.wywiwyg;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WysiwygFileRepository extends
        PagingAndSortingRepository<WysiwygFile, Long>,
        QuerydslPredicateExecutor<WysiwygFile> {

    WysiwygFile findByUrl(String url);
}
