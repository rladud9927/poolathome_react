package kr.co.poolathome.core.model.reqbody.user;

import kr.co.poolathome.core.model.BaseRequestBody;
import kr.co.poolathome.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Pattern;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChangePasswordReqBody extends BaseRequestBody {

    private static final long serialVersionUID = -3469914382919401016L;



    @Schema(description = "old password", example = "abcd", minLength = 6, maxLength = 30)
    private String password;
    @Schema(description = "new password", example = "abcd1234", minLength = 6, maxLength = 30)
    private String newPassword;
    @Schema(description = "confirm new password", example = "abcd1234", minLength = 6, maxLength = 30)
    private String confirmNewPassword;
    @Pattern(regexp = ValidUtils.PATTERN_EMAIL)
    @Schema(description = "email", example = "test@poolathome.co.kr", minLength = 3, maxLength = 255)
    private String email;

    @Schema(description = "phone", example = "01011112222", minLength = 10, maxLength = 12)
    private String phone;



}
