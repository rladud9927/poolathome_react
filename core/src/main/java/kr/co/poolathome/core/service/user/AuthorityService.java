package kr.co.poolathome.core.service.user;

import kr.co.poolathome.core.domain.user.Authority;

import java.util.List;

public interface AuthorityService {

  List<Authority> list();

  List<Authority> list(Authority.Role... roles);
}
