package kr.co.poolathome.core.service.board.post;

import kr.co.poolathome.core.domain.board.post.Post;
import kr.co.poolathome.core.model.Filter;
import kr.co.poolathome.core.model.admin.PostInfoResBody;
import kr.co.poolathome.core.service.DomainService;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Locale;

public interface PostService extends DomainService<Post, Long> {

  Page<Post> page(Locale locale, Filter filter, Post.Type type);

  Page<Post> page(Locale locale, Filter filter, Post.Type type, Boolean isActive);

  Page<Post> page(Locale locale, Filter filter, Post.Type type, Boolean isActive, Long idCategory);

  List<PostInfoResBody> list(String query);
}
