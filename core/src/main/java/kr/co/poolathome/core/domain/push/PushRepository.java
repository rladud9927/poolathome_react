package kr.co.poolathome.core.domain.push;


import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PushRepository extends
        PagingAndSortingRepository<Push, Long>,
        QuerydslPredicateExecutor<Push> {
}
