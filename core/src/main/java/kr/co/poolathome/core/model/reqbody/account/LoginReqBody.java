package kr.co.poolathome.core.model.reqbody.account;

import kr.co.poolathome.core.model.BaseRequestBody;
import kr.co.poolathome.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginReqBody extends BaseRequestBody {

    private static final long serialVersionUID = -6594245922268862985L;

    @Email
    @Schema(description = "이메일 주소", example = "test@poolathome.co.kr", minLength = 3, maxLength = 255)
    private String email;

    @Pattern(regexp = ValidUtils.PATTERN_PASSWORD_NEW)
    @Schema(description = "비밀번호", example = "abcd1234", minLength = 6, maxLength = 30)
    private String password;
}
