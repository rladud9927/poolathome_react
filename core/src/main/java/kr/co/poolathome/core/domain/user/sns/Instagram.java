package kr.co.poolathome.core.domain.user.sns;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kr.co.poolathome.core.domain.user.Gender;
import kr.co.poolathome.core.utils.StringUtils;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Instagram implements Serializable {
    private static final long serialVersionUID = 2302782338661875354L;

    private String id;
    private String email;
    private String name;
    private String picture;
    private String gender;

    public void setGender(String gender) {
        if (StringUtils.isNotEmpty(gender)) {
            if (Objects.equals(gender, "male")) {
                this.setGenderEnum(Gender.MALE);
            } else if (Objects.equals(gender, "female")) {
                this.setGenderEnum(Gender.FEMALE);
            }
        }
        this.gender = gender;
    }

    @JsonIgnore
    private Gender genderEnum;

    public static Instagram of(String id, String email, String name, String pictureMap, String gender) {
        Instagram instagram = new Instagram();
        instagram.setId(id);
        instagram.setName(name);

        if (StringUtils.isNotEmpty(email)) {
            instagram.setEmail(email);
        }

        if (pictureMap != null) {
            instagram.setPicture(pictureMap);
        }

        if (StringUtils.isNotEmpty(gender)) {
            if (Objects.equals(gender, "male")) {
                instagram.setGenderEnum(Gender.MALE);
            } else if (Objects.equals(gender, "female")) {
                instagram.setGenderEnum(Gender.FEMALE);
            }
        }

        return instagram;
    }

    public Map<String, String> toMap() {
        Map<String, String> result = new HashMap<>();
        result.put("access_token", "sign_up");
        result.put("id", this.id);
        result.put("name", this.name);
        result.put("email", StringUtils.isEmpty(this.email) ? null : this.email);
        result.put("thumbnail", StringUtils.isEmpty(this.picture) ? null : this.picture);
        result.put("gender", this.genderEnum == null ? null : this.genderEnum.name());
        return result;
    }
}
