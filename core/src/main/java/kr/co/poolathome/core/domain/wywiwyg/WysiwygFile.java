package kr.co.poolathome.core.domain.wywiwyg;

import kr.co.poolathome.core.domain.AbstractEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class WysiwygFile extends AbstractEntity<Long> {

    private static final long serialVersionUID = 8500368358046956545L;

    @Id
    @GeneratedValue
    @Column(unique = true)
    private Long id;  // PK

    @NonNull
    @Column(unique = true)
    private String url;

    @NonNull
    @Column
    private String filename;

    @NonNull
    @Column
    private Long fileSize;

    @Override
    public void delete() {

    }

    @Override
    public void lazy() {

    }

}
