package kr.co.poolathome.core.service.map.kakao;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class KakaoRoadAddress implements Serializable {
  private static final long serialVersionUID = -6665579891400548386L;

  @SerializedName("address_name")
  private String addressName;

  @SerializedName("building_name")
  private String buildingName;

  @SerializedName("main_building_no")
  private String mainBuildingNo;

  @SerializedName("region_1depth_name")
  private String region1depthName;

  @SerializedName("region_2depth_name")
  private String region2depthName;

  @SerializedName("region_3depth_name")
  private String region3depthName;

  @SerializedName("sub_building_no")
  private String subBuildingNo;

  @SerializedName("underground_yn")
  private String undergroundYN;

  @SerializedName("zone_no")
  private String zoneNo;

  @SerializedName("x")
  private BigDecimal x;

  @SerializedName("y")
  private BigDecimal y;
}
