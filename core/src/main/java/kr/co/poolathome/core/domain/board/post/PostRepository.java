package kr.co.poolathome.core.domain.board.post;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.time.LocalDateTime;
import java.util.List;

public interface PostRepository extends
        JpaRepository<Post, Long>,
        QuerydslPredicateExecutor<Post>,
        PostRepositoryCustom {

    @Query("select p.id from Post p where p.regTime < ?1 and p.type = ?2 order by p.regTime desc")
    List<Long> previous(LocalDateTime regTime, Post.Type type, Pageable pageable);

    @Query("select p.id from Post p where p.regTime > ?1 and p.type = ?2 order by p.regTime asc")
    List<Long> next(LocalDateTime regTime, Post.Type type, Pageable pageable);

    @Modifying
    @Query(value = "update Post p set p.pageView = p.pageView + 1 where p.id = :id")
    void pageView(Long id);
}
