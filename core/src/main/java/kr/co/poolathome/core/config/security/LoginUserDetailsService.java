package kr.co.poolathome.core.config.security;

import kr.co.poolathome.core.domain.user.User;
import kr.co.poolathome.core.domain.user.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class LoginUserDetailsService implements UserDetailsService {

  private final UserRepository userRepository;

  public LoginUserDetailsService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

    try {
      log.debug(">>>> loadUserByUsername ::: {}", email);
      User user = userRepository.findByEmail(email).orElse(null);
      log.debug(">>>> loadUserByUsername ::: {}", user);
      if(user != null && user.getUserDetails() != null) {
        log.debug("user.getUserDetails() : {}", user.getUserDetails());
        log.debug("isSeller : {}", user.getUserDetails().isSeller());
      }

      return userRepository.findByEmail(email).map(User::getUserDetails).orElseThrow(RuntimeException::new);
    } catch (NullPointerException e) {
      throw new UsernameNotFoundException(null);
    }
  }
}