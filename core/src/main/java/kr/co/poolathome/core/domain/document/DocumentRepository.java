package kr.co.poolathome.core.domain.document;


import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DocumentRepository extends
    PagingAndSortingRepository<Document, Long>,
    QuerydslPredicateExecutor<Document>,
    DocumentRepositoryCustom {

  @Query("from Document d where d.type = ?1")
  List<Document> lastType(Document.Type type, Pageable pageable);

}
