package kr.co.poolathome.core.domain.user;

import java.util.List;

public interface AuthorityRepositoryCustom {

  List<Authority> listByRoles(Authority.Role... roles);
}
