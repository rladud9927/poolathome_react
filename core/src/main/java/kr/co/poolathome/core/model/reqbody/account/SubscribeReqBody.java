package kr.co.poolathome.core.model.reqbody.account;

import kr.co.poolathome.core.model.BaseRequestBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class SubscribeReqBody extends BaseRequestBody {
  private static final long serialVersionUID = -6369426921362956820L;

//  public enum Type {
//    EMAIL, SMS, KAKAO, PUSH
//  }
//
//  @Schema(description = "수신 동의 유형", example = "EMAIL")
//  private Type type;

  @Schema(description = "Toggle (true:동의, false:비동의)", example = "true")
  private Boolean active;
}
