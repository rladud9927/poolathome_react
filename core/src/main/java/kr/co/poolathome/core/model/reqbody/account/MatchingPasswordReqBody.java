package kr.co.poolathome.core.model.reqbody.account;

import kr.co.poolathome.core.model.BaseRequestBody;
import kr.co.poolathome.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Pattern;

@Setter
@Getter
@ToString
public class MatchingPasswordReqBody extends BaseRequestBody {

  private static final long serialVersionUID = -967274294480844947L;

  @Pattern(regexp = ValidUtils.PATTERN_PASSWORD_NEW)
  @Schema(description = "password", example = "abcd1234", minLength = 6, maxLength = 30)
  private String password;
}
