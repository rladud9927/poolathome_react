package kr.co.poolathome.core.model.resbody.maincontent;//package kr.co.poolathome.core.model.resbody.maincontent;
//
//import kr.co.poolathome.core.model.BaseResponseBody;
//import lombok.*;
//import org.springframework.hateoas.server.core.Relation;
//
//@Relation(value = "mainContent", collectionRelation = "mainContents")
//@Getter
//@Setter
//@ToString
//@NoArgsConstructor
//@AllArgsConstructor
//@Builder
//public class MainContentResBody extends BaseResponseBody {
//
//
//  private static final long serialVersionUID = 8298953652736429028L;
//  private Long id;
//  private String image;
//  private String title;
//  private String subtitle;
//  private String description;
//  private String link1;
//  private String link2;
//  private String link3;
//
//}
