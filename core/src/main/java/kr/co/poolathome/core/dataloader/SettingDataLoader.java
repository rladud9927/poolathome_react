package kr.co.poolathome.core.dataloader;//package kr.co.poolathome.core.dataloader;
//
//import kr.co.poolathome.core.domain.International.InterText;
//import kr.co.poolathome.core.domain.International.InternationalMode;
//import kr.co.poolathome.core.domain.commerce.buyerlevel.BuyerLevel;
//import kr.co.poolathome.core.domain.commerce.buyerlevel.BuyerLevelRepository;
//import kr.co.poolathome.core.domain.setting.AppSetting;
//import kr.co.poolathome.core.domain.setting.AppSettingRepository;
//import kr.co.poolathome.core.utils.LocaleUtils;
//import com.google.common.collect.Lists;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
//import java.math.BigDecimal;
//
///**
// * 초기 환결 데이터 값 설정
// */
//@Slf4j
//@Component
//@Order(0) // 낮을수록 먼저
//public class SettingDataLoader implements CommandLineRunner {
//
//    @Value("${app.international.mode}")
//    private boolean internationalMode;
//
//    @Value("${app.international.locale}")
//    private String internationalLocale;
//
//    @Autowired
//    private AppSettingRepository appSettingRepository;
//
//    @Autowired
//    private BuyerLevelRepository buyerLevelRepository;
//
//    @Override
//    public void run(String... args) {
//
//        // init
//        if (Lists.newArrayList(appSettingRepository.findAll()).size() == 0) {
//            AppSetting setting = new AppSetting();
//            setting.setInternational(internationalMode);
//            setting.setDefaultLocale(LocaleUtils.toLocale(internationalLocale));
//
//            InternationalMode im = new InternationalMode();
//            im.setKoKr(true);
//            im.setZhCn(true);
//            im.setZhTw(true);
//            im.setJaJp(true);
//            im.setEnUs(true);
//            setting.setInternationalMode(im);
//
//            appSettingRepository.save(setting);
//        }
//
//
//        if (Lists.newArrayList(buyerLevelRepository.findAll()).size() == 0){
//            BuyerLevel buyerLevel1 = new BuyerLevel();
//            buyerLevel1.setName(getName("Level 1"));
//            buyerLevel1.setId(1L);
//            buyerLevel1.setPreMonPerfStart(BigDecimal.valueOf(0));
//            buyerLevel1.setPreMonPerfEnd(BigDecimal.valueOf(29));
//            buyerLevel1.setSaveRate(BigDecimal.valueOf(0.5));
//            buyerLevel1.setTotalBenefits(BigDecimal.valueOf(0));
//            buyerLevelRepository.save(buyerLevel1);
//
//            BuyerLevel buyerLevel2 = new BuyerLevel();
//            buyerLevel2.setName(getName("Level 2"));
//            buyerLevel2.setCouponInfo(getName("월 5% 할인쿠폰 2장 지급"));
//            buyerLevel2.setId(2L);
//            buyerLevel2.setPreMonPerfStart(BigDecimal.valueOf(30));
//            buyerLevel2.setPreMonPerfEnd(BigDecimal.valueOf(49));
//            buyerLevel2.setSaveRate(BigDecimal.valueOf(1));
//            buyerLevel2.setTotalBenefits(BigDecimal.valueOf(180000));
//            buyerLevelRepository.save(buyerLevel2);
//
//            BuyerLevel buyerLevel3 = new BuyerLevel();
//            buyerLevel3.setName(getName("Level 3"));
//            buyerLevel3.setCouponInfo(getName("월 10% 할인쿠폰 4장 지급"));
//            buyerLevel3.setId(3L);
//            buyerLevel3.setPreMonPerfStart(BigDecimal.valueOf(50));
//            buyerLevel3.setPreMonPerfEnd(BigDecimal.valueOf(99));
//            buyerLevel3.setSaveRate(BigDecimal.valueOf(3));
//            buyerLevel3.setTotalBenefits(BigDecimal.valueOf(600000));
//            buyerLevelRepository.save(buyerLevel3);
//
//            BuyerLevel buyerLevel4 = new BuyerLevel();
//            buyerLevel4.setName(getName("Level 4"));
//            buyerLevel4.setCouponInfo(getName("월 15% 할인쿠폰 2장 지급\n월 10% 할인쿠폰 2장 지급"));
//            buyerLevel4.setId(4L);
//            buyerLevel4.setPreMonPerfStart(BigDecimal.valueOf(100));
//            buyerLevel4.setPreMonPerfEnd(BigDecimal.valueOf(149));
//            buyerLevel4.setSaveRate(BigDecimal.valueOf(5));
//            buyerLevel4.setTotalBenefits(BigDecimal.valueOf(1560000));
//            buyerLevelRepository.save(buyerLevel4);
//
//            BuyerLevel buyerLevel5 = new BuyerLevel();
//            buyerLevel5.setName(getName("Level 5"));
//            buyerLevel5.setCouponInfo(getName("월 15% 할인쿠폰 4장 지급"));
//            buyerLevel5.setId(5L);
//            buyerLevel5.setPreMonPerfStart(BigDecimal.valueOf(150));
//            buyerLevel5.setPreMonPerfEnd(BigDecimal.valueOf(1000000000));
//            buyerLevel5.setSaveRate(BigDecimal.valueOf(7));
//            buyerLevel5.setTotalBenefits(BigDecimal.valueOf(2700000));
//            buyerLevelRepository.save(buyerLevel5);
//        }
//    }
//
//    private InterText getName(String name){
//        InterText interText = new InterText();
//        interText.setTextKoKr(name);
////        interText.setTextEnUs(name);
////        interText.setTextJaJp(name);
////        interText.setTextZhCn(name);
////        interText.setTextZhTw(name);
//        return interText;
//    }
//}
