package kr.co.poolathome.core.domain.user;

import lombok.Getter;

@Getter
public enum ApprovalStatus {
  NONE("해당 없음"),
  WAITING("승인 대기중"),
  APPROVAL("가입 승인"),
  DENY("반려");

  private final String value;

  ApprovalStatus(final String value) {
    this.value = value;
  }
}
