package kr.co.poolathome.core.domain.setting;

import kr.co.poolathome.core.domain.AbstractEntity;
import kr.co.poolathome.core.domain.International.InternationalMode;
import kr.co.poolathome.core.domain.RestEntityBody;
import kr.co.poolathome.core.model.resbody.setting.AppSettingResBody;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Locale;

@Entity
@Getter
@Setter
@ToString
public class AppSetting extends AbstractEntity<Long> implements RestEntityBody<AppSettingResBody> {

  private static final long serialVersionUID = 3667972003401550990L;

  public static String[] IGNORE_PROPERTIES = {"id"};

  @PrePersist
  public void prePersist() {
    if (this.defaultLocale == null) {
      this.setDefaultLocale(Locale.KOREA);
    }
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  /**
   * 언어 설정 (국제화)
   */
  @Column
  private Locale defaultLocale;

  @Column
  private boolean international; // 국제화 모드

  @Embedded
  private InternationalMode internationalMode;

  @Embedded
  private ComponentMode componentMode;

  @Column(columnDefinition = "INT(10) default 3")
  private int maxOrderQuantity;

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {

  }

  @Override
  public AppSettingResBody toBody(Locale locale) {
    return AppSettingResBody.builder()
        .defaultLocale(defaultLocale)
        .international(international)
        .internationalMode(internationalMode)
        .maxOrderQuantity(maxOrderQuantity)
        .build();
  }
}
