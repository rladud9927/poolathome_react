package kr.co.poolathome.core.vo.info;

import kr.co.poolathome.core.vo.BaseEntityInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserInfo extends BaseEntityInfo {

  private static final long serialVersionUID = -5511509450884992633L;

  private String email;
  private String mobile;
}
