package kr.co.poolathome.core.model.resbody.document;

import kr.co.poolathome.core.domain.document.Document;
import kr.co.poolathome.core.model.BaseResponseBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDate;


@Relation(value = "document", collectionRelation = "documents")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocumentResBody extends BaseResponseBody {

  private static final long serialVersionUID = 6623011747570416708L;

  private Long id;
  private Document.Type type;
  private String title; // 제목
  private String content;

//  @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_UNIT_BAR)
//  @JsonSerialize(using = JsonLocalDateSerializer.class)
//  @JsonDeserialize(using = JsonLocalDateDeserializer.class)
  private LocalDate executeDate; // 시행일
}
