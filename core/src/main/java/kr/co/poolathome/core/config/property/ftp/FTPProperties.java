package kr.co.poolathome.core.config.property.ftp;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "ftp")
public class FTPProperties implements Serializable {
  private static final long serialVersionUID = -9064235219361265834L;
  private String host;
  private int port;
  private String username;
  private String password;
}
