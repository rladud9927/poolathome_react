package kr.co.poolathome.core.model.reqbody.account;

import kr.co.poolathome.core.model.BaseRequestBody;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ForgotPassword extends BaseRequestBody {

  private static final long serialVersionUID = 450287893066215212L;

  private String email;
  private String phone;
}
