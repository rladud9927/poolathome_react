package kr.co.poolathome.core.domain.user.mybatis;

import kr.co.poolathome.core.model.resbody.account.MeResBody;

import java.util.List;

/**
 * MyBatis
 */
public interface UserMyBatis {

    List<MeResBody> selectUsers() throws Exception;
}
