package kr.co.poolathome.core.service.send.email;

import freemarker.template.Configuration;
import freemarker.template.Template;
import kr.co.poolathome.core.config.security.SecurityUtils;
import kr.co.poolathome.core.domain.email.EmailHistory;
import kr.co.poolathome.core.domain.email.EmailHistoryRepository;
import kr.co.poolathome.core.domain.user.UserRepository;
import kr.co.poolathome.core.model.property.MetaEmail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class SendEmailServiceImpl implements SendEmailService {

  @Value("${spring.mail.username}")
  private String sendEmailAddress;

  @Autowired
  private JavaMailSenderImpl javaMailSender;

  @Autowired
  private Configuration cfg;


  @Autowired
  private UserRepository userRepository;

  @Autowired
  private EmailHistoryRepository emailHistoryRepository;

  @Autowired
  private MetaEmail metaEmail;


  @Override
  public void send(String to, String subject, String body) throws Exception {

    SimpleMailMessage smm = new SimpleMailMessage();
    smm.setTo(to);
    smm.setFrom(sendEmailAddress);
    smm.setSubject(subject);
    smm.setText(body);

    log.debug("sendEmailAddress : {}", sendEmailAddress);
    log.debug("to : {}", to);
    log.debug("subject : {}", subject);
    log.debug("body : {}", body);

    log.debug("Host : {}", javaMailSender.getHost());
    log.debug("Password : {}", javaMailSender.getPassword());
    log.debug("Encoding : {}", javaMailSender.getDefaultEncoding());
    log.debug("Protocol : {}", javaMailSender.getProtocol());

    javaMailSender.testConnection();
    javaMailSender.send(smm);
  }

  @Override
  public void send(String to, String subject, String body, String[] ccList) throws Exception {

    SimpleMailMessage smm = new SimpleMailMessage();
    smm.setTo(to);
    smm.setFrom(sendEmailAddress);
    smm.setSubject(subject);
    smm.setCc(ccList);
    smm.setText(body);

    javaMailSender.send(smm);
  }

  @Override
  public void send(String to, String subject, Map<String, Object> model, String templatePathName) throws Exception {

    this.addMetaEmail(model);
    Template template = cfg.getTemplate(templatePathName);
    String body = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);

    log.info("body : {}", body);

    SimpleMailMessage smm = new SimpleMailMessage();
    smm.setTo(to);
    smm.setFrom(sendEmailAddress);
    smm.setSubject(subject);
    smm.setText(body);

    javaMailSender.send(smm);

  }

  @Override
  public void send(List<String> to, String subject, String body) throws Exception {
    SimpleMailMessage smm = new SimpleMailMessage();

    String[] tos = new String[to.size()];
    to.toArray(tos);
    smm.setFrom(sendEmailAddress);
    smm.setTo(tos);
    smm.setSubject(subject);
    smm.setText(body);

    javaMailSender.send(smm);
  }

  private void addMetaEmail(Map<String, Object> model) {
    model.put("metaEmail", metaEmail);
  }

  private void createHistory(String to, String subject, String body) {

    EmailHistory emailHistory = new EmailHistory();
    emailHistory.setSendAddress(sendEmailAddress);
    emailHistory.setDestAddress(to);
    emailHistory.setSubject(subject);
    emailHistory.setMsgBody(body);

    Long idUser = SecurityUtils.getCurrentUserId();
    if (idUser != null) {
      userRepository.findById(idUser)
          .ifPresent(emailHistory::setRelativeUser);
    }

    emailHistoryRepository.save(emailHistory);
  }
}
