package kr.co.poolathome.core.domain.board.qna;

import kr.co.poolathome.core.domain.AbstractEntity;
import kr.co.poolathome.core.domain.RestEntityBody;
import kr.co.poolathome.core.utils.HtmlUtils;
import kr.co.poolathome.core.utils.StringUtils;
import kr.co.poolathome.core.model.resbody.board.QnaResBody;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.Locale;

/**
 * 1:1 문의하기
 */
@Slf4j
@Entity(name = "QuestionsAndAnswers")
@Getter
@Setter
@ToString(exclude = {
})
@NoArgsConstructor
public class Qna extends AbstractEntity<Long> implements RestEntityBody<QnaResBody> {

  private static final long serialVersionUID = 6328325247234411688L;

  public static String[] IGNORE_PROPERTIES = {
      "id",
      "qnaNoMember"};

  @PrePersist
  public void prePersist() {

    if (StringUtils.isNotEmpty(this.content)) {
      this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
    }
  }

  @PreUpdate
  public void preUpdate() {

    if (StringUtils.isNotEmpty(this.content)) {
      this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
    }
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Lob
  @Column(columnDefinition = "TEXT", length = 65535)
  private String content;

  @Column(length = 100)
  private String area;

  @Column(length = 30)
  private String companyName;

  @Column(length = 10)
  private String contactName;

  @Column(length = 12)
  private String phone;

  @Column(length = 100)
  private String email;

  @Column
  private boolean termsAgree;


  @Column(columnDefinition = "BIT(1) default 1")
  private boolean active; // 활성/비활성

  @Column(name = "deleteRow", columnDefinition = "BIT(1) default 0")
  private boolean delete; // 활성/비활성

  @Column(length = 20)
  private String ipAddress;

  @Embedded
  private Answer answer;

  public boolean getHasAnswer() {
    return this.answer != null && StringUtils.isNotEmpty(this.answer.getContent());
  }

  @Override
  public void delete() {

  }

  @Override
  public void lazy() {
  }

  @Override
  public QnaResBody toBody(Locale locale) {
    return QnaResBody.builder()
        .id(this.id)
        .content(this.content)
        .answer(this.answer)
        .regTime(this.createdDate)
        .build();
  }
}
