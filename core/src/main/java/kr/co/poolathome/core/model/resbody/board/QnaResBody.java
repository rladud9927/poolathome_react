package kr.co.poolathome.core.model.resbody.board;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import kr.co.poolathome.core.config.serializer.JsonLocalDateTimeDeserializer;
import kr.co.poolathome.core.config.serializer.JsonLocalDateTimeSerializer;
import kr.co.poolathome.core.domain.board.qna.Answer;
import kr.co.poolathome.core.model.BaseResponseBody;
import kr.co.poolathome.core.utils.StringUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Q&A")
@Relation(value = "qna", collectionRelation = "qnas")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QnaResBody extends BaseResponseBody {

  private static final long serialVersionUID = -8865272994721416070L;

  @Schema(description = "ID")
  private Long id;

  @Schema(description = "질문 제목")
  private String title; //

  @Schema(description = "질문 내용")
  private String content;

  @Schema(description = "답변")
  private Answer answer;

//  @Schema(description = "첨부파일")
//  private List<FileMeta> files;

  @Schema(description = "작성일")
  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  @JsonDeserialize(using = JsonLocalDateTimeDeserializer.class)
  private LocalDateTime regTime;

  @Schema(description = "작성자 계정 ID")
  private Long idUser; // 작성자명

  @Schema(description = "작성자명")
  private String fullName; // 작성자명

  @Schema(description = "카테고리")
  private List<CategoryResBody> categories;

  @Schema(description = "답변 유무")
  public boolean getHasAnswer() {
    return !StringUtils.isEmpty(this.answer);
  }
}
