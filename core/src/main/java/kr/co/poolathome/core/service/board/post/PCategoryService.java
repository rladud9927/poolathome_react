package kr.co.poolathome.core.service.board.post;

import kr.co.poolathome.core.domain.board.post.Post;
import kr.co.poolathome.core.domain.board.post.category.PCategory;
import kr.co.poolathome.core.service.CategoryService;

import java.util.List;
import java.util.Locale;

public interface PCategoryService extends CategoryService<PCategory, Long> {

  List<PCategory> list(Locale locale, Post.Type type);

  boolean isDuplicate(Locale locale, String name, Post.Type type);
}
