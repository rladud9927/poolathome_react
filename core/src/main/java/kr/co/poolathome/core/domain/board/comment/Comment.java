package kr.co.poolathome.core.domain.board.comment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kr.co.poolathome.core.domain.AbstractEntity;
import kr.co.poolathome.core.domain.RestEntityBody;
//import kr.co.poolathome.core.domain.board.event.Event;
import kr.co.poolathome.core.domain.board.post.Post;
import kr.co.poolathome.core.domain.user.User;
import kr.co.poolathome.core.model.resbody.board.CommentResBody;
import kr.co.poolathome.core.utils.HtmlUtils;
import kr.co.poolathome.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 댓글 테이블
 */
@Entity
@Getter
@Setter
@ToString(exclude = {"relativeUser", "relativePost", "relativeParent"})
public class Comment extends AbstractEntity<Long>
    implements RestEntityBody<CommentResBody> {

  private static final long serialVersionUID = -4955881323202878008L;

  public static String[] IGNORE_PROPERTIES = {
      "id",
      "relativeUser",
      "relativePost",
      "relativeEvent",
      "relativeParent"
  };

  @Getter
  public enum Type {
    //        EVENT("Event"),
    POST("Post");

    private final String value;

    Type(final String value) {
      this.value = value;
    }
  }


  @PrePersist
  public void prePersist() {
    if (StringUtils.isNotEmpty(this.content))
      this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
  }

  @PreUpdate
  public void preUpdate() {
    if (StringUtils.isNotEmpty(this.content))
      this.content = HtmlUtils.convertLineSeparatorToBrTag(this.content);
  }

  @Id
  @GeneratedValue
  @Column(unique = true)
  private Long id;

  @Lob
  @Column(columnDefinition = "TEXT", length = 65535)
  private String content;

  @Enumerated
  @Column(columnDefinition = "TINYINT(1) default 0")
  private Type type;

  @Column(columnDefinition = "BIT(1) default 1")
  private boolean active; // 활성/비활성

  @Column(name = "deleteRow", columnDefinition = "BIT(1) default 0")
  private boolean delete; // 댓글삭제

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idParent", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Reply_For_Comment"))
  private Comment relativeParent;

  @JsonIgnore
  @OneToMany(mappedBy = "relativeParent", cascade = CascadeType.REMOVE)
  private List<Comment> comments = new ArrayList<>();

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idUser", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_User_For_Comment"))
  private User relativeUser;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "idPost", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_Post_For_Comment"))
  private Post relativePost;

  @Override
  public void delete() {
    if (this.relativeUser != null)
      this.relativeUser = null;
    if (this.relativePost != null)
      this.relativePost = null;
  }

  @Override
  public void lazy() {
    if (this.relativeUser != null)
      this.relativeUser.getId();
    if (this.relativePost != null)
      this.relativePost.getId();
  }

  @Override
  public CommentResBody toBody(Locale locale) {

    return CommentResBody.builder()
        .id(this.getId())
        .content(this.isDelete() ? "삭제된 댓글입니다." : this.getContent())
        .delete(this.isDelete())
        .profileImage(this.getRelativeUser() != null ? this.getRelativeUser().getImage() : "")
        .locale(locale)
        .build();
  }
}
