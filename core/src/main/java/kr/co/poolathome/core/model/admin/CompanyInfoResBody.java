package kr.co.poolathome.core.model.admin;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

import java.io.Serializable;


@Relation(value = "companyInfo")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompanyInfoResBody implements Serializable {


  private static final long serialVersionUID = -167991422532129164L;
  private Long id;
  private String name;
}
