package kr.co.poolathome.core.service.file;

import kr.co.poolathome.core.config.exception.BadRequestException;
import kr.co.poolathome.core.config.exception.crud.UpdateErrorException;
import kr.co.poolathome.core.domain.file.FileEntity;
import kr.co.poolathome.core.domain.file.FileEntityPredicate;
import kr.co.poolathome.core.domain.file.FileEntityRepository;
import kr.co.poolathome.core.model.Filter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Slf4j
@Service
public class FileEntityServiceImpl implements FileEntityService {

  @Autowired
  private FileEntityRepository fileEntityRepository;

  @Override
  @Transactional
  public FileEntity create(FileEntity file) {
    return fileEntityRepository.save(file);
  }

  @Override
  @Transactional
  public FileEntity update(FileEntity file) {

    if (file.getId() == null) {
      throw new BadRequestException();
    }

    return fileEntityRepository.findById(file.getId())
        .map(ori -> {
          BeanUtils.copyProperties(file, ori, FileEntity.IGNORE_PROPERTIES);
          return fileEntityRepository.save(ori);
        }).orElseThrow(() -> new UpdateErrorException(file.getId(), FileEntity.class.getName()));
  }

  @Override
  @Transactional
  public void delete(Long id) {
    fileEntityRepository.findById(id)
        .ifPresent(file -> {
          file.delete();
          fileEntityRepository.delete(file);
        });
  }

  @Override
  @Transactional(readOnly = true)
  public FileEntity get(Locale locale, Long id) {
    return fileEntityRepository.findById(id)
        .map(file -> {
          return file;
        }).orElse(null);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<FileEntity> page(Locale locale, Filter filter) {

    Page<FileEntity> page = fileEntityRepository.findAll(
        FileEntityPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .values(),
        filter.getPageable());

    return page;
  }
}