package kr.co.poolathome.core.config.property.ship;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "ship.sm")
public class ShipPostalCode implements Serializable {

  private static final long serialVersionUID = -9166652319294598186L;
  private List<PostalCodeRange> ranges;
}
