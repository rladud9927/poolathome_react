package kr.co.poolathome.core.domain.embedd;

import kr.co.poolathome.core.utils.StringUtils;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class Address implements java.io.Serializable {

  private static final long serialVersionUID = -624670171718146604L;


  // 주소
  @Column
  private String postalCode; // 우편번호

  @Column
  private String address1; // 기본 주소

  @Column
  private String address2; // 상세 주소


  @Column
  private String jibunAddr; // 지번 주소

  @Column
  private String engAddr; // 지번 주소

  @Embedded
  private GPS gps; // 위도/경도


  @Column
  private String siNm; // 지역 1 (시도명)

  @Column
  private String sggNm; // 지역 2 (시군구명)

  @Column
  private String emdNm; // 지역 3 (읍면동명)

  @Column
  private String liNm; // 지역 4 (법정리명)

  @Override
  public String toString() {

    StringBuilder sb = new StringBuilder();
    if (StringUtils.isNotEmpty(this.postalCode))
      sb.append("(" + this.postalCode + ") ");
    sb.append(this.address1);
    if (StringUtils.isNotEmpty(this.address2))
      sb.append(this.address2);

    return sb.toString();
  }

  public String getValue() {
    return (StringUtils.isNotEmpty(this.address1) ? this.address1 : "") + (StringUtils.isNotEmpty(this.address2) ? (" " + this.address2) : "");
  }
}
