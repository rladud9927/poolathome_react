package kr.co.poolathome.core.service.map.kakao;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
public class KakaoAddressSearch implements Serializable {
  private static final long serialVersionUID = -3954758159217821929L;


  private List<KakaoAddressDocument> documents;
  private KakaoAddressMeta meta;

}
