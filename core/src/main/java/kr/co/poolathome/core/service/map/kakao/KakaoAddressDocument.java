package kr.co.poolathome.core.service.map.kakao;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class KakaoAddressDocument implements Serializable {
  private static final long serialVersionUID = 3106174960918287103L;

  private KakaoAddress address;

  @SerializedName("address_name")
  private String addressName;

  @SerializedName("address_type")
  private String addressType;

  @SerializedName("road_address")
  private KakaoRoadAddress roadAddress;

  @SerializedName("x")
  private BigDecimal x;

  @SerializedName("y")
  private BigDecimal y;
}
