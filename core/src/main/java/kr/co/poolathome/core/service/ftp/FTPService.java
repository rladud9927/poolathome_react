package kr.co.poolathome.core.service.ftp;

import kr.co.poolathome.core.model.file.FileMeta;
import org.springframework.web.multipart.MultipartFile;

public interface FTPService {

  boolean ftpCheck();
  FileMeta upload(String host, String path, String filename, MultipartFile file);
}
