package kr.co.poolathome.core.model.reqbody.account;

import kr.co.poolathome.core.model.BaseRequestBody;
import kr.co.poolathome.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Pattern;

@Getter
@Setter
@ToString
public class FindAccountEmailReqBody extends BaseRequestBody {

  private static final long serialVersionUID = 3309010887381009987L;

  @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
  @Schema(description = "휴대폰 번호", example = "01011112222", minLength = 10, maxLength = 11)
  private String mobile;

  @Schema(description = "Auth code")
  private String fullName; // 이름
}
