package kr.co.poolathome.core.service.aws;


import kr.co.poolathome.core.model.aws.SESSender;

import java.util.concurrent.Future;

public interface AWSSESService {

    Future<String> send(SESSender sesSender);
}
