package kr.co.poolathome.core.domain.document;

// import kr.co.poolathome.core.domain.b2b.ServiceType;

public interface DocumentRepositoryCustom {

  Document latestDoc(Document.Type type);
}
