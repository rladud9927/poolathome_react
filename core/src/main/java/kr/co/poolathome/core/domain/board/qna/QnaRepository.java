package kr.co.poolathome.core.domain.board.qna;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface QnaRepository extends
    JpaRepository<Qna, Long>,
    QuerydslPredicateExecutor<Qna> {

}
