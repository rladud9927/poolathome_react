package kr.co.poolathome.core.service.map.kakao;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
public class KakaoAddressMeta implements Serializable {

  private static final long serialVersionUID = -6807133525038188164L;

  @SerializedName("is_end")
  private boolean end;

  @SerializedName("pageable_count")
  private int size;

  @SerializedName("total_count")
  private long total;
}
