package kr.co.poolathome.core.domain;

import lombok.Getter;

@Getter
public enum BusinessType {
  B2C("B2C 비티스 몰 사이트"),
  B2B("B2B 사이트"),
  BRAND("브랜드 사이트"),
  ;

  @Getter
  private final String value;

  BusinessType(final String value) {
    this.value = value;
  }
}
