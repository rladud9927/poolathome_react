package kr.co.poolathome.core.model.file;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileUploadService {

    List<FileMeta> uploadImages(MultipartFile[] files);
    FileMeta uploadImage(MultipartFile file);
    FileMeta uploadFile(MultipartFile file);
    FileMeta uploadVideo(MultipartFile file);
    boolean delete(String url);
}
