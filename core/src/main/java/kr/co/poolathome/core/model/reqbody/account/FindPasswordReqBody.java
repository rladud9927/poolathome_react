package kr.co.poolathome.core.model.reqbody.account;

import kr.co.poolathome.core.model.BaseRequestBody;
import kr.co.poolathome.core.utils.ValidUtils;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FindPasswordReqBody extends BaseRequestBody {

    private static final long serialVersionUID = 357681494224833031L;

    @Email
    @Schema(description = "이메일 주소", example = "test@poolathome.co.kr", minLength = 3, maxLength = 255)
    private String email;

    @Pattern(regexp = ValidUtils.PATTERN_MOBILE)
    @Schema(description = "휴대폰 번호", example = "01011112222", minLength = 10, maxLength = 11)
    private String mobile;

    @Pattern(regexp = ValidUtils.PATTERN_FULLNAME)
    @Schema(description = "성명", example = "홍길동", minLength = 2, maxLength = 10)
    private String fullName;

    @Schema(description = "Auth code")
    private String code;
}
