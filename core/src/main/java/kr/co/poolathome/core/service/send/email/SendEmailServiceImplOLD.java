package kr.co.poolathome.core.service.send.email;//package kr.co.poolathome.core.service.send.email;
//
//import kr.co.poolathome.core.config.security.SecurityUtils;
//import kr.co.poolathome.core.domain.email.EmailHistory;
//import kr.co.poolathome.core.domain.email.EmailHistoryRepository;
//import kr.co.poolathome.core.domain.user.UserRepository;
//import kr.co.poolathome.core.model.aws.SESSender;
//import kr.co.poolathome.core.model.property.MetaEmail;
//import kr.co.poolathome.core.service.aws.AWSSESService;
//import freemarker.template.Configuration;
//import freemarker.template.Template;
//import freemarker.template.TemplateException;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
//
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.Future;
//
//@Slf4j
//@Service
//public class SendEmailServiceImplOLD implements SendEmailService {
//
//  public static final String EVENT_BUS_COMMAND = "ES_MAIL";
//
//  @Value("${aws.ses.sender.name}")
//  private String appName;
//
//  @Value("${aws.ses.sender.email}")
//  private String sendEmailAddress;
//
//  @Autowired
//  private AWSSESService awssesService;
//
//  @Autowired
//  private Configuration cfg;
//
//
//  @Autowired
//  private UserRepository userRepository;
//
//  @Autowired
//  private EmailHistoryRepository emailHistoryRepository;
//
//  @Autowired
//  private MetaEmail metaEmail;
//
//
//  @Override
//  public void send(String to, String subject, String body) {
//
//    try {
//      SESSender sesSender = new SESSender(Arrays.asList(to), subject, body);
//      Future<String> future = awssesService.send(sesSender);
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }
//
//  @Override
//  public void send(String to, String subject, String body, String[] ccList) {
//    try {
//      SESSender sesSender = new SESSender(Arrays.asList(to), subject, body);
//      awssesService.send(sesSender);
//    } catch (Exception e) {
//      log.error("> email error", e);
//      e.printStackTrace();
//    }
//  }
//
//  @Override
//  public void send(String to, String subject, Map<String, Object> model, String templatePathName) {
//    try {
//
//      this.addMetaEmail(model);
//      Template template = cfg.getTemplate(templatePathName);
//      String body = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
//
//      SESSender sesSender = new SESSender(Arrays.asList(to), subject, body);
//      awssesService.send(sesSender);
//    } catch (IOException | TemplateException e) {
//      log.error("> email error", e);
//      e.printStackTrace();
//    }
//  }
//
//  @Override
//  public void send(List<String> to, String subject, String body) {
//    try {
//      SESSender sesSender = new SESSender(to, subject, body);
//      awssesService.send(sesSender);
//
//      to.forEach(destAddress -> {
//        this.createHistory(destAddress, subject, body);
//      });
//    } catch (Exception e) {
//      log.error("> email error", e);
//      e.printStackTrace();
//    }
//  }
//
//  private void addMetaEmail(Map<String, Object> model) {
//    model.put("metaEmail", metaEmail);
//  }
//
//  private void createHistory(String to, String subject, String body) {
//
//    EmailHistory emailHistory = new EmailHistory();
//    emailHistory.setSendAddress(sendEmailAddress);
//    emailHistory.setDestAddress(to);
//    emailHistory.setSubject(subject);
//    emailHistory.setMsgBody(body);
//
//    Long idUser = SecurityUtils.getCurrentUserId();
//    if (idUser != null) {
//      userRepository.findById(idUser)
//          .ifPresent(emailHistory::setRelativeUser);
//    }
//
//    emailHistoryRepository.save(emailHistory);
//  }
//}
