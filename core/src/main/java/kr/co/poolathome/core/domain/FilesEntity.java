package kr.co.poolathome.core.domain;

/**
 * 파일을 가지고 있는 Entity 처리
 */
public interface FilesEntity {

  void uploadFiles();
}
