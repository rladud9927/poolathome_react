package kr.co.poolathome.core.model.property.kko;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.Serializable;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "kko-template")
public class KKOTemplate implements Serializable {

  private static final long serialVersionUID = 3905654535118777647L;

  @Value("${spring.application.name}")
  private String appName;

  private KKOMeta tmpl001;
  private KKOMeta tmpl002;
  private KKOMeta tmpl003;
  private KKOMeta tmpl004;
  private KKOMeta tmpl005;
  private KKOMeta tmpl006;
  private KKOMeta tmpl007;
  private KKOMeta tmpl008;
  private KKOMeta tmpl009;
  private KKOMeta tmpl010;
  private KKOMeta tmpl011;
  private KKOMeta tmpl012;
  private KKOMeta tmpl013;
  private KKOMeta tmpl014;
  private KKOMeta tmpl015;
  private KKOMeta tmpl016;
  private KKOMeta tmpl017;
  private KKOMeta tmpl018;
  private KKOMeta tmpl019;

  public KKO toTemplate001(String mobile, String test) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(test)) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl001;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{code}", test);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate002(String mobile, String fullName, String email, String joinTime) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(joinTime)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl002;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{email}", email);
    message = message.replace("#{joinTime}", joinTime);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate003(String mobile, String fullName, String productName) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(productName)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl003;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{productName}", productName);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate004(String mobile, String fullName, String oid, String productName, String amount, String storeName, String address, String pickUpDate) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(amount)
        || StringUtils.isEmpty(storeName)
        || StringUtils.isEmpty(address)
        || StringUtils.isEmpty(pickUpDate)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl004;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{amount}", amount);
    message = message.replace("#{storeName}", storeName);
    message = message.replace("#{address}", address);
    message = message.replace("#{pickUpDate}", pickUpDate);


    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate005(String mobile, String fullName, String oid, String productName, String amount, String storeName, String address, String pickUpDate) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(amount)
        || StringUtils.isEmpty(storeName)
        || StringUtils.isEmpty(address)
        || StringUtils.isEmpty(pickUpDate)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl005;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{amount}", amount);
    message = message.replace("#{storeName}", storeName);
    message = message.replace("#{address}", address);
    message = message.replace("#{pickUpDate}", pickUpDate);


    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate006(String mobile, String fullName, String oid, String productName, String amount, String storeName, String address) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(amount)
        || StringUtils.isEmpty(storeName)
        || StringUtils.isEmpty(address)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl006;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{amount}", amount);
    message = message.replace("#{storeName}", storeName);
    message = message.replace("#{address}", address);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate007(String mobile, String fullName, String productName, String courierInfo, String address) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(courierInfo)
        || StringUtils.isEmpty(address)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl007;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{courierInfo}", courierInfo);
    message = message.replace("#{address}", address);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate008(String mobile, String fullName, String oid, String productName, String amount, String paymentTime) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(amount)
        || StringUtils.isEmpty(paymentTime)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl008;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{amount}", amount);
    message = message.replace("#{paymentTime}", paymentTime);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate009(String mobile, String fullName, String productName, String arrivalDate, String address) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(arrivalDate)
        || StringUtils.isEmpty(address)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl009;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{arrivalDate}", arrivalDate);
    message = message.replace("#{address}", address);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate010(String mobile, String fullName, String oid, String productName, String address) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(address)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl010;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{address}", address);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate011(String mobile, String fullName, String oid, String productName, String address) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(address)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl011;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{address}", address);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate012(String mobile, String fullName, String oid, String productName, String address) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(oid)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(address)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl012;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{oid}", oid);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{address}", address);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate013(String mobile, String fullName, String changeDate, String buyerLevelName, String saleRate) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(changeDate)
        || StringUtils.isEmpty(buyerLevelName)
        || StringUtils.isEmpty(saleRate)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl013;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{changeDate}", changeDate);
    message = message.replace("#{buyerLevelName}", buyerLevelName);
    message = message.replace("#{saleRate}", saleRate);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate014(String mobile, String fullName, String couponName, String couponNumber, String expireDate) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(couponName)
        || StringUtils.isEmpty(couponNumber)
        || StringUtils.isEmpty(expireDate)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl014;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{couponName}", couponName);
    message = message.replace("#{couponNumber}", couponNumber);
    message = message.replace("#{expireDate}", expireDate);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate015(String mobile, String fullName, String couponName, String couponNumber, String expireDate) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(couponName)
        || StringUtils.isEmpty(couponNumber)
        || StringUtils.isEmpty(expireDate)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl015;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{couponName}", couponName);
    message = message.replace("#{couponNumber}", couponNumber);
    message = message.replace("#{expireDate}", expireDate);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate016(String mobile, String fullName, String dormantDate) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(dormantDate)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl016;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{dormantDate}", dormantDate);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate017(String mobile, String fullName) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl017;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate018(String mobile, String fullName, String termName, String startDate, String content) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(termName)
        || StringUtils.isEmpty(startDate)
        || StringUtils.isEmpty(content)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl018;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{termName}", termName);
    message = message.replace("#{startDate}", startDate);
    message = message.replace("#{content}", content);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }

  public KKO toTemplate019(String mobile, String fullName, String productName, String vbankNum, String vbankName, String vbankHolder, String amount, String expirationDate, String oid) {

    if (StringUtils.isEmpty(mobile)
        || StringUtils.isEmpty(fullName)
        || StringUtils.isEmpty(productName)
        || StringUtils.isEmpty(vbankNum)
        || StringUtils.isEmpty(vbankName)
        || StringUtils.isEmpty(vbankHolder)
        || StringUtils.isEmpty(amount)
        || StringUtils.isEmpty(expirationDate)
        || StringUtils.isEmpty(oid)
    ) {
      throw new IllegalArgumentException("필드를 모두 입력하세요.");
    }

    final KKOMeta kkoMeta = this.tmpl019;

    String message = kkoMeta.getMessage();
    message = message.replace("#{appName}", appName);
    message = message.replace("#{fullName}", fullName);
    message = message.replace("#{productName}", productName);
    message = message.replace("#{vbankNum}", vbankNum);
    message = message.replace("#{vbankName}", vbankName);
    message = message.replace("#{vbankHolder}", vbankHolder);
    message = message.replace("#{amount}", amount);
    message = message.replace("#{expirationDate}", expirationDate);
    message = message.replace("#{oid}", oid);

    return KKO.builder()
        .phone(mobile)
        .message(message)
        .templateCode(kkoMeta.getCode())
        .failedType(KKO.FailedType.LMS)
        .failedSubject(kkoMeta.getName())
        .failedMsg(message)
        .btnTypes(kkoMeta.getBtnTypes())
        .btnTxts(kkoMeta.getBtnTxts())
        .btnUrls1(kkoMeta.getBtnUrls1())
        .btnUrls2(kkoMeta.getBtnUrls2())
        .build();
  }
}
