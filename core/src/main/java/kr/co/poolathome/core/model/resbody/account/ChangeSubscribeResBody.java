package kr.co.poolathome.core.model.resbody.account;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import kr.co.poolathome.core.config.serializer.JsonLocalDateTimeDeserializer;
import kr.co.poolathome.core.config.serializer.JsonLocalDateTimeSerializer;
import kr.co.poolathome.core.model.BaseResponseBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDateTime;

@Relation(value = "changeSubscribe")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChangeSubscribeResBody extends BaseResponseBody {

  private static final long serialVersionUID = -3274066935580047895L;

  @Schema(name = "동의한 여부")
  private boolean agree;

  @Schema(name = "동의한 시간")
  @JsonSerialize(using = JsonLocalDateTimeSerializer.class)
  @JsonDeserialize(using = JsonLocalDateTimeDeserializer.class)
  private LocalDateTime time;
}
