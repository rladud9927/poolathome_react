package kr.co.poolathome.core.service.board.post;

import com.google.common.collect.Lists;
import kr.co.poolathome.core.config.exception.BadRequestException;
import kr.co.poolathome.core.domain.board.post.Post;
import kr.co.poolathome.core.domain.board.post.PostPredicate;
import kr.co.poolathome.core.domain.board.post.PostRepository;
import kr.co.poolathome.core.model.Filter;
import kr.co.poolathome.core.model.admin.PostInfoResBody;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@Service
@Validated
public class PostServiceImpl implements PostService {

  @Autowired
  private PostRepository postRepository;

  @Override
  @Transactional
  public Post create(Post post) {
    post.uploadFiles();
    return postRepository.save(post);
  }

  @Override
  @Transactional
  public Post update(Post post) {

    if (post.getId() == null) {
      throw new BadRequestException();
    }
    return postRepository.findById(post.getId())
            .map(ori -> {
              post.uploadFiles();
              BeanUtils.copyProperties(post, ori, Post.IGNORE_PROPERTIES);
              return postRepository.save(ori);
            }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public Post get(Locale locale, Long id) {
    return postRepository.findById(id)
            .map(post -> {
              post.lazy();
              post.setLocale(locale);
              return post;
            }).orElse(null);
  }

  @Override
  @Transactional
  public void delete(Long id) {
    postRepository.deleteById(id);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Post> page(Locale locale, Filter filter) {
   return this.page(locale, filter, null, null, null);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Post> page(Locale locale, Filter filter, Post.Type type) {
    return this.page(locale, filter, type, null, null);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Post> page(Locale locale, Filter filter, Post.Type type, Boolean isActive) {
    return this.page(locale, filter, type, isActive, null);
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Post> page(Locale locale, Filter filter, Post.Type type, Boolean isActive, Long idCategory) {

    Page<Post> page = postRepository.findAll(
            PostPredicate.getInstance()
                    .search(filter.getQuery())
                    .startDate(filter.getStartDate())
                    .endDate(filter.getEndDate())
                    .type(type)
                    .active(isActive)
                    .values(),
            filter.getPageable());

    page.forEach(post -> {
      post.lazy();
      post.setLocale(locale);
    });
    return page;
  }

  @Override
  @Transactional(readOnly = true)
  public List<PostInfoResBody> list(String query) {

    List<Post> list = Lists.newArrayList(postRepository.findAll(
        PostPredicate.getInstance()
            .search(query)
            .values()));

    return list.stream().map(post -> {
      PostInfoResBody po = new PostInfoResBody();
      po.setId(post.getId());
      po.setTitle(post.getTitle());
      po.setContent(post.getContent());
      return po;
    }).collect(Collectors.toList());
  }
}
