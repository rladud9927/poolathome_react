package kr.co.poolathome.core.model.file;

import kr.co.poolathome.core.domain.file.FileEntity;
import kr.co.poolathome.core.utils.StringUtils;
import lombok.*;

import java.io.Serializable;
import java.net.URL;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FileMeta implements Serializable {

  private static final long serialVersionUID = -3770830091024680811L;

  private Long id;
  private URL url;
  private String originalFilename;
  private String filename;
  private String mimeType;
  private long size;

  public FileEntity toFileEntity() {
    FileEntity fileEntity = new FileEntity();
    fileEntity.setUrl(StringUtils.isNotEmpty(this.url) ? this.url.toString() : null);
    fileEntity.setFilename(this.filename);
    fileEntity.setOriginalFilename(this.originalFilename);
    fileEntity.setMimeType(this.mimeType);
    fileEntity.setSize(this.size);
    return fileEntity;
  }
}

