package kr.co.poolathome.core.config.property.ship;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostalCodeRange implements Serializable {
  private static final long serialVersionUID = 7737270770035207097L;

  private String title;
  private int start;
  private int end;
}
