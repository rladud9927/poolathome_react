package kr.co.poolathome.core.service.send.email;

import kr.co.poolathome.core.model.Filter;
import kr.co.poolathome.core.config.exception.BadRequestException;
import kr.co.poolathome.core.domain.email.EmailHistory;
import kr.co.poolathome.core.domain.email.EmailHistoryPredicate;
import kr.co.poolathome.core.domain.email.EmailHistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Slf4j
@Service
public class EmailHistoryServiceImpl implements EmailHistoryService {

  @Autowired
  private EmailHistoryRepository emailHistoryRepository;

  @Override
  @Transactional
  public EmailHistory create(EmailHistory history) {
    return emailHistoryRepository.save(history);
  }

  @Override
  @Transactional
  public EmailHistory update(EmailHistory history) {
    if (history.getId() == null) {
      throw new BadRequestException();
    }

    return emailHistoryRepository.findById(history.getId())
      .map(ori -> {
        BeanUtils.copyProperties(history, ori, EmailHistory.IGNORE_PROPERTIES);
        return emailHistoryRepository.save(ori);
      }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional
  public EmailHistory get(Locale locale, Long id) {
    return emailHistoryRepository.findById(id)
      .map(history -> {
        history.lazy();
        return history;
      }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional
  public void delete(Long id) {
    emailHistoryRepository.deleteById(id);
  }

  @Override
  public Page<EmailHistory> page(Locale locale, Filter filter) {

    Page<EmailHistory> page = emailHistoryRepository.findAll(
        EmailHistoryPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .values(),
        filter.getPageable());

    page.forEach(EmailHistory::lazy);
    return page;
  }

  @Override
  @Transactional
  public Page<EmailHistory> page(Filter filter, EmailHistory.Type type) {
    Page<EmailHistory> page = emailHistoryRepository.findAll(
            EmailHistoryPredicate.getInstance()
                    .search(filter.getQuery())
                    .startDate(filter.getStartDate())
                    .endDate(filter.getEndDate())
                    .type(type)
                    .values(),
      filter.getPageable());

    page.forEach(EmailHistory::lazy);
    return page;
  }
}
