package kr.co.poolathome.core.model.resbody;

import kr.co.poolathome.core.model.BaseResponseBody;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

@Relation(value = "result", collectionRelation = "results")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder(buildMethodName = "resultBodyBuild", builderMethodName = "resultBodyBuilder")
public class ResultResBody extends BaseResponseBody {

    private static final long serialVersionUID = 2813017186081236225L;

    private boolean success;
    private String message;

    public static ResultResBody of(boolean result) {
        ResultResBody resultResBody = new ResultResBody();
        if (result) {
            resultResBody.setSuccess(true);
        } else {
            resultResBody.setSuccess(false);
        }
        return resultResBody;
    }

    public static ResultResBody of(boolean result, String message) {
        ResultResBody resultResBody = new ResultResBody();
        if (result) {
            resultResBody.setSuccess(true);
        } else {
            resultResBody.setSuccess(false);
        }
        resultResBody.setMessage(message);
        return resultResBody;
    }
}
