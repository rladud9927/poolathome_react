package kr.co.poolathome.core.service.ftp;

import kr.co.poolathome.core.config.exception.BadRequestException;
import kr.co.poolathome.core.config.property.ftp.FTPProperties;
import kr.co.poolathome.core.model.file.FileMeta;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

@Slf4j
@Service
public class FTPServiceImpl implements FTPService {

  @Autowired
  private FTPProperties ftpProperties;

  @Override
  public boolean ftpCheck() {

    FTPClient ftp = null;

    try {

      ftp = new FTPClient();
      ftp.setControlEncoding("UTF-8");
      log.info("# FTP Properties : {}", ftpProperties);
      ftp.connect(ftpProperties.getHost(), ftpProperties.getPort());
      log.info("# ftp.getReplyCode() : {}", ftp.getReplyCode());
      ftp.login(ftpProperties.getUsername(), ftpProperties.getPassword());
      ftp.logout();
      return true;
    } catch (IOException e) {
      log.error("FTP Failed", e);
      e.printStackTrace();
    }
    return false;
  }

  @Override
  public FileMeta upload(String host, String path, String filename, MultipartFile file) {

    log.info("##### upload file #####");
    log.info("host : {}", host);
    log.info("path : {}", path);
    log.info("filename : {}", filename);

    FTPClient ftp = null;
    if (file == null) {
      throw new BadRequestException("업로드할 파일이 존재하지 않습니다");
    }
    String originalFilename = file.getOriginalFilename();
    String mimeType = file.getContentType();
    long size = file.getSize();

    try {

      log.debug("ftpProperties.getHost() : {}", ftpProperties.getHost());
      log.debug("ftpProperties.getPort() : {}", ftpProperties.getPort());
      log.debug("ftpProperties.getPassword() : {}", ftpProperties.getPassword());
      ftp = new FTPClient();
      ftp.setControlEncoding("UTF-8");
      ftp.connect(ftpProperties.getHost(), ftpProperties.getPort());
      ftp.login(ftpProperties.getUsername(), ftpProperties.getPassword());
      ftp.enterLocalPassiveMode();
      ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
      ftp.changeWorkingDirectory(path);

      log.info("# ftp.isConnected() ::: {}", ftp.isConnected());
      if (ftp.isConnected()) {
        InputStream fileInputStream = file.getInputStream();
        log.debug("# ftp upload start !");
        boolean isSuccess = ftp.storeFile(filename, fileInputStream);
        log.info("# ftp result ::: {}", isSuccess);
        fileInputStream.close();
        ftp.logout();

        if (!isSuccess) {
          throw new RuntimeException("FTP 업로드 실패");
        }
      }
      log.debug("filename ::: {}", filename);
      log.debug(host + "/" + path + "/" + filename);

//      return CDN_IMG_DOMAIN + "/" + filename;
      return FileMeta.builder()
          .url(new URL(host + "/" + path + "/" + filename))
          .originalFilename(originalFilename)
          .filename(filename)
          .size(size)
          .mimeType(mimeType)
          .build();

    } catch (IOException e) {
      log.error("FILE UPLOAD ERROR", e);
      e.printStackTrace();
    } finally {
      if (ftp != null && ftp.isConnected()) {
        try {
          ftp.disconnect();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    return null;
  }
}
