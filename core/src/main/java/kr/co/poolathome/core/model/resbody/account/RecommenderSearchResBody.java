package kr.co.poolathome.core.model.resbody.account;

import kr.co.poolathome.core.model.BaseResponseBody;
import lombok.*;
import org.springframework.hateoas.server.core.Relation;

@Relation(value = "recommenderSearch")
@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RecommenderSearchResBody extends BaseResponseBody {
  private static final long serialVersionUID = -5793896919279895515L;
  private Long id;
  private String recommender;
}
