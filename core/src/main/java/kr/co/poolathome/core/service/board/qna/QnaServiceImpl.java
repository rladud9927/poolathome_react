package kr.co.poolathome.core.service.board.qna;

import kr.co.poolathome.core.model.Filter;
import kr.co.poolathome.core.config.exception.BadRequestException;
import kr.co.poolathome.core.domain.board.qna.Qna;
import kr.co.poolathome.core.domain.board.qna.QnaPredicate;
import kr.co.poolathome.core.domain.board.qna.QnaRepository;
import kr.co.poolathome.core.domain.user.UserRepository;
import kr.co.poolathome.core.service.setting.AppSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Slf4j
@Service
public class QnaServiceImpl implements QnaService {

  @Autowired
  private QnaRepository qnaRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AppSettingService appSettingService;

//  @Autowired
//  private EmailService emailService;

  @Override
  @Transactional
  public Qna create(Qna qna) {
    return qnaRepository.save(qna);
  }

  @Override
  @Transactional
  public Qna update(Qna qna) {

    if (qna.getId() == null) {
      throw new BadRequestException();
    }

    return qnaRepository.findById(qna.getId())
        .map(ori -> {

          BeanUtils.copyProperties(qna, ori, Qna.IGNORE_PROPERTIES);
          return qnaRepository.save(ori);
        }).orElseThrow(BadRequestException::new);
  }

  @Override
  @Transactional(readOnly = true)
  public Qna get(Locale locale, Long id) {

    return qnaRepository.findById(id)
        .map(qna -> {
          qna.lazy();
          return qna;
        }).orElse(null);
  }

  @Override
  @Transactional
  public void delete(Long id) {

    qnaRepository.findById(id).ifPresent(ebQna -> {
      ebQna.delete();
      qnaRepository.delete(ebQna);
    });
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Qna> page(Locale locale, Filter filter) {
    Page<Qna> page = qnaRepository.findAll(
        QnaPredicate.getInstance()
            .search(filter.getQuery())
            .startDate(filter.getStartDate())
            .endDate(filter.getEndDate())
            .values(),
        filter.getPageable());

    page.forEach(ebQna -> {
      ebQna.lazy();
    });
    return page;
  }
}
