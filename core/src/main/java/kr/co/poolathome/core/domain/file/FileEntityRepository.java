package kr.co.poolathome.core.domain.file;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface FileEntityRepository extends
    JpaRepository<FileEntity, Long>,
    QuerydslPredicateExecutor<FileEntity>,
    FileEntityRepositoryCustom {

  FileEntity findOneByFilename(String filename);

  FileEntity findOneByUrl(String url);
}
