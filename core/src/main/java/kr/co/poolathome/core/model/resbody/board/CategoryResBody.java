package kr.co.poolathome.core.model.resbody.board;

import kr.co.poolathome.core.model.BaseResponseBody;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.server.core.Relation;

@Schema(description = "카테고리")
@Relation(value = "category", collectionRelation = "categories")
@Slf4j
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryResBody extends BaseResponseBody {

    private static final long serialVersionUID = 6898460053912840759L;

    @Schema(description = "ID")
    private Long id;

    @Schema(description = "카테고리 명")
    private String name;

    @Schema(description = "아이콘 이미지")
    private String image;

    @Schema(description = "유형")
    private String type;
}
