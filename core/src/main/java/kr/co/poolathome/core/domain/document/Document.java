package kr.co.poolathome.core.domain.document;

import kr.co.poolathome.core.domain.AbstractEntityInternational;
import kr.co.poolathome.core.domain.International.InterText;
import kr.co.poolathome.core.domain.RestEntityBody;
import kr.co.poolathome.core.utils.DateUtils;
import kr.co.poolathome.core.utils.HtmlUtils;
import kr.co.poolathome.core.utils.StringUtils;
import kr.co.poolathome.core.config.serializer.JsonLocalDateDeserializer;
import kr.co.poolathome.core.config.serializer.JsonLocalDateSerializer;
//// // import kr.co.poolathome.core.domain.b2b.ServiceType;
import kr.co.poolathome.core.model.resbody.document.DocumentResBody;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Locale;

@Entity
@Getter
@Setter
@ToString(exclude = {})
public class Document extends AbstractEntityInternational<Long>
        implements RestEntityBody<DocumentResBody> {

    private static final long serialVersionUID = 6709048204298569714L;

    public static String[] IGNORE_PROPERTIES = {"id"};

    @Getter
    public enum Type {
        TERM("서비스 이용약관"),
        PRIVACY("개인정보 처리방침"),
        MARKETING("마케팅 활용 동의"),
        LEAVE("회원탈퇴 안내 및 동의"),
        LOCATION("위치기반서비스 이용약관"),
        CONTACT("문의하기 개인정보 처리방침"),
        ADULT("만 19세이상 이용 동의"),
        ;

        private final String value;

        Type(final String value) {
            this.value = value;
        }
    }

    @PrePersist
    public void prePersist() {
        if (StringUtils.isNotEmpty(this.content.getTextEnUs()))
            this.content.setTextEnUs(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextEnUs()));
        if (StringUtils.isNotEmpty(this.content.getTextJaJp()))
            this.content.setTextJaJp(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextJaJp()));
        if (StringUtils.isNotEmpty(this.content.getTextKoKr()))
            this.content.setTextKoKr(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextKoKr()));
        if (StringUtils.isNotEmpty(this.content.getTextZhCn()))
            this.content.setTextZhCn(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhCn()));
        if (StringUtils.isNotEmpty(this.content.getTextZhTw()))
            this.content.setTextZhTw(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhTw()));
    }

    @PreUpdate
    public void PreUpdate() {
        if (StringUtils.isNotEmpty(this.content.getTextEnUs()))
            this.content.setTextEnUs(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextEnUs()));
        if (StringUtils.isNotEmpty(this.content.getTextJaJp()))
            this.content.setTextJaJp(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextJaJp()));
        if (StringUtils.isNotEmpty(this.content.getTextKoKr()))
            this.content.setTextKoKr(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextKoKr()));
        if (StringUtils.isNotEmpty(this.content.getTextZhCn()))
            this.content.setTextZhCn(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhCn()));
        if (StringUtils.isNotEmpty(this.content.getTextZhTw()))
            this.content.setTextZhTw(HtmlUtils.convertLineSeparatorToBrTag(this.content.getTextZhTw()));
    }

    @Id
    @GeneratedValue
    @Column(unique = true)
    private Long id;

    @Enumerated
    @Column(columnDefinition = "TINYINT(1) default 0")
    private Type type; // 유형

//    @Column
//    @Enumerated(EnumType.STRING)
//    private ServiceType serviceType;    //B2B or B2C


    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "textKoKr", column = @Column(name = "titleKoKr", length = 100)),
            @AttributeOverride(name = "textEnUs", column = @Column(name = "titleEnUs", length = 100)),
            @AttributeOverride(name = "textJaJp", column = @Column(name = "titleJaJp", length = 100)),
            @AttributeOverride(name = "textZhCn", column = @Column(name = "titleZhCn", length = 100)),
            @AttributeOverride(name = "textZhTw", column = @Column(name = "titleZhTw", length = 100))
    })
    private InterText title; // 제목

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "textKoKr", column = @Column(name = "contentKoKr", columnDefinition = "LONGTEXT")),
            @AttributeOverride(name = "textEnUs", column = @Column(name = "contentEnUs", columnDefinition = "LONGTEXT")),
            @AttributeOverride(name = "textJaJp", column = @Column(name = "contentJaJp", columnDefinition = "LONGTEXT")),
            @AttributeOverride(name = "textZhCn", column = @Column(name = "contentZhCn", columnDefinition = "LONGTEXT")),
            @AttributeOverride(name = "textZhTw", column = @Column(name = "contentZhTw", columnDefinition = "LONGTEXT"))
    })
    private InterText content; // 내용

    @DateTimeFormat(pattern = DateUtils.FORMAT_DATE_UNIT_BAR)
    @JsonSerialize(using = JsonLocalDateSerializer.class)
    @JsonDeserialize(using = JsonLocalDateDeserializer.class)
    private LocalDate executeDate; // 시행일

    @Column(columnDefinition = "BIT(1) default 1")
    private boolean active; // 활성/비활성

    @Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);
        if (this.title != null)
            this.title.setLocale(this.locale);
        if (this.content != null)
            this.content.setLocale(this.locale);
    }

    @Override
    public void delete() {

    }

    @Override
    public void lazy() {

    }

    @Override
    public DocumentResBody toBody(Locale locale) {

        this.setLocale(locale);
        return DocumentResBody.builder()
                .id(this.id)
                .type(this.type)
                .title(this.title != null ? this.title.getValue() : null)
                .content(this.content != null ? this.content.getValue() : null)
                .executeDate(this.executeDate)
                .build();
    }
}
