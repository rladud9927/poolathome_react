package kr.co.poolathome.admin.service.wysiwyg;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface WysiwygService {

    List<Map<String, String>> imagesWysiwyg();
    String uploadWysiwyg(MultipartFile file);
    String uploadWysiwyg(MultipartFile file, String path);
    String uploadWysiwygFile(MultipartFile file);
    String uploadWysiwygFile(MultipartFile file, String path);
    void deleteWysiwyg(String src);
}
