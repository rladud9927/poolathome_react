package kr.co.poolathome.admin.service.wysiwyg;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import kr.co.poolathome.core.domain.wywiwyg.WysiwygFile;
import kr.co.poolathome.core.domain.wywiwyg.WysiwygFileRepository;
import kr.co.poolathome.core.model.file.FileMeta;
import kr.co.poolathome.core.model.file.FileUploadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class WysiwygServiceImpl implements WysiwygService {

  @Autowired
  private WysiwygFileRepository wysiwygFileRepository;

  @Autowired
  private FileUploadService fileUploadService;

  @Override
  public List<Map<String, String>> imagesWysiwyg() {
    List<Map<String, String>> results = new ArrayList<>();

    List<WysiwygFile> wysiwygFiles = Lists.newArrayList(wysiwygFileRepository.findAll(Sort.by(Sort.Direction.DESC, "createdDate")));
    wysiwygFiles.forEach(wysiwygFile ->
        results.add(ImmutableMap.of("url", wysiwygFile.getUrl()))
    );
    return results;
  }

  @Override
  @Transactional
  public String uploadWysiwyg(MultipartFile file) {

    FileMeta fileMeta = fileUploadService.uploadImage(file);
    WysiwygFile wysiwygFile = WysiwygFile.of(fileMeta.getUrl().toString(), fileMeta.getFilename(), fileMeta.getSize());
    wysiwygFileRepository.save(wysiwygFile);

    return fileMeta.getUrl().toString();
  }

  @Override
  @Transactional
  public String uploadWysiwyg(MultipartFile file, String path) {

    FileMeta fileMeta = fileUploadService.uploadImage(file);
    WysiwygFile wysiwygFile = WysiwygFile.of(fileMeta.getUrl().toString(), fileMeta.getFilename(), fileMeta.getSize());
    wysiwygFileRepository.save(wysiwygFile);

    return fileMeta.getUrl().toString();
  }

  @Override
  public String uploadWysiwygFile(MultipartFile file) {

    FileMeta fileMeta = fileUploadService.uploadImage(file);
    WysiwygFile wysiwygFile = WysiwygFile.of(fileMeta.getUrl().toString(), fileMeta.getFilename(), fileMeta.getSize());
    wysiwygFileRepository.save(wysiwygFile);

    return fileMeta.getUrl().toString();
  }

  @Override
  public String uploadWysiwygFile(MultipartFile file, String path) {

    FileMeta fileMeta = fileUploadService.uploadImage(file);
    WysiwygFile wysiwygFile = WysiwygFile.of(fileMeta.getUrl().toString(), fileMeta.getFilename(), fileMeta.getSize());
    wysiwygFileRepository.save(wysiwygFile);

    return fileMeta.getUrl().toString();
  }

  @Override
  public void deleteWysiwyg(String src) {

    boolean isDelete = fileUploadService.delete(src);

    if (isDelete) {
      Optional.ofNullable(wysiwygFileRepository.findByUrl(src))
          .ifPresent(wysiwygFile -> {
            wysiwygFileRepository.delete(wysiwygFile);
          });
    }
  }

}
