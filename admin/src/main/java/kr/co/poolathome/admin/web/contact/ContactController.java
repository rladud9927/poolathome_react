package kr.co.poolathome.admin.web.contact;

import kr.co.poolathome.core.config.exception.BadRequestException;
import kr.co.poolathome.core.domain.board.qna.Qna;
import kr.co.poolathome.core.service.board.qna.QnaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Locale;

@Slf4j
@Controller
@RequestMapping("/contact")
public class ContactController {

  @Autowired
  private QnaService qnaService;

  @GetMapping
  public ModelAndView contact(Model model) {
    Qna qna = new Qna();
    qna.setActive(true);

    model.addAttribute("qna", qna);
    model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));
    return new ModelAndView("/contact/PH_contact.ftl");
  }

  @PostMapping(value = "/create")
  public ModelAndView contactCreate(@Valid Qna qna,
                                    org.springframework.validation.BindingResult result,
                                    SessionStatus status) {

    if (result.hasErrors()) {
      result.getAllErrors().forEach(objectError -> {
        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
      });
      throw new BadRequestException(result.getObjectName());
    }

    log.debug("qna ::: {}", qna);

    qnaService.create(qna);
    status.setComplete();
    return new ModelAndView("redirect:/contact");
  }
}
