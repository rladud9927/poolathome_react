package kr.co.poolathome.admin.web.notice;

import kr.co.poolathome.core.config.freemarker.PageableModel;
import kr.co.poolathome.core.domain.board.post.Post;
import kr.co.poolathome.core.domain.setting.AppSetting;
import kr.co.poolathome.core.model.Filter;
import kr.co.poolathome.core.service.board.post.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/notice")
public class NoticeController {

  @Autowired
  private PostService postService;

  @GetMapping
  public ModelAndView page(Model model,
                           @ModelAttribute("setting") AppSetting setting,
                           @PageableDefault(size = 6, sort = {"top", "regTime"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Post> page = postService.page(setting.getDefaultLocale(), filter);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    model.addAttribute("types", Post.Type.values());

    return new ModelAndView("/notice/PH_notice.ftl");
  }

  @GetMapping("/news")
  public ModelAndView newsPage(Model model,
                           @ModelAttribute("setting") AppSetting setting,
                           @PageableDefault(size = 6, sort = {"top", "regTime"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Post> page = postService.page(setting.getDefaultLocale(), filter, Post.Type.NEWS);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    model.addAttribute("types", Post.Type.values());

    return new ModelAndView("/notice/PH_notice.ftl");
  }

  @GetMapping("/notice")
  public ModelAndView noticePage(Model model,
                               @ModelAttribute("setting") AppSetting setting,
                               @PageableDefault(size = 6, sort = {"top", "regTime"}, direction = Sort.Direction.DESC) Pageable pageable,
                               @RequestParam(required = false, defaultValue = "") String query,
                               @RequestParam(required = false) String startDate,
                               @RequestParam(required = false) String endDate) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Post> page = postService.page(setting.getDefaultLocale(), filter, Post.Type.NOTICE);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    model.addAttribute("types", Post.Type.values());

    return new ModelAndView("/notice/PH_notice.ftl");
  }

  @GetMapping("/faq")
  public ModelAndView faqPage(Model model,
                               @ModelAttribute("setting") AppSetting setting,
                               @PageableDefault(size = 6, sort = {"top", "regTime"}, direction = Sort.Direction.DESC) Pageable pageable,
                               @RequestParam(required = false, defaultValue = "") String query,
                               @RequestParam(required = false) String startDate,
                               @RequestParam(required = false) String endDate) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Post> page = postService.page(setting.getDefaultLocale(), filter, Post.Type.FAQ);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    model.addAttribute("types", Post.Type.values());

    return new ModelAndView("/notice/PH_notice.ftl");
  }

  @GetMapping(value = "/{id}")
  public ModelAndView noticeDetails(@PathVariable Long id,
                             @ModelAttribute("setting") AppSetting setting,
                             Model model) {

    model.addAttribute("post", postService.get(setting.getDefaultLocale(), id));
    return new ModelAndView("/notice/notice-details.ftl");
  }
}
