package kr.co.poolathome.admin.web;

import com.google.common.collect.ImmutableMap;
import jdk.nashorn.internal.parser.JSONParser;
import kr.co.poolathome.admin.service.wysiwyg.WysiwygService;
import kr.co.poolathome.core.config.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Controller
@RequestMapping("/admin/api/wysiwyg")
public class AdminAPIWysiwygController {

  @Autowired
  private WysiwygService wysiwygService;

  @ResponseBody
  @GetMapping
  public ResponseEntity<?> imagesWYSIWYG() {
    return ResponseEntity.ok(wysiwygService.imagesWysiwyg());
  }

  @ResponseBody
  @PostMapping
  public ResponseEntity<?> uploadWYSIWYG(@RequestParam("file") MultipartFile file) {
    return ResponseEntity.ok(ImmutableMap.of("link", wysiwygService.uploadWysiwyg(file)));
  }

  @ResponseBody
  @DeleteMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> deleteWYSIWYG(@RequestParam MultiValueMap<String, String> body) {

    if (body == null || body.getFirst("src") == null) {
      throw new BadRequestException();
    }

    String src = body.getFirst("src");
    wysiwygService.deleteWysiwyg(src);

    return ResponseEntity.ok(JSONParser.quote("Success"));
  }

  @ResponseBody
  @PostMapping("file")
  public ResponseEntity uploadFileWYSIWYG(@RequestParam("file") MultipartFile file) {
    return ResponseEntity.ok(ImmutableMap.of("link", wysiwygService.uploadWysiwyg(file)));
  }
}
