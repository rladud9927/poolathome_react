package kr.co.poolathome.admin.web.about;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/about")
public class AboutController {

  @GetMapping
  public ModelAndView about(Model model) {
    return new ModelAndView("/about/PH_about_1.ftl");
  }
}
