package kr.co.poolathome.admin.service.user;

import kr.co.poolathome.core.model.resbody.account.UserInfoResBody;

import java.util.List;

/**
 * 관리자 전용 비즈니스 로직
 */
//@PreAuthorize("hasAnyRole('ROLE_SUPER', 'ROLE_ADMIN')")
public interface UserAdminService {

  void leave(Long id);

  void removePrivacy(Long id);

  void restoreLeave(Long id);

  List<UserInfoResBody> list(String query);

  // 비밀번호 재설정 (아이디, 병견할 비밀번호)
  void resetPassword(Long id, String password);

}
