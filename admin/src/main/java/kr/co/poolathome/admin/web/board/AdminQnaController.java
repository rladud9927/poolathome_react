package kr.co.poolathome.admin.web.board;

import kr.co.poolathome.core.config.freemarker.PageableModel;
import kr.co.poolathome.core.service.board.qna.QnaService;
import com.google.common.collect.Lists;
import kr.co.poolathome.core.config.exception.BadRequestException;
import kr.co.poolathome.core.domain.board.qna.Qna;
import kr.co.poolathome.core.domain.board.qna.QnaNoMember;
import kr.co.poolathome.core.model.Filter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Slf4j
@Controller
@RequestMapping("/admin/qna")
public class AdminQnaController {

  @Autowired
  private QnaService qnaService;

  @GetMapping
  public ModelAndView page(Model model,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Qna> page = qnaService.page(null, filter);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    log.debug("data : {}", PageableModel.of(page, pageable, query).toModel());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("qna/list.ftl");
  }

  @GetMapping(value = "/create")
  public ModelAndView create(Model model) {
    Qna qna = new Qna();
    qna.setActive(true);

    model.addAttribute("qna", qna);
    model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));

    return new ModelAndView("qna/create.ftl");
  }

  @GetMapping(value = "update/{id}")
  public ModelAndView update(@PathVariable Long id,
                             Model model) {

    Qna qna = qnaService.get(null, id);

    model.addAttribute("qna", qna);
    model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));

    return new ModelAndView("qna/update.ftl");
  }

  @PostMapping(value = "/create")
  public ModelAndView qnaCreate(@Valid Qna qna,
                                org.springframework.validation.BindingResult result,
                                SessionStatus status) {

    if (result.hasErrors()) {
      result.getAllErrors().forEach(objectError -> {
        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
      });
      throw new BadRequestException(result.getObjectName());
    }

    log.debug("qna ::: {}", qna);

    qnaService.create(qna);
    status.setComplete();
    return new ModelAndView("redirect:/admin/qna");
  }

  @PostMapping(value = "/update")
  public ModelAndView qnaUpdate(@Valid Qna qna,
                                org.springframework.validation.BindingResult result,
                                SessionStatus status,
                                HttpServletRequest request) {

    if (result.hasErrors()) {
      result.getAllErrors().forEach(objectError -> {
        log.error("name -> " + objectError.getObjectName() + ", msg -> " + objectError.getDefaultMessage());
      });
      throw new BadRequestException(result.getObjectName());
    }
    qnaService.update(qna);
    status.setComplete();
    return new ModelAndView("redirect:/admin/qna/update/" + qna.getId() + "?success");
  }

  @PostMapping(value = "/delete")
  public ModelAndView delete(@RequestParam Long id) {
    qnaService.delete(id);
    return new ModelAndView("redirect:/admin/qna");
  }
}
