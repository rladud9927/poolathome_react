package kr.co.poolathome.admin.web.board;

import kr.co.poolathome.core.config.freemarker.PageableModel;
import kr.co.poolathome.core.domain.board.post.Post;
import kr.co.poolathome.core.domain.setting.AppSetting;
import kr.co.poolathome.core.service.board.post.PostService;
import kr.co.poolathome.core.utils.DataBinderUtils;
import kr.co.poolathome.core.model.Filter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/admin/post-notice")
public class AdminPostNoticeController {

  @Autowired
  private PostService postService;

  @GetMapping
  public ModelAndView page(Model model,
                           @ModelAttribute("setting") AppSetting setting,
                           @PageableDefault(size = 20, sort = {"top", "regTime"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<Post> page = postService.page(setting.getDefaultLocale(), filter);

    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("post-notice/list.ftl");
  }

  @GetMapping(value = "/create")
  public ModelAndView create(Model model) {
    Post post = new Post();
    post.setActive(true);

    model.addAttribute("types", Post.Type.values());
    model.addAttribute("post", post);

    return new ModelAndView("post-notice/create.ftl");
  }

  @GetMapping(value = "/update/{id}")
  public ModelAndView update(@PathVariable Long id,
                             @ModelAttribute("setting") AppSetting setting,
                             Model model) {

    model.addAttribute("types", Post.Type.values());
    model.addAttribute("post", postService.get(setting.getDefaultLocale(), id));
    return new ModelAndView("post-notice/update.ftl");
  }

  @PostMapping(value = "/create")
  public ModelAndView postCreate(@Valid Post post,
                                 org.springframework.validation.BindingResult result,
                                 SessionStatus status,
                                 @ModelAttribute("setting") AppSetting setting) {

    DataBinderUtils.objectValidate(result);

    log.debug("post ::: {}", post);

    postService.create(post);
    status.setComplete();
    return new ModelAndView("redirect:/admin/post-notice");
  }

  @PostMapping(value = "/update")
  public ModelAndView postUpdate(@Valid Post post,
                                 org.springframework.validation.BindingResult result,
                                 SessionStatus status) {

    DataBinderUtils.objectValidate(result);

    postService.update(post);
    status.setComplete();
    return new ModelAndView("redirect:/admin/post-notice/update/" + post.getId() + "?success");
  }

  @PostMapping(value = "/delete")
  public ModelAndView delete(@RequestParam Long id) {
    postService.delete(id);
    return new ModelAndView("redirect:/admin/post-notice");
  }
}
