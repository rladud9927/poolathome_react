package kr.co.poolathome.admin.web.terms;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/terms")
public class TermsController {

  @GetMapping
  public ModelAndView terms(Model model) {
    return new ModelAndView("/terms/PH_terms.ftl");
  }
}
