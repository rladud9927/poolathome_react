package kr.co.poolathome.admin.service.user;

import kr.co.poolathome.core.config.database.PwdEncConfig;
import kr.co.poolathome.core.domain.user.*;
import kr.co.poolathome.core.model.resbody.account.UserInfoResBody;
import kr.co.poolathome.core.service.account.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class UserAdminServiceImpl implements UserAdminService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserService userService;

  @Autowired
  private AuthorityRepository authorityRepository;

  @Autowired
  private PwdEncConfig pwdEncConfig;

  @Override
  @Transactional
  @PreAuthorize("hasRole('ROLE_SUPER')")
  public void leave(Long id) {
    userService.leave(id, "관리자에의해 탈퇴처리됨");
  }

  @Override
  @Transactional
  @PreAuthorize("hasRole('ROLE_SUPER')")
  public void removePrivacy(Long id) {
//    userService.removePrivacy(id);
  }

  @Override
  @Transactional
  public void restoreLeave(Long id) {
    userRepository.findById(id)
        .ifPresent(user -> {
          user.getUserDetailsMeta().setEnabled(true);
          user.getLeaveMeta().setLeave(false);
          user.getLeaveMeta().setLeaveTime(null);
          user.getLeaveMeta().setLeaveReason(null);
        });
  }

  @Override
  public List<UserInfoResBody> list(String query) {
    return null;
  }
  
  @Override
  @Transactional
  public void resetPassword(Long id, String password) {
    userRepository.findById(id)
        .ifPresent(user -> {
          user.setPassword(pwdEncConfig.getPasswordEncoder().encode(password));
          user.getUserDetailsMeta().setUpdatedPasswordDateTime(LocalDateTime.now());
        });
  }
}
