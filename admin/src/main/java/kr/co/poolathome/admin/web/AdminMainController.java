package kr.co.poolathome.admin.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class AdminMainController {

  @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView mainView(){
    return new ModelAndView("redirect:/admin/post-notice");
  }
}
