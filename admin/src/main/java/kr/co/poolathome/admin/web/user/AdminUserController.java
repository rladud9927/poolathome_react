package kr.co.poolathome.admin.web.user;

import kr.co.poolathome.admin.service.user.UserAdminService;
import kr.co.poolathome.core.config.freemarker.PageableModel;
import kr.co.poolathome.core.domain.setting.AppSetting;
import kr.co.poolathome.core.domain.user.ApprovalStatus;
import kr.co.poolathome.core.domain.user.Authority;
import kr.co.poolathome.core.domain.user.TermsAgree;
import kr.co.poolathome.core.domain.user.User;
import kr.co.poolathome.core.model.Filter;
import kr.co.poolathome.core.service.account.user.UserService;
import kr.co.poolathome.core.service.setting.AppSettingService;
import kr.co.poolathome.core.service.user.AuthorityService;
import kr.co.poolathome.core.utils.DataBinderUtils;
import kr.co.poolathome.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/admin/user")
public class AdminUserController {

  @Autowired
  private UserAdminService userAdminService;

  @Autowired
  private UserService userService;

  @Autowired
  private AuthorityService authorityService;

  @Autowired
  private AppSettingService appSettingService;

  @GetMapping
  public ModelAndView page(Model model,
                           @ModelAttribute("setting") AppSetting setting,
                           @PageableDefault(size = 20, sort = {"createdDate"}, direction = Sort.Direction.DESC) Pageable pageable,
                           @RequestParam(required = false, defaultValue = "") String query,
                           @RequestParam(required = false) String startDate,
                           @RequestParam(required = false) String endDate,
                           @RequestParam(required = false) Authority.Role role) {

    Filter filter = new Filter(pageable, query, startDate, endDate);
    Page<User> page = userService.page(setting.getDefaultLocale(), filter);

    // Default
    model.addAttribute("data", PageableModel.of(page, pageable, query).toModel());
    model.addAttribute("page", pageable.getPageNumber());

    model.addAttribute("roles", Authority.Role.values());
    model.addAttribute("role", role);

    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("admin/user/list.ftl");
  }

  @GetMapping(value = "/create")
  public ModelAndView create(Model model,
                             @ModelAttribute("setting") AppSetting setting) {
    User user = new User();
    TermsAgree ta = TermsAgree.builder()
        .taService(true)
        .taPrivacy(true)
        .taEft(true)
        .taLocation(true)
        .build();
    user.setTermsAgree(ta);

    model.addAttribute("user", user);
    model.addAttribute("approvalStatus", ApprovalStatus.values());
    model.addAttribute("authorities", Authority.Role.values());
    model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));

    return new ModelAndView("admin/user/create.ftl");
  }

  @GetMapping("update/{idUser}")
  public ModelAndView update(@PageableDefault(size = 10, sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
                             @PathVariable Long idUser,
                             @ModelAttribute("setting") AppSetting setting,
                             @RequestParam(required = false) String startDate,
                             @RequestParam(required = false) String endDate,
                             @RequestParam(required = false, defaultValue = "") String query,
                             Model model) {
    Locale defaultLocale = appSettingService.getDefaultLocale();
    Filter filter = new Filter(pageable, query, startDate, endDate);
    User user = userService.get(idUser);

    model.addAttribute("user", user);
    model.addAttribute("approvalStatus", ApprovalStatus.values());
    model.addAttribute("authorities", Authority.Role.values());
    model.addAttribute("locales", Arrays.asList(Locale.getAvailableLocales()));
    model.addAttribute("page", pageable.getPageNumber());
    // Search filter
    model.addAttribute("startDate", startDate);
    model.addAttribute("endDate", endDate);

    return new ModelAndView("admin/user/update.ftl");
  }

  @PostMapping(value = "/create")
  public ModelAndView createSubmit(@Valid User user,
                                   @ModelAttribute("setting") AppSetting setting,
                                   org.springframework.validation.BindingResult result,
                                   SessionStatus status) {

    DataBinderUtils.objectValidate(result);
    userService.create(user);
    status.setComplete();
    return new ModelAndView("redirect:/admin/user");
  }

  @PostMapping("update")
  public ModelAndView updateSubmit(@Valid User user,
                                   @ModelAttribute("setting") AppSetting setting,
                                   org.springframework.validation.BindingResult result,
                                   SessionStatus status) {

    DataBinderUtils.objectValidate(result);
    userService.update(user);
    status.setComplete();
    return new ModelAndView("redirect:/admin/user/update/" + user.getId() + "?success");
  }

  @ResponseBody
  @PostMapping("update/password")
  public ResponseEntity<?> updatePassword(@RequestBody Map<String, String> data) {

    Long idUser = StringUtils.isEmpty(data.get("id")) ? null : Long.valueOf(data.get("id"));
    String password = StringUtils.isEmpty(data.get("password")) ? null : data.get("password");

    if (idUser == null || password == null) {
      return ResponseEntity.badRequest().build();
    }

    userAdminService.resetPassword(idUser, password);
    return ResponseEntity.ok().build();
  }

  @PreAuthorize("hasRole('ROLE_SUPER')")
  @PostMapping("leave")
  public ModelAndView leave(HttpServletRequest request,
                            @RequestParam Long id) {
    userAdminService.leave(id);
    return new ModelAndView("redirect:" + request.getHeader("referer"));
  }

  @PreAuthorize("hasRole('ROLE_SUPER')")
  @PostMapping("remove-privacy")
  public ModelAndView removePrivacy(HttpServletRequest request,
                                    @RequestParam Long id) {
    userAdminService.removePrivacy(id);
    return new ModelAndView("redirect:" + request.getHeader("referer"));
  }

  @PreAuthorize("hasRole('ROLE_SUPER')")
  @PostMapping("restore-leave")
  public ModelAndView restoreLeave(HttpServletRequest request,
                                   @RequestParam Long id) {
    userAdminService.restoreLeave(id);
    return new ModelAndView("redirect:" + request.getHeader("referer"));
  }

  @PreAuthorize("hasRole('ROLE_SUPER')")
  @PostMapping("delete")
  public ModelAndView delete(@RequestParam Long id) {

    userService.delete(id);
    return new ModelAndView("redirect:/admin/user");
  }
}
