package kr.co.poolathome.admin.web.main;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("/")
public class MainController {

  @GetMapping
  public ModelAndView main(Model model) {
    return new ModelAndView("/main/PH_main_1.ftl");
  }
}
