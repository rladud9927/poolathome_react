var mainUi = {
  getIcons: function (i) {
    var text =
      '<i class="icon icon-sm-21 icon-md-30 icon-65 icon-furniture-' +
      i +
      ' px-3 px-lg-5 mx-lg-4"></i>';
    return text;
  },

  //-------------
  getMetaContents: function (item) {
    var text = "";
    text +=
      '<div class="col-4 d-flex justify-content-center justify-content-lg-start align-items-center mb-4">';
    text += "  <div";
    text +=
      '    class="rounded-circle bg-light mr-2 circle-sm-36 circle-58 d-flex justify-content-center align-items-center">';
    text +=
      '    <i class="icon icon-sm-16 icon-md-22 icon-34 icon-' +
      item.icon +
      '"></i>';
    text += "  </div>";
    text +=
      '  <span class="intro-text text-14 text-lg-18 text-xl-22 text-primary">';
    text += item.text;
    text += "  </span>";
    text += "</div>";

    return text;
  },

  //-------------
  getCards: function (item) {
    var text = "";
    text += '<div class="col-12 col-md-4 mt-4">';
    text +=
      ' <div class="bg-white p-4 d-flex flex-column align-items-center rounded-40 h-100">';
    text +=
      '<div class="circle-sm-96 circle-118 bg-light-blue mb-3 d-flex justify-content-center align-items-center">';
    text += ' <i class="icon icon-' + item.icon + ' icon-sm-40 icon-50"></i>';
    text += " </div>";

    text +=
      '<h6 class="font-weight-bold text-22 text-lg-24 text-xl-32 mb-3">' +
      item.title +
      "</h6>";

    text +=
      '<span class="text-gray text-center text-14 text-lg-16 text-xl-20">' +
      item.text +
      "</span>";

    text += "</div>";
    text += "</div>";

    return text;
  },

  //-------------
  getPins: function (item, idx) {
    var text = "";
    text += "<button ";
    text += 'type="button"';
    text += 'class="pin-buttons pin-' + idx + ' btn p-0 position-absolute "';
    text += 'data-html="true"';
    text += 'data-container="body"';
    text += 'data-toggle="popover"';
    text += 'data-placement="top"';
    text += 'title="' + item.title + '"';
    text += 'data-content="' + item.location + '<br/>' + '배송권역: ' + item.area + '"';
    text += 'data-trigger="hover"';
    text +=
      'style="top:' +
      item.top +
      "%; left: " +
      item.left +
      "%; bottom:" +
      item.bottom +
      "%;right:" +
      item.right +
      '%; z-index: 50">';
    text += '<div class="pins shadow"><div class="inner-pin"></div></div>';
    text += "</button>";
    return text;
  },

  //-------------
  getFilterButtons: function (item) {
    var text = "";
    text += ' <button class=" btn-' + item.type + "";
    text += " pill";
    text += " bg-white";
    text += " border";
    text += " text-gray text-14";
    text += " py-1";
    text += " w-sm-96 w-120";
    text += ' mx-1 mx-md-2 ">';
    text += " <span>" + item.text + "</span>";
    text += " </button>";
    return text;
  },

  //-------------
  getCustomerCards: function (item, i) {
    var text = "";
    text += '<div class="col-6 col-lg-3">';
    text +=
      '<div class="bg-white rounded-20 mb-4 py-md-3 px-md-5 p-lg-2 py-xl-3 px-xl-5">';
    text += '<img src="' + item.src + '" class="w-100 p-4 customer-' + i + '">';
    text += "</div></div>";

    return text;
  },
};
