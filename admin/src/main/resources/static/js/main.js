function main() {
  // popover
  $(function () {
    $('[data-toggle="popover"]').popover();
  });

  // icons
  var loopCount = 0;
  for (let i = 1; i < 8; i++) {
    if (i === 7) {
      i = 1;
      loopCount++;
    }
    if (loopCount == 2) {
      break;
    }
    var elem = mainUi.getIcons(i);
    $(".icon-list").append(elem);
  }

  // animation
  function animate() {
    var dWidth = document.body.clientWidth;
    var aniStartPart = document.querySelector(".animate-start").offsetTop;

    var showItems = [
      "phone",
      "text",
      "cloud",
      "car",
      "first",
      "second",
      "third",
      "talk",
    ];

    if (pageYOffset > aniStartPart / 1.6) {
      if (dWidth < 991) {
        document.querySelector(".elem-title").classList.add("show-title");
      }
      showItems.forEach((item) => {
        document.querySelector(".elem-" + item).classList.add("show-" + item);
      });
    } else {
    }
  }

  window.onscroll = () => {
    animate();

    // main_GNB
    var scrollBool = pageYOffset > 2;
    document.querySelector(".global-nav").style.backgroundColor = scrollBool
      ? "#fff"
      : "rgba(0,0,0,0)";
  };

  // meta-contents
  var introItem = [
    {
      icon: "bulb",
      text: "전문설치",
    },
    {
      icon: "calendar",
      text: "원하는날",
    },
    {
      icon: "coin",
      text: "가격만족",
    },
    {
      icon: "setting",
      text: "신속설치",
    },
    {
      icon: "box",
      text: "친절배송",
    },
    {
      icon: "good",
      text: "배송조회",
    },
  ];

  for (let i = 0; i < introItem.length; i++) {
    var elem = mainUi.getMetaContents(introItem[i]);
    $("#intros").append(elem);
  }

  // 제공 서비스
  var providesService = [
    {
      icon: "delivery",
      title: "3PL",
      text: "WMS 기반 입출고 및 관리업무 서비스, 컨테이너 운송, 납품대행, 유통 가공 등 종합물류서비스를 제공합니다.",
    },
    {
      icon: "install",
      title: "설치배송",
      text: "가전, 가구, 헬스케어 제품 등 단순 배송에서 빌트인 전문설치까지 서비스를 제공하고 있습니다.",
    },
    {
      icon: "builtin",
      title: "반품물류",
      text: "반품된 상품에 대한 재판매가 가능하도록 허브센터 내에서 리퍼비시 서비스를 제공 및 불량재고에 대한 판매망을 연결해드리고 있습니다.",
    },
    {
      icon: "as",
      title: "AS",
      text: "사후관리 전담팀을 전국적으로 운영하고 있어, 설치배송 외에 AS까지 원스톱으로 위탁하실 수 있습니다.",
    },
    {
      icon: "moving",
      title: "이전설치",
      text: "다년간 다양한 상품에 대한 노하우를 통해 개인고객의 소중한 제품을 원하는 곳에 재설치해드립니다.",
    },
    {
      icon: "diy",
      title: "유통가공",
      text: "상품의 납품을 위한 재포장, 라벨링 등 유통가공 서비스를 수행합니다.",
    },
    {
      icon: "file",
      title: "리퍼비시",
      text: "단순변심, 불량으로 반품된 가전제품의 양품화를 수행할 수 있는 리퍼실을 운영하고 있습니다.",
    },
  ];

  for (let i = 6; i >= 0; i--) {
    var elem = mainUi.getCards(providesService[i]);
    $("#provide-services").prepend(elem);
  }

  // map
  var pins = [
    // pin-0
    {
      top: "25",
      left: "24",
      right: "unset",
      bottom: "unset",
      title: "용인HUB센터",
      location: "경기 용인시",
      area: "전국",
    },

    // pin-1
    {
      top: "25",
      left: "26",
      right: "unset",
      bottom: "unset",
      title: "수도권1센터",
      location: "경기 용인시",
      area: "서울, 인천, 경기",
    },

    // pin-2
    {
      top: "30",
      left: "26",
      right: "unset",
      bottom: "unset",
      title: "수도권2센터",
      location: "경기 안성시",
      area: "서울, 인천, 경기",
    },

    // pin-3
    {
      top: "unset",
      left: "65",
      right: "unset",
      bottom: "34",
      title: "경남센터",
      location: "경남 김해시",
      area: "부산, 울산, 경남",
    },

    // pin-4
    {
      top: "unset",
      left: "54",
      right: "unset",
      bottom: "50",
      title: "경북센터",
      location: "경북 칠곡군",
      area: "대구, 경북",
    },

    // pin-5
    {
      top: "unset",
      left: "30",
      right: "unset",
      bottom: "57",
      title: "충청센터",
      location: "세종시",
      area: "대전, 세종, 충남, 충북",
    },

    // pin-6
    {
      top: "unset",
      left: "17",
      right: "unset",
      bottom: "37",
      title: "전남센터",
      location: "전남 장성군",
      area: "광주, 전남",
    },

    // pin-7
    {
      top: "unset",
      left: "20",
      right: "unset",
      bottom: "49",
      title: "전북센터",
      location: "전북 익산시",
      area: "전북",
    },

    // pin-8
    {
      top: "unset",
      left: "7",
      right: "unset",
      bottom: "5",
      title: "제주센터",
      location: "제주 제주시",
      area: "제주",
    },

    // pin-9
    {
      top: "22",
      left: "42",
      right: "unset",
      bottom: "unset",
      title: "강원센터",
      location: "강원 원주시",
      area: "강원",
    },
  ];
  pins.forEach((items, idx) => {
    var elem = mainUi.getPins(items, idx);
    $("#map-section").append(elem);
  });

  // pin 호버시 지역 색
  function mapHover(selector, area) {
    if (document.querySelector(selector)) {
      document
        .querySelector(selector)
        .addEventListener("mouseover", function () {
          document.querySelector(area).style.fill = "#dfe4f7";
        });
      document
        .querySelector(selector)
        .addEventListener("mouseout", function () {
          document.querySelector(area).style.fill = "#E8EDF2";
        });
    }
  }

  mapHover(".pin-0", ".gyeonggi");
  mapHover(".pin-0", ".seoul");

  mapHover(".pin-1", ".gyeonggi");
  mapHover(".pin-1", ".seoul");

  mapHover(".pin-2", ".gyeonggi");
  mapHover(".pin-2", ".seoul");

  mapHover(".pin-3", ".gyeong-nam");
  mapHover(".pin-3", ".ulsan");
  mapHover(".pin-3", ".busan");
  mapHover(".pin-3", ".geoje");

  mapHover(".pin-4", ".gyeong-buk");
  mapHover(".pin-4", ".daegu");

  mapHover(".pin-5", ".chung-nam");
  mapHover(".pin-5", ".chung-buk");
  mapHover(".pin-5", ".daejeon");

  mapHover(".pin-6", ".jeon-nam");
  mapHover(".pin-6", ".gwangju");

  mapHover(".pin-7", ".jeon-buk");

  mapHover(".pin-8", ".jeju");

  mapHover(".pin-9", ".gangwon");

  // 고객사 버튼
  var filterButtons = [
    {
      text: "전체",
      type: "all",
    },
    {
      text: "생활가전류",
      type: "appliances",
    },
    {
      text: "가구",
      type: "furniture",
    },
    {
      text: "헬스가구",
      type: "health",
    },
  ];

  // 고객사
  var customers = [
    {
      src: "../assets/image/customer1@3x.png",
      color: "../assets/image/color1.png",
      type: "health",
    },
    {
      src: "../assets/image/customer2@3x.png",
      color: "../assets/image/color2.png",
      type: "appliances",
    },
    {
      src: "../assets/image/customer3@3x.png",
      color: "../assets/image/color3.png",
      type: "furniture",
    },
    {
      src: "../assets/image/customer4@3x.png",
      color: "../assets/image/color4.png",
      type: "appliances",
    },
    {
      src: "../assets/image/customer5@3x.png",
      color: "../assets/image/color5.png",
      type: "furniture",
    },
    {
      src: "../assets/image/customer6@3x.png",
      color: "../assets/image/color6.png",
      type: "furniture",
    },
    {
      src: "../assets/image/customer7@3x.png",
      color: "../assets/image/color7.png",
      type: "furniture",
    },
    {
      src: "../assets/image/customer8@3x.png",
      color: "../assets/image/color8.png",
      type: "appliances",
    },
    {
      src: "../assets/image/customer9@3x.png",
      color: "../assets/image/color9.png",
      type: "furniture",
    },
    {
      src: "../assets/image/customer10@3x.png",
      color: "../assets/image/color10.png",
      type: "health",
    },
    {
      src: "../assets/image/customer11@3x.png",
      color: "../assets/image/color11.png",
      type: "health",
    },
    {
      src: "../assets/image/customer12@3x.png",
      color: "../assets/image/color12.png",
      type: "furniture",
    },
    {
      src: "../assets/image/customer13@3x.png",
      color: "../assets/image/color13.png",
      type: "health",
    },
    {
      src: "../assets/image/customer14@3x.png",
      color: "../assets/image/color14.png",
      type: "health",
    },
    {
      src: "../assets/image/customer15@3x.png",
      color: "../assets/image/color15.png",
      type: "health",
    },
    {
      src: "../assets/image/customer16@3x.png",
      color: "../assets/image/color16.png",
      type: "health",
    },
    {
      src: "../assets/image/customer17@3x.png",
      color: "../assets/image/color17.png",
      type: "health",
    },
    {
      src: "../assets/image/customer18@3x.png",
      color: "../assets/image/color18.png",
      type: "appliances",
    },
    {
      src: "../assets/image/customer19@3x.png",
      color: "../assets/image/color19.png",
      type: "furniture",
    },
    {
      src: "../assets/image/customer20@3x.png",
      color: "../assets/image/color20.png",
      type: "health",
    },
  ];
  var currentType = "all";
  var filtered = customers.filter((x) => x.type === currentType);

  // 고객사 카드
  function customerCard(arr) {
    $("#customers").empty();
    arr.forEach((item, idx) => {
      var elem = mainUi.getCustomerCards(item, idx);
      $("#customers").append(elem);
      customerHover(item, idx);
    });
  }

  customerCard(filtered && filtered.length > 0 ? filtered : customers);

  var reset = false;
  // 버튼 클릭 이벤트
  filterButtons.forEach((button, i) => {
    var elem = mainUi.getFilterButtons(button);
    $(".filter-buttons").append(elem);
    var btn = document.querySelector(".btn-" + button.type);

    function resetActive(btnName) {
      document
        .querySelector(".btn-" + btnName)
        .classList.remove("tab-active-primary");
    }

    btn.addEventListener("click", function () {
      resetActive("all");
      resetActive("appliances");
      resetActive("furniture");
      resetActive("health");
      currentType = button.type;
      btn.classList.add("tab-active-primary");
      filtered = customers.filter((x) => x.type === currentType);
      customerCard(filtered && filtered.length > 0 ? filtered : customers);
    });
  });
  document.querySelector(".btn-all").classList.add("tab-active-primary");

  // 고객사 hover
  function customerHover(item, i) {
    var elem = document.querySelector(".customer-" + i);
    if (elem) {
      elem.addEventListener("mouseover", function () {
        elem.src = item.color;
      });
      elem.addEventListener("mouseout", function () {
        elem.src = item.src;
      });
    }
  }
}

document.addEventListener("readystatechange", function () {
  if (document.readyState === "complete") {
    main();
  }
});
