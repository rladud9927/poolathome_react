const slickOptions = {
  centerMode: true,
  slidesToShow: 1,
  arrows: false,
  dots: true,
  // todo
  responsive: [
    // 반응형 웹 구현 옵션
    {
      breakpoint: 960, //화면 사이즈 960px
      settings: {
        //위에 옵션이 디폴트 , 여기에 추가하면 그걸로 변경
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 768, //화면 사이즈 768px
      settings: {
        //위에 옵션이 디폴트 , 여기에 추가하면 그걸로 변경
        slidesToShow: 2,
      },
    },
  ],
};

function setSlick(category) {
  const systemSlickItem = document.querySelector(`.${category}-slick`);
  window.innerWidth < 756
    ? systemSlickItem.classList.add(`set-${category}-slick`)
    : systemSlickItem.classList.remove(`set-${category}-slick`);

  $(`.set-${category}-slick`).slick(slickOptions);
}

$(document).on("click", ".modal-button", function () {
  $(".modal-slick").not(".slick-initialized").slick();
  $("#MobileIntroModal").modal("show");
});

document.addEventListener("readystatechange", function () {
  if (document.readyState === "complete") {
    // 외부 슬릭
    setSlick("service");
    setSlick("system");
    $(".modal").on("shown.bs.modal", function (e) {
      setTimeout(() => {
        const slicks = $(e.target).find(".modal-slick");
        if (!slicks.hasClass("slick-initialized")) {
          slicks.slick(slickOptions);
        }
        slicks.slick("setPosition");
      }, 0);
    });
    $(".modal").on("hide.bs.modal", function (e) {
      const slicks = $(e.target).find(".modal-slick");
      console.log("slicks", slicks);
      if (slicks.hasClass("slick-initialized")) {
        $(slicks[0]).slick("unslick");
      }
    });
  }
});
