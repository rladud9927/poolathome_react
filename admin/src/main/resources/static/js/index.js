const navs = [
  // {
  //   text: "HOME DESIGN",
  //   link: "/home/list.html",

  // },
  {
    text: "HOME-TREST",
    link: "/home/list.html",
    id: "home-design",
    lnb: [
      {
        text: "Total Design",
        link: "/",
      },
      {
        text: "Bath Design Package",
        link: "/",
      },
      {
        text: "Kitchen Design Package",
        link: "/",
      },
    ],
  },
  {
    text: "GALLERY",
    link: "/gallery/list.html",
  },
  {
    text: "COMMUNITY",
    link: "/gallery/list.html",
    id: "community",
    lnb: [
      {
        text: "Interior Counseling",
        link: "/",
      },
      {
        text: "Interior Contest",
        link: "/",
      },
    ],
  },
  {
    text: "SPACE KIT",
    link: "/space/list",
  },
  {
    text: "MAGAZINE",
    link: "/magazine",
    id: "magazine",
    lnb: [
      {
        text: "Open House",
        link: "/",
      },
      {
        text: "Space Makeover",
        link: "/",
      },
    ],
  },
  {
    text: "CONTENTS",
    link: "/contents/home/list",
  },
];

const ftrNavs = [
  {
    text: "서비스 이용 약관",
    link: "/terms",
  },
  {
    text: "개인정보 수집 및 이용동의",
    link: "/terms",
  },
  {
    text: "전자금융거래 이용약관",
    link: "/terms",
  },
  {
    text: "개인정보 제3자 제공",
    link: "/terms",
  },
  {
    text: "공지사항",
    link: "/notice/list",
  },
  {
    text: "FAQ",
    link: "/faq/list",
  },
];
/**
 * GlobalNav 초기화 메서드입니다.
 */
const setGnv = function () {
  const navContent = $(`
  <div class="container-fluid justify-content-between px-lg-5">
      <a class="navbar-brand" href="/">
        
      </a>
      <button
        class="navbar-toggler"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
        class="ml-auto mr-0"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div
        class="collapse navbar-collapse flex-grow-0"
        id="navbarSupportedContent"
      >
        <ul
          id="gnvLinks"
          class="navbar-nav align-items-center me-auto mb-lg-n2"
        ></ul>
      </div>
      <div class="d-none d-md-flex align-items-center">
        <div class="icons-wrap d-none d-md-flex align-items-md-center">
          <div
            class="dropdown"
            id="notification"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <button
              class="btn btn-link px-0 mr-2 position-relative"
              aria-labelledby="notification"
            >
              <i class="icon icon-bell">@</i>
              <span class="noti-badge">8</span>
            </button>
            <ul
              class="dropdown-menu dropdown-menu-right notification-wrapper mt-3"
            >
              <li class="notification-el is-new">
                <div class="row justify-content-start px-3">
                  <div class="px-0 col-9 d-flex flex-column">
                    <span class="notification-type">[안내]</span>
                    <p class="text-truncate notification-desc">
                      안내 내용 샘플 텍스트 안내 내용 샘플 텍스트 안내 내용 샘플
                      텍스트 안내 내용 샘플 텍스트
                    </p>
                  </div>
                  <div class="px-0 col-3 text-right">
                    <span class="notification-time">7분 전</span>
                  </div>
                </div>
              </li>
              <li class="notification-el">
                <div class="row justify-content-start px-3">
                  <div class="px-0 col-9 d-flex flex-column">
                    <span class="notification-type">[안내]</span>
                    <p class="text-truncate notification-desc">
                      안내 내용 샘플 텍스트 안내 내용 샘플 텍스트 안내 내용 샘플
                      텍스트 안내 내용 샘플 텍스트
                    </p>
                  </div>
                  <div class="px-0 col-3 text-right">
                    <span class="notification-time">2020.12.31</span>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="icons-wrap d-none d-md-flex align-items-md-center">
          <div
            class="dropdown"
            id="lnb-user"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <button
              class="btn btn-link px-0 mr-2 position-relative"
              aria-labelledby="lnb-user"
            >
              <i class="icon icon-bell">@</i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right dropdown-lnb mt-3">
              <li class="text-noto">
                <button class="btn btn-text p-0 w-100 h-100">마이스페이스</button>
              </li>
              <li class="text-noto">
                <button class="btn btn-text p-0 w-100 h-100">로그아웃</button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  `);
  $("#gnv").append(navContent);
  navs.forEach((nav) => {
    const li = $('<li class="nav-item mx-4"></li>');
    const a = $(
      `<a class="nav-link text-smg" href="${nav.link}">${nav.text}</a>`
    );
    const dropdown = $(`
    <div
      class="dropdown"
      id="${nav.id}"
      data-toggle="dropdown"
      aria-haspopup="true"
      aria-expanded="false"
    >
      <button
        class="btn btn-text px-0 mr-2 position-relative text-smg"
        aria-labelledby="${nav.id}"
      >
        ${nav.text}
      </button>
      <ul class="dropdown-menu dropdown-lnb mt-3">
        ${$.each(nav.lnb, function (index, item) {
          $(
            `
              <li class="lnb-item">
                <a class="btn btn-text p-0 text-smg" href="${item.link}">${item.text}</a>
              </li>
            `
          );
        })}
      </ul>
    </div>
    `);

    if (nav.lnb) {
      li.append(dropdown);
    } else {
      li.append(a);
    }
    $("#gnvLinks").append(li);
  });
};
const setFtr = function () {
  const navContent =
    $(`<div class="container-fluid justify-content-between px-lg-5 py-3">
  <a class="navbar-brand" href="#">
    
  </a>
  <div class="d-flex flex-column align-items-end">
    <div class="flex-grow-0">
      <ul id="ftrLinks" class="navbar-nav mb-0">
      </ul>
    </div>
    <ul class="list-unstyled d-flex pt-3 pb-lg-0 m-0 opacity-">
      <li>상호 : 회사명</li>
      <li>대표 : 대표자명</li>
      <li>사업자등록번호 : 123-45-12345</li>
      <li>주소 : 서울특별시 강서구 (우: 07806)</li>
      <li>팩스 : 02-123-1234</li>
      <li>통신판매업신고번호 : 제 2019-서울강서-0001호</li>
      <li>서비스이용문의: 02-1234-1234</li>
      <li>이메일 : cs@a.com</li>
    </ul>
  </div>
 </div>`);

  $("#ftr").append(navContent);

  ftrNavs.forEach((nav) => {
    const li = $('<li class="nav-item ml-lg-3 text-right"></li>');
    const a = $(
      `<a class="nav-link px-0 pb-1" href="${nav.link}">${nav.text}</a>`
    );

    $("#ftrLinks").append(li);
  });
};

$(document).ready(function () {
  setGnv();
  setFtr();
});

// hover-img 에서 dim 텍스트 없애기
$(document).ready(function () {
  $(".hover-img").on("mouseenter", function () {
    $(".hover-img .info").addClass("hide");
  });
});
