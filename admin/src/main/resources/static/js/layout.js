function layout() {
  function header() {
    var text = "";
    text +=
      '  <div class="container py-2 d-flex justify-content-between align-items-center">';
    text += "    <nav";
    text +=
      '      class="navbar navbar-expand-lg w-100 d-flex justify-content-between px-3">';
    text += '      <a class="navbar-brand" href="/">';
    text +=
      '        <img src="../assets/icon/logo-3.svg" alt="" class="nav-logo" />';
    text += "      </a>";

    text += '      <button class="navbar-toggler p-0" type="button" ';
    text += '      data-toggle="collapse" data-target="#navbarNavAltMarkup" ';
    text +=
      '      aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">';
    text += '      <div class="nav-stick"></div>';
    text += '      <div class="nav-stick my-1"></div>';
    text += '      <div class="nav-stick"></div>';
    text += "      </button>";

    text += "      <div";
    text +=
      '        class="collapse navbar-collapse d-flex justify-content-end"';
    text += '        id="navbarNavAltMarkup">';
    text += "        <div";
    text += '          class="';
    text += "            collapse";
    text += "            navbar-collapse";
    text += "            text-center";
    text += "            d-lg-flex";
    text += '            justify-content-end"';
    text += '          id="navbarNavAltMarkup">';
    text += "          <div";
    text += '            class="';
    text += "              navbar-nav";
    text += "              d-lg-flex";
    text += "              align-items-center";
    text += "              mt-2 mt-md-3 mt-lg-0";
    text += '              py-5 py-md-0">';
    text += "            <a";
    text += '              class="nav-link text-primary mx-4"';
    text += '              href="/about">';
    text += "                회사소개";
    text += "                </a>";
    text +=
      '            <a class="nav-link text-primary" href="notice">고객지원</a>';
    text += '            <div class="d-flex mt-4 mt-lg-0">';
    text +=
      '              <a class="nav-link text-primary" href="contact">';
    text += "                <button";
    text += '                  class="';
    text += "                    btn";
    text += "                    pill";
    text += "                    bg-primary";
    text += "                    ml-n2 ml-lg-4";
    text += "                    px-4";
    text += '                    text-white text-15">';
    text += "                    이용문의";
    text += "                </button>";
    text += "              </a>";
    text += '              <a class="nav-link" href="" onclick="soon()">';
    text += "                <button";
    text +=
      '                  class="btn pill bg-secondary ml-3 px-4 text-white text-15">';
    text += "                  WINUS";
    text += "                </button>";
    text += "              </a>";
    text += "            </div>";
    text += "          </div>";
    text += "        </div>";
    text += "      </div>";
    text += "    </nav>";
    text += "  </div>";

    return text;
  }
  $(".global-nav").append(header());

  // main_GNB - 모바일에서 toggle-nav 배경 색
  if (document.querySelector("#ph_main")) {
    document
      .querySelector("#ph_main .navbar-toggler")
      .addEventListener("click", () => {
        if (pageYOffset == 0) {
          document.querySelector(".global-nav").classList.toggle("bg-white");
        }
      });
  }

  // footer
  function footer() {
    var text = "";
    text += '<section class="bg-primary text-center py-5">';
    text +=
      '    <div class="d-flex justify-content-center align-items-center mb-4">';
    text += "      <span";
    text +=
      '        class="font-weight-bold text-white text-16 text-lg-18 text-xl-22">';
    text += "        풀앳홈";
    text += "      </span>";
    // text += '      <div class="mx-4 text-12 text-white">|</div>';
    // text += '      <i class="icon icon-sm-14 icon-16 icon-facebook"></i>';
    // text += '      <i class="icon icon-sm-14 icon-16 icon-instagram mx-2"></i>';
    // text += '      <i class="icon icon-sm-14 icon-16 icon-twitter"></i>';
    text += "    </div>";
    text += '    <span class="text-white text-10 text-lg-14 d-block px-5">';
    text +=
      '      <strong class="mr-3">풀앳홈 주식회사</strong> 사업자번호: 488-81-01469';
    text += "      대표: 소순호. 주소: 서울 마포구 마포대로 63-8, 삼창프라자빌딩 7층<br />";
    text += `      이메일: jakuzu@Logisall.co.kr 대표번호: 02-3669-7733 (평일)`;
    text += "      09:00~18:00, (점심) 12:00~13:00";
    text += "    </span>";
    text +=
      '    <div class="text-white mt-4 mt-md-5 d-lg-flex justify-content-center">';
    text += '      <div class="d-flex justify-content-center">';
    text += "        <a";
    text += '          class="text-white text-10 text-lg-16 px-2 px-lg-3"';
    text += '          href="/about#map">';
    text += "          오시는 길";
    text += "        </a>";
    text += '        <div class="text-white text-9">|</div>';
    text += "        <a";
    text += '          class="text-white text-10 text-lg-16 px-2 px-lg-3"';
    text += '          href="/notice">';
    text += "          이용안내";
    text += "        </a>";
    text += '        <div class="text-white text-9">|</div>';
    text += "        <a";
    text += '          class="text-white text-10 text-lg-16 px-2 px-lg-3"';
    text += '          href="/about">';
    text += "          회사소개";
    text += "        </a>";
    text += '        <div class="text-white text-9">|</div>';
    text += "        <a";
    text += '          class="text-white text-10 text-lg-16 px-2 px-lg-3"';
    text += '          href="/terms">';
    text += "          이용약관";
    text += "        </a>";
    text += '        <div class="text-white text-9">|</div>';
    text += "        <a";
    text += '          class="';
    text += "            text-white text-10 text-lg-16";
    text += "            px-2 px-lg-3";
    text += '            font-weight-bold" href="/terms">';
    text += "          개인정보처리방침";
    text += "        </a>";
    // text += '        <div class="border my-1 my-lg-2 d-none d-lg-block"></div>';
    text += "      </div>";
    // text += '      <div class="dropdown mt-n1">';
    // text += "        <button";
    // text += '          class="';
    // text += "            btn";
    // text += "            dropdown-toggle";
    // text += "            text-white text-10 text-lg-16";
    // text += "            py-0";
    // text += "            px-2 px-lg-3";
    // text += '            mt-2 mt-lg-0"';
    // text += '          type="button"';
    // text += '          id="dropdownMenuButton"';
    // text += '          data-toggle="dropdown"';
    // text += '          aria-expanded="false">';
    // text += "          패밀리 사이트";
    // text += "        </button>";
    text +=
      '        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
    text +=
      '          <a class="dropdown-item text-10 text-lg-16" href="#">sample</a>';
    text += "        </div>";
    text += "      </div>";
    text += "    </div>";
    text += "  </section>";
    return text;
  }
  $(".global-footer").append(footer());
}

document.addEventListener("readystatechange", function () {
  if (document.readyState === "complete") {
    layout();
  }
});
