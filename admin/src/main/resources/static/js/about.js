// 히스토리
function about() {
  // top-button
  document.querySelector(".top-button").addEventListener("click", () => {
    window.scrollTo(0, 0);
  });

  // sticky-nav

  var nav = document.querySelector(".about-nav");
  var marginItem = document.querySelector(".sticky-margin");
  var itemTop = nav.offsetTop;

  function stickyNav() {
    var globalNav = document.querySelector(".global-nav");
    var currentScroll = window.pageYOffset;
    currentScroll > itemTop + globalNav.clientHeight
      ? (nav.classList.add("sticky"),
        (marginItem.style.paddingTop = `${nav.clientHeight}px`))
      : (nav.classList.remove("sticky"), (marginItem.style.paddingTop = 0));
  }

  // sticky-nav li add .active
  function getActive() {
    var currentY = pageYOffset;
    var items = ["service", "system", "history", "map"];
    var navBtn = items.map((x) => document.querySelector("." + x + "-link"));

    items.forEach((item, idx) => {
      var section = document.querySelector("#" + item);
      var minH = section.offsetTop - 10;
      var maxH = minH + section.clientHeight;

      if (currentY > minH && currentY < maxH) {
        navBtn[idx].classList.add("active");
        if (idx > 0) {
          navBtn[idx - 1].classList.remove("active");
        }
        if (idx < navBtn.length - 1) {
          navBtn[idx + 1].classList.remove("active");
        }
      }
    });
  }

  window.onscroll = () => {
    stickyNav();
    getActive();
  };
}
document.addEventListener("readystatechange", function () {
  if (document.readyState === "complete") {
    about();
  }
});
