(function (window, document, $, undefined) {

    $(function () {

        $('.comment-create-update').each(function () {
            // variables
            var $radioMode = $('[name="type"]');
            var $divEvent = $('[data-role="comment-type-event"]');
            var $divPost = $('[data-role="comment-type-post"]');

            var collapse = function(val) {
                if (val === 'EVENT') {
                    $divEvent.closest('div').show();
                    $divPost.closest('div').hide();
                } else {
                    $divEvent.closest('div').hide();
                    $divPost.closest('div').show();
                }
            };

            $radioMode.on('change', function () {
                collapse($(this).val());
            });

            collapse($('[name="type"]:checked').val());

        });

    });

})(window, document, window.jQuery);
