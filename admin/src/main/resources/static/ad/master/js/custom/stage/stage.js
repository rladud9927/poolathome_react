(function (window, document, $, undefined) {

  $(function () {

    $('[data-type="btn-b2c-quota"]').each(function () {
      var $btn = $(this);

      $btn.on('click', function (e) {
        var _this = $(this);

        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", _this.data('action'));
        document.body.appendChild(form);

        var token = $("meta[name='_csrf']").attr("content");
        var parameterName = $("meta[name='_csrf_param']").attr("content");

        var inputCsrf = document.createElement("input");
        inputCsrf.setAttribute("type", "hidden");
        inputCsrf.setAttribute("name", parameterName);
        inputCsrf.setAttribute("value", token);
        form.appendChild(inputCsrf);

        var inputId = document.createElement("input");
        inputId.setAttribute("type", "hidden");
        inputId.setAttribute("name", "id");
        inputId.setAttribute("value", _this.data("id"));
        form.appendChild(inputId);

        var inputB2CQuota = document.createElement("input");
        inputB2CQuota.setAttribute("type", "hidden");
        inputB2CQuota.setAttribute("name", "b2cQuota");
        inputB2CQuota.setAttribute("value", _this.siblings("input[name=b2c-quota]").val());
        form.appendChild(inputB2CQuota);

        var inputParam = document.createElement("input");
        inputParam.setAttribute("type", "hidden");
        inputParam.setAttribute("name", "query");
        inputParam.setAttribute("value", _this.data("param"));
        form.appendChild(inputParam);

        form.submit();
      });
    });
  });

})(window, document, window.jQuery);
(function (window, document, $, undefined) {

  $(function () {

    $('[data-type="btn-register"]').each(function () {
      var $btn = $(this);

      $btn.on('click', function (e) {
        var _this = $(this);

        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", _this.data('action'));
        document.body.appendChild(form);

        var token = $("meta[name='_csrf']").attr("content");
        var parameterName = $("meta[name='_csrf_param']").attr("content");

        var inputCsrf = document.createElement("input");
        inputCsrf.setAttribute("type", "hidden");
        inputCsrf.setAttribute("name", parameterName);
        inputCsrf.setAttribute("value", token);
        form.appendChild(inputCsrf);

        var inputId = document.createElement("input");
        inputId.setAttribute("type", "hidden");
        inputId.setAttribute("name", "id");
        inputId.setAttribute("value", _this.data("id"));
        form.appendChild(inputId);

        var inputParam = document.createElement("input");
        inputParam.setAttribute("type", "hidden");
        inputParam.setAttribute("name", "query");
        inputParam.setAttribute("value", _this.data("param"));
        form.appendChild(inputParam);

        form.submit();
      });
    });
  });

})(window, document, window.jQuery);
