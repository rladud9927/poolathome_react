$(function () {

  var ajaxKind = function (id) {
    $.ajax({
      url: '/admin/api/wine-type/wine-kind/' + id,
      method: 'GET',
      contentType: "application/json"
    }).done(function (result) {
      console.debug(result, 'result');

      if (result) {
        $('#radio-wine-type').html('');
        var selectedType = $('#radio-wine-type').data('selectedType');
        console.log(selectedType, 'selectedType');

        $(result).each(function (index, item) {
          console.log(selectedType, 'selectedType');
          var data = item.data;
          var checked = index === 0 ? 'checked' : '';
          if (selectedType) {
            checked = data.id === selectedType ? 'checked' : '';
          }
          console.log(data.id, 'data.id');
          console.log(checked, 'checked');

          $('#radio-wine-type').append('<label class="radio-inline c-radio" style="margin-bottom: 5px">' +
            '<input data-type="radio-active-language" id="inline-radio-' + index + '" type="radio" name="types" value="' + data.id + '" ' + checked + '>' +
            '<span class="fa fa-circle"></span>' + data.name + '</label>');
        })
      }

    }).fail(function (jqXHR, textStatus) {

      var message = "(" + jqXHR.status + ") ";
      if (jqXHR.responseJSON && jqXHR.responseJSON.message) {
        message = message + jqXHR.responseJSON.message;
      }
      $.notify(textStatus.toUpperCase() + ": 관리자에게 문의하세요.<br>Message: " + message, {status: "danger"});
    });
  };

  if ($('[name="wineKinds"]').size() > 0) {
    var first = $('[name="wineKinds"]:checked').first();
    if (!first)
      first = $('[name="wineKinds"]').first();

    if (first.val())
      ajaxKind(first.val());
  }

  $('[name="wineKinds"]').each(function () {

    $(this).change(function () {
      var id = $(this).val();
      ajaxKind(id);
    });
  });


  $('[name="countryCode"]').each(function () {

    var ajaxProductionAreas = function (code) {

      $.ajax({
        url: '/admin/api/wine-type/production-area/' + code,
        method: 'GET',
        contentType: "application/json"
      }).done(function (result) {
        console.debug(result, 'result');
        var selectedProductionArea = $('#radio-production-area').data('selectedProductionArea');
        console.log(selectedProductionArea, 'selectedProductionArea');

        if (result) {
          $('#radio-production-area').html('');

          $(result).each(function (index, item) {
            console.log(selectedProductionArea, 'selectedProductionArea');
            var data = item.data;
            var checked = index === 0 ? 'checked' : '';
            if (selectedProductionArea) {
              checked = data.id === selectedProductionArea ? 'checked' : '';
            }
            console.log(data.id, 'data.id');
            console.log(checked, 'checked');

            $('#radio-production-area').append('<label class="radio-inline c-radio" style="margin-bottom: 5px">' +
              '<input data-type="radio-active-language" id="inline-radio-' + index + '" type="radio" name="productions" value="' + data.id + '" ' + checked + '>' +
              '<span class="fa fa-circle"></span>' + data.name + '</label>');
          })
        }

      }).fail(function (jqXHR, textStatus) {

        var message = "(" + jqXHR.status + ") ";
        if (jqXHR.responseJSON && jqXHR.responseJSON.message) {
          message = message + jqXHR.responseJSON.message;
        }
        $.notify(textStatus.toUpperCase() + ": 관리자에게 문의하세요.<br>Message: " + message, {status: "danger"});
      });
    }

    $(this).change(function () {
      var code = $(this).val();


      ajaxProductionAreas(code);
    });


    if ($('[name="countryCode"]').size() > 0) {
      var first = $('[name="countryCode"]:checked').first();
      if (!first)
        first = $('[name="countryCode"]').first();

      if (first.val())
        ajaxProductionAreas(first.val());
    }
  });

});
