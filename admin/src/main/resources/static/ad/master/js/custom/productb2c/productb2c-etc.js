$(function () {

  $('[data-type="del-md-category"]').each(function () {

    $(this).on('click', function (e) {
      e.preventDefault();

      var id = $(this).data('id');

      swal({
        title: "정말 데이터를 삭제하시겠습니까?",
        text: "삭제 후, 다시 복구할 수 없습니다.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "네, 삭제합니다.",
        cancelButtonText: "아니오.",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: '/admin/product-b2c/md-category/' + id,
            method: 'DELETE',
            contentType: "application/json"
          }).done(function (result) {
            window.location.reload();
          }).fail(function (jqXHR, textStatus) {
            if (jqXHR.status.toString().startsWith("4")) {
              $.notify("현재페이지에 오류가 있습니다. 페이지를 새로고침(F5)하여 다시 이용해주세요.", {status: "danger"});
            } else {
              $.notify(textStatus.toUpperCase() + ": 관리자에게 문의하세요. <br>STATUS CODE: " + jqXHR.status, {status: "danger"});
            }
          });
        } else {
          swal("취소되었습니다.", "이 데이터는 그대로 유지됩니다.", "error");
        }
      });

    })
  });

  $('[data-type="del-event"]').each(function () {

    $(this).on('click', function (e) {
      e.preventDefault();

      var id = $(this).data('id');

      swal({
        title: "정말 데이터를 삭제하시겠습니까?",
        text: "삭제 후, 다시 복구할 수 없습니다.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "네, 삭제합니다.",
        cancelButtonText: "아니오.",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: '/admin/product-b2c/event/' + id,
            method: 'DELETE',
            contentType: "application/json"
          }).done(function (result) {
            window.location.reload();
          }).fail(function (jqXHR, textStatus) {
            if (jqXHR.status.toString().startsWith("4")) {
              $.notify("현재페이지에 오류가 있습니다. 페이지를 새로고침(F5)하여 다시 이용해주세요.", {status: "danger"});
            } else {
              $.notify(textStatus.toUpperCase() + ": 관리자에게 문의하세요. <br>STATUS CODE: " + jqXHR.status, {status: "danger"});
            }
          });
        } else {
          swal("취소되었습니다.", "이 데이터는 그대로 유지됩니다.", "error");
        }
      });

    })
  });

  $('[data-type="del-magazine"]').each(function () {

    $(this).on('click', function (e) {
      e.preventDefault();

      var id = $(this).data('id');

      swal({
        title: "정말 데이터를 삭제하시겠습니까?",
        text: "삭제 후, 다시 복구할 수 없습니다.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "네, 삭제합니다.",
        cancelButtonText: "아니오.",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: '/admin/product-b2c/magazine/' + id,
            method: 'DELETE',
            contentType: "application/json"
          }).done(function (result) {
            window.location.reload();
          }).fail(function (jqXHR, textStatus) {
            if (jqXHR.status.toString().startsWith("4")) {
              $.notify("현재페이지에 오류가 있습니다. 페이지를 새로고침(F5)하여 다시 이용해주세요.", {status: "danger"});
            } else {
              $.notify(textStatus.toUpperCase() + ": 관리자에게 문의하세요. <br>STATUS CODE: " + jqXHR.status, {status: "danger"});
            }
          });
        } else {
          swal("취소되었습니다.", "이 데이터는 그대로 유지됩니다.", "error");
        }
      });

    })
  });

  $('[data-type="del-featured"]').each(function () {

    $(this).on('click', function (e) {
      e.preventDefault();

      var id = $(this).data('id');

      swal({
        title: "정말 데이터를 삭제하시겠습니까?",
        text: "삭제 후, 다시 복구할 수 없습니다.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "네, 삭제합니다.",
        cancelButtonText: "아니오.",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: '/admin/product-b2c/featured/' + id,
            method: 'DELETE',
            contentType: "application/json"
          }).done(function (result) {
            window.location.reload();
          }).fail(function (jqXHR, textStatus) {
            if (jqXHR.status.toString().startsWith("4")) {
              $.notify("현재페이지에 오류가 있습니다. 페이지를 새로고침(F5)하여 다시 이용해주세요.", {status: "danger"});
            } else {
              $.notify(textStatus.toUpperCase() + ": 관리자에게 문의하세요. <br>STATUS CODE: " + jqXHR.status, {status: "danger"});
            }
          });
        } else {
          swal("취소되었습니다.", "이 데이터는 그대로 유지됩니다.", "error");
        }
      });

    })
  });
});
