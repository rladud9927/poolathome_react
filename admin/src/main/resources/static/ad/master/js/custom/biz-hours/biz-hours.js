$(function () {

  $('#biz-hours').each(function () {

    var $datetimeOpenWeekday = $('#datetime-open-weekday').find('[data-type="datetimepicker"]');
    var $datetimeEndWeekday = $('#datetime-end-weekday').find('[data-type="datetimepicker"]');
    var $datetimeBreakTimeStartWeekday = $('#datetime-breaktime-start-weekday').find('[data-type="datetimepicker"]');
    var $datetimeBreakTimeEndWeekday = $('#datetime-breaktime-end-weekday').find('[data-type="datetimepicker"]');

    var $datetimeOpenWeekend = $('#datetime-open-weekend').find('[data-type="datetimepicker"]');
    var $datetimeEndWeekend = $('#datetime-end-weekend').find('[data-type="datetimepicker"]');
    var $datetimeBreakTimeStartWeekend = $('#datetime-breaktime-start-weekend').find('[data-type="datetimepicker"]');
    var $datetimeBreakTimeEndWeekend = $('#datetime-breaktime-end-weekend').find('[data-type="datetimepicker"]');


    $datetimeOpenWeekday.on('dp.change', function (e) {
      var value = e.date ? e.date.format('HH:mm') : null;
      $('[name="bizHour.monOpenTime"]').val(value);
      $('[name="bizHour.tueOpenTime"]').val(value);
      $('[name="bizHour.wedOpenTime"]').val(value);
      $('[name="bizHour.thuOpenTime"]').val(value);
      $('[name="bizHour.friOpenTime"]').val(value);

    });
    $datetimeEndWeekday.on('dp.change', function (e) {
      var value = e.date ? e.date.format('HH:mm') : null;
      $('[name="bizHour.monCloseTime"]').val(value);
      $('[name="bizHour.tueCloseTime"]').val(value);
      $('[name="bizHour.wedCloseTime"]').val(value);
      $('[name="bizHour.thuCloseTime"]').val(value);
      $('[name="bizHour.friCloseTime"]').val(value);
    });
    $datetimeBreakTimeStartWeekday.on('dp.change', function (e) {
      var value = e.date ? e.date.format('HH:mm') : null;
      $('[name="bizHour.monStartBreakTime"]').val(value);
      $('[name="bizHour.tueStartBreakTime"]').val(value);
      $('[name="bizHour.wedStartBreakTime"]').val(value);
      $('[name="bizHour.thuStartBreakTime"]').val(value);
      $('[name="bizHour.friStartBreakTime"]').val(value);
    });
    $datetimeBreakTimeEndWeekday.on('dp.change', function (e) {
      var value = e.date ? e.date.format('HH:mm') : null;
      $('[name="bizHour.monEndBreakTime"]').val(value);
      $('[name="bizHour.tueEndBreakTime"]').val(value);
      $('[name="bizHour.wedEndBreakTime"]').val(value);
      $('[name="bizHour.thuEndBreakTime"]').val(value);
      $('[name="bizHour.friEndBreakTime"]').val(value);
    });

    $datetimeOpenWeekend.on('dp.change', function (e) {
      var value = e.date ? e.date.format('HH:mm') : null;
      $('[name="bizHour.satOpenTime"]').val(value);
      $('[name="bizHour.sunOpenTime"]').val(value);
      $('[name="bizHour.holiOpenTime"]').val(value);
    });
    $datetimeEndWeekend.on('dp.change', function (e) {
      var value = e.date ? e.date.format('HH:mm') : null;
      $('[name="bizHour.satCloseTime"]').val(value);
      $('[name="bizHour.sunCloseTime"]').val(value);
      $('[name="bizHour.holiCloseTime"]').val(value);
    });
    $datetimeBreakTimeStartWeekend.on('dp.change', function (e) {
      var value = e.date ? e.date.format('HH:mm') : null;
      $('[name="bizHour.satStartBreakTime"]').val(value);
      $('[name="bizHour.sunStartBreakTime"]').val(value);
      $('[name="bizHour.holiStartBreakTime"]').val(value);
    });
    $datetimeBreakTimeEndWeekend.on('dp.change', function (e) {
      var value = e.date ? e.date.format('HH:mm') : null;
      $('[name="bizHour.satEndBreakTime"]').val(value);
      $('[name="bizHour.sunEndBreakTime"]').val(value);
      $('[name="bizHour.holiEndBreakTime"]').val(value);
    });
  });
});