$(function () {

  var method1 = function (id) {
    $('#' + id).each(function () {
      var $checkbox = $(this);
      var $section = $('[data-type="' + id + '"]');

      if ($checkbox.is(':checked')) {
        $section.show();
      } else {
        $section.hide();
      }

      $checkbox.on('change', function () {

        if ($checkbox.is(':checked')) {
          $section.show();
        } else {
          $section.hide();
        }
      })
    });
  };

  method1('chk-mon');
  method1('chk-tue');
  method1('chk-wed');
  method1('chk-thu');
  method1('chk-fri');
  method1('chk-sat');
  method1('chk-sun');
  method1('chk-holi');


});