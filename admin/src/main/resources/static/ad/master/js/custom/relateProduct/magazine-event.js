$(function () {

  // 비동기 함수 (utils)
  function asyncData(url, method, data, callback) {
    $.ajax({
      url: url,
      method: method,
      contentType: "application/json",
      data: data
    }).done(function (result) {
      callback(result);
    }).fail(function (jqXHR, textStatus) {
      if (jqXHR.status.toString().startsWith("4")) {
        $.notify("현재페이지에 오류가 있습니다. 페이지를 새로고침(F5)하여 다시 이용해주세요.", {status: "danger"});
      } else {
        $.notify(textStatus.toUpperCase() + ": 관리자에게 문의하세요. <br>STATUS CODE: " + jqXHR.status, {status: "danger"});
      }
    });
  }

  $('[data-type="relate-product"]').each(function () {
    var $this = $(this);
    var contentId = $this.data('content-id') || null;
    var type = $this.data('content-type') || null;
    var vm_rpm = new Vue({
      el: $this[0],
      data: {
        list: [],
      },
      methods: {
        // 목록 요청
        reqList() {
          asyncData(
            '/admin/api/product-b2c/' + type + '?idContent=' + contentId,
            'GET',
            null,
            function (result) {
              if (!isEmpty(result)) {
                // 목록 업데이트.
                vm_rpm.list = result;
              } else {
                vm_rpm.list = []
              }
            });
        },
        // 등록 요청
        reqRegister(idProductB2C) {
          if (!idProductB2C || !contentId) {
            alert('잘못된 요청입니다.')
          }
          // 중복 체크
          var isDuplicate = vm_rpm.list.some(function (item) {
            return item.data.id === idProductB2C;
          })
          if (!isDuplicate) {
            asyncData(
              '/admin/api/product-b2c/' + type,
              'POST',
              JSON.stringify({
                idContent: contentId,
                idProductB2C: idProductB2C
              }),
              function (result) {
                // 목록 업데이트
                vm_rpm.reqList();
              });
          } else {
            alert('이미 연결된 상품입니다.')
          }

        },
        // 삭제 요청
        reqDelete(idProductB2C) {
          if (!idProductB2C || !contentId) {
            alert('잘못된 요청입니다.');
            return;
          }
          asyncData(
            '/admin/api/product-b2c/' + type + '/' + idProductB2C + '/' + contentId,
            'DELETE',
            null,
            function (result) {
              // 목록 업데이트
              vm_rpm.reqList();
            });
        }
      },
      mounted() {
        // 초기 데이터 로드
        if (!isEmpty(contentId))
          this.reqList();
      }
    })

    // 자동 완성 함수의 결과 리스트 선택 시 호출;
    // static/ad/master/js/custom/autocomplete-ajax/autocomplete-ajax.js
    window.callbackRelatedMagazine = function (data) {
      vm_rpm.reqRegister(data.id)
    };
  });
});
