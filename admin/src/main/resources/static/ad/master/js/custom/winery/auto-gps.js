$(function () {

  $('#btn-winery-auto-gps').each(function () {

    $(this).on('click', function (e) {
      e.preventDefault();

      $.ajax({
        url: "/admin/api/winery/auto-gps",
        method: 'POST',
        contentType: "application/json"
      }).done(function (result) {
        console.debug(result);
        window.location.reload();

      }).fail(function (jqXHR, textStatus) {
        var message = "(" + jqXHR.status + ") ";
        if (jqXHR.responseJSON && jqXHR.responseJSON.message) {
          message = message + jqXHR.responseJSON.message;
        }
        $.notify(textStatus.toUpperCase() + ": 관리자에게 문의하세요.<br>Message: " + message, {status: "danger"});
      });
    })
  });
});
