(function (window, document, $, undefined) {

  $(function () {

    $('.coupon-create-update').each(function () {

      var connCompany = $('#conn-company');
      var connStore = $('#conn-store');

      var changeRadio = function (val) {
        switch(val) {
          case 'ALL':
            connCompany.hide();
            connStore.hide();
            break;
          case 'COMPANY':
            connCompany.show();
            connStore.hide();
            break;
          case 'STORE':
            connCompany.hide();
            connStore.show();
            break;
        }
      }

      var $radio = $('[name="whereToUse"]');
      changeRadio($radio.val());
      $radio.on('change', function (e){
        var $radio = $(this);
        changeRadio($radio.val());
      });

    });

    $('.coupon-create-update').each(function () {

      var connProductB2C = $('#conn-product-b2c');
      var autoProductB2C = $('[data-fn-name="callbackByProductB2C"]');
      var connProduct = $('#conn-product');
      var autoProduct = $('[data-fn-name="callbackByProduct"]');
      var shipCoupon = $('[name="shipCoupon"]');
      shipCoupon.val(false);

      var changeRadio = function (val) {
        switch(val) {
          case 'PRODUCT':
            connProductB2C.hide();
            connProductB2C.find('[name="relativeProductB2C"]').val('');
            connProductB2C.find('#product-b2c-name').val('');
            autoProductB2C.val('');
            connProduct.show();
            shipCoupon.val(false);
            break;
          case 'PRODUCTB2C':
            connProductB2C.show();
            connProduct.hide();

            connProduct.find('[name="tempProduct"]').val('');
            connProduct.find('#product-name').val('');
            autoProduct.val('');
            shipCoupon.val(false);
            break;
          case 'SHIP':
            connProductB2C.hide();
            connProduct.hide();
            connProductB2C.find('[name="relativeProductB2C"]').val('');
            connProductB2C.find('#product-b2c-name').val('');
            autoProductB2C.val('');
            connProduct.find('[name="tempProduct"]').val('');
            connProduct.find('#product-name').val('');
            autoProduct.val('');
            shipCoupon.val(true);
            break;
          case 'ORDER':
            connProductB2C.hide();
            connProduct.hide();
            connProductB2C.find('[name="relativeProductB2C"]').val('');
            connProductB2C.find('#product-b2c-name').val('');
            autoProductB2C.val('');
            connProduct.find('[name="tempProduct"]').val('');
            connProduct.find('#product-name').val('');
            autoProduct.val('');
            shipCoupon.val(false);
            break;
        }
      }

      var $radio = $('[name="ctype"]');
      changeRadio($radio.val());
      $radio.on('change', function (e){
        var $radio = $(this);
        changeRadio($radio.val());
      });

    });

  });

})(window, document, window.jQuery);
