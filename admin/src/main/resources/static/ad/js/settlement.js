
(function (window, document, $, undefined) {

    $(function () {

        //Export excel Order
        var isClickExcelOrder = false;
        $('#export-excel-settlement-store').on('click', function (e) {
            e.preventDefault();

            if (isClickExcelOrder) {
                return;
            }
            $.notify("Downloading...", {timeout: 30000});
            isClickExcelOrder = true;

            var objects = $('#form-list-filter').serializeObject();
            var token = $("meta[name='_csrf']").attr("content");
            var param = $("meta[name='_csrf_param']").attr("content");

            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", $(this).data('action'));
            document.body.appendChild(form);

            var inputs = [];
            inputs.push({name: param, value: token});

            for (var key in objects) {
                inputs.push({name: key, value: objects[key]});
            }

            inputs.forEach(function (item) {
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", item.name);
                input.setAttribute("value", item.value);
                form.appendChild(input);
            });

            form.submit();

            setTimeout(function () {
                isClickExcelOrder = false;
                form.remove();
            }, 60000);
        });

        //Export excel Order
        var isClickExcelOrder = false;
        $('#export-excel-settlement-order').on('click', function (e) {
            e.preventDefault();

            if (isClickExcelOrder) {
                return;
            }
            $.notify("Downloading...", {timeout: 30000});
            isClickExcelOrder = true;

            var objects = $('#form-list-filter').serializeObject();
            var token = $("meta[name='_csrf']").attr("content");
            var param = $("meta[name='_csrf_param']").attr("content");

            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", $(this).data('action'));
            document.body.appendChild(form);

            var inputs = [];
            inputs.push({name: param, value: token});

            for (var key in objects) {
                inputs.push({name: key, value: objects[key]});
            }

            inputs.forEach(function (item) {
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", item.name);
                input.setAttribute("value", item.value);
                form.appendChild(input);
            });

            form.submit();

            setTimeout(function () {
                isClickExcelOrder = false;
                form.remove();
            }, 60000);
        });

    });

})(window, document, window.jQuery);
