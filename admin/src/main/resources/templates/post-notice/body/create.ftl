<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid post-create-update">
    <#-- FORM -->
  <form id="form-create-post" action="<@spring.url header.url + "/create"/>" method="post" data-parsley-validate=""
        novalidate="" data-parsley-international="true">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            저장
          </button>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="목록보기">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
        <#-- CONTENT-->
      <div class="col-lg-7">

          <#-- TAB Panel or Panel Wrapper -->
        <div <#if international>role="tabpanel" class="panel panel-transparent" data-type="tabpanel-language"
             <#else>class="panel panel-default"</#if>>

            <@spring.bind "post.title"/>
            <#assign tabPath = spring.status.expression/>
            <#-- TAB LANGUAGE HEADER -->
            <@ui.tabListByLanguage tabPath/>
            <#-- END : TAB LANGUAGE HEADER -->

            <#-- TAB LANGUAGE BODY -->
          <div <#if international>class="tab-content bg-white" <#else>class="panel-body"</#if>>


                <div id="${tabPath}-ko" <#if international>role="tabpanel" class="tab-pane"</#if>>

                    <#-- 제목 -->
                    <@ui.formInputTextByLanguage "post.title.textKoKr" "제목" true 200 "제목을 입력하세요."/>

                    <#-- WYSISWYG EDITOR -->
                  <hr/>
                    <@ui.wysiswygEdior "post.content.textKoKr" "내용" true/>

                    <@ui.formActiveByLanguage "post.internationalMode.koKr" />
                </div>

          </div>
        </div>
          <#-- END : TAB LANGUAGE BODY -->

      </div>
        <#-- END : CONTENT-->
        <#-- SIDBAR -->
      <div class="col-lg-5 pl0-lg">

        <div class="panel panel-default">
          <div class="panel-heading">설정</div>
          <div class="panel-body">

              <@ui.formDate "post.regTime" "등록날짜" true "YYYY-MM-DD HH:mm:ss"/>

            <hr/>
              <#-- 공지 모드 -->
<#--              <@ui.formBoolean "post.top" "공지모드" "활성" "비활성" "활성일 경우 최상단에 노출됩니다."/>-->

<#--            <hr/>-->

              <@ui.formRadioboxEnum "post.type" "유형" true types/>

            <hr/>

              <#-- 활성 모드 -->
              <@ui.formActive "post.active"/>

          </div>
        </div>

      </div>
        <#-- END : SIDBAR -->
    </div>
  </form>
</div>
