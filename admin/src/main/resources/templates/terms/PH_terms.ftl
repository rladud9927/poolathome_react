<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta name="keywords" content=“풀앳홈, PAH, pool at home, 3PL, 설치배송, 설치, 배송, 가전설치, 가구설치, 가전배송, 가구배송, 중량물배송, 바로설치, 반품물류, AS, 이전설치, 유통가공, 리퍼비시" />
    <meta name="description" content=“풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A/S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content=“풀앳홈"/>
    <meta property="og:description" content="풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A/S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
    <meta property="og:image" content="/assets/image/about-main.png"/>
    <meta property="og:url" content="http://www.poolathome.co.kr"/>
    <meta name="naver-site-verification" content="08c6b592ad6098170ac111f8d469e4fb152c0643" />
    <meta name="google-site-verification" content="G0p9XQnbnVzY8nNsQIJurWBUvVHYf3RsHURZmBIOY78" />

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/site.webmanifest">

    <!-- bootstrap -->
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
      integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn"
      crossorigin="anonymous"
    />
    <script
      src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
      integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"
      integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2"
      crossorigin="anonymous"
    ></script>
    <!--  -->
    <script type="text/javascript" src="../js/layout.js"></script>

    <!-- style -->
    <!-- 완료시 index.css만 -->
    <link rel="stylesheet" href="../css/index.css" />
    <link rel="stylesheet" href="../css/commons.css" />
    <script type="text/javascript" src="../js/index.js"></script>
    <title>풀앳홈 | Terms</title>
  </head>

  <body>
    <!-- global-nav -->
    <header class="global-nav"></header>
    <!-- /global-nav -->

    <section class="container py-5">
      <h1
        class="
          text-26 text-lg-42 text-xl-56 text-center
          font-weight-bold
          mt-md-5
        "
      >
        개인정보 수집 및 이용 약관
      </h1>
      <!-- dropdown -->
      <div class="dropdown text-center mt-4 mt-md-5 mb-5">
        <button
          class="
            btn
            dropdown-toggle
            pill
            border
            px-4
            py-2
            text-gray text-12 text-xl-14
          "
          type="button"
          id="dropdownMenuButton"
          data-toggle="dropdown"
          aria-expanded="false"
        >
          2021-09-09
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <a class="dropdown-item" href="#">2021-09-09</a>
          <a class="dropdown-item" href="#">2021-09-08</a>
          <a class="dropdown-item" href="#">2021-09-07</a>
        </div>
      </div>

      <!--  -->
      <div>
        <h2 class="text-22 text-xl-32 font-weight-bold mb-4">제 1장 총칙</h2>
        <p class="text-gray text-14" style="line-height: 1.7">
          <span class="font-weight-bold">제 1조 (목적)</span><br /><br />

          1. 이 약관은 이용자가 주식회사 풀앳홈(이하 “회사”)이 운영하는 인터넷
          서비스 사이트(이하 “사이트” 또는 “풀앳홈”)를 통해 제공하는 인터넷
          전자상거래 관련 서비스(이하 “서비스”)와 관련하여 회사와 이용자의 권리,
          의무, 책임사항을 규정함을 목적으로 합니다. 또한 본 약관은 유무선
          PC통신, 태블릿 PC, 스마트폰(아이폰, 안드로이드폰 등) 어플리케이션 및
          모바일웹 등을 이용하는 전자상거래 등에 대해서도 그 성질에 반하지 않는
          한 준용됩니다.<br />

          2. 본 약관이 규정한 내용 이외의 회사와 이용자 간의 권리, 의무 및
          책임사항에 관하여서는 전기통신사업법 기타 대한민국의 관련 법령과
          상관습에 의합니다.<br /><br /><br />

          <span class="font-weight-bold">제 2조 (정의)</span><br /><br />

          1. 회사가 운영하는 사이트는 아래와 같습니다.<br />
          추후 회사에서 공지하고 제공하는 기타 웹사이트, 스마트폰 및
          이동통신기기를 통해 제공되는 모바일 어플리케이션 포함<br /><br />

          2. 회사가 운영하는 사이트는 아래와 같습니다.<br />
          추후 회사에서 공지하고 제공하는 기타 웹사이트, 스마트폰 및
          이동통신기기를 통해 제공되는 모바일 어플리케이션 포함<br />
        </p>
      </div>
      <!--  -->
      <div class="py-5">
        <h2 class="text-22 text-xl-32 font-weight-bold mb-4 mt-5">
          제 2장 서비스 이용 계약
        </h2>
        <p class="text-gray text-14" style="line-height: 1.7">
          <span class="font-weight-bold">제 1조 (목적)</span><br /><br />

          1. 이 약관은 이용자가 주식회사 풀앳홈(이하 “회사”)이 운영하는 인터넷
          서비스 사이트(이하 “사이트” 또는 “풀앳홈”)를 통해 제공하는 인터넷
          전자상거래 관련 서비스(이하 “서비스”)와 관련하여 회사와 이용자의 권리,
          의무, 책임사항을 규정함을 목적으로 합니다. 또한 본 약관은 유무선
          PC통신, 태블릿 PC, 스마트폰(아이폰, 안드로이드폰 등) 어플리케이션 및
          모바일웹 등을 이용하는 전자상거래 등에 대해서도 그 성질에 반하지 않는
          한 준용됩니다.<br />

          2. 본 약관이 규정한 내용 이외의 회사와 이용자 간의 권리, 의무 및
          책임사항에 관하여서는 전기통신사업법 기타 대한민국의 관련 법령과
          상관습에 의합니다.<br /><br /><br />

          <span class="font-weight-bold">제 2조 (정의)</span><br /><br />

          1. 회사가 운영하는 사이트는 아래와 같습니다.<br />
          추후 회사에서 공지하고 제공하는 기타 웹사이트, 스마트폰 및
          이동통신기기를 통해 제공되는 모바일 어플리케이션 포함<br /><br />

          2. 회사가 운영하는 사이트는 아래와 같습니다.<br />
          추후 회사에서 공지하고 제공하는 기타 웹사이트, 스마트폰 및
          이동통신기기를 통해 제공되는 모바일 어플리케이션 포함<br />
        </p>
      </div>
    </section>

    <!-- footer -->
    <footer class="global-footer"></footer>
    <!--/footer  -->
  </body>
</html>
