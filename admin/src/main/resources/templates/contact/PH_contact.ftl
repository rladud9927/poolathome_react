<#import "/spring.ftl" as spring/>
<#import "/common/ui.ftl" as ui/>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <meta name="keywords" content=“풀앳홈, PAH, pool at home, 3PL, 설치배송, 설치, 배송, 가전설치, 가구설치, 가전배송, 가구배송, 중량물배송, 바로설치, 반품물류, AS, 이전설치, 유통가공, 리퍼비시" />
  <meta name="description" content=“풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A/S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
  <meta property="og:type" content="website"/>
  <meta property="og:title" content=“풀앳홈"/>
  <meta property="og:description" content="풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A/S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
  <meta property="og:image" content="/assets/image/about-main.png"/>
  <meta property="og:url" content="http://www.poolathome.co.kr"/>
  <meta name="naver-site-verification" content="08c6b592ad6098170ac111f8d469e4fb152c0643" />
  <meta name="google-site-verification" content="G0p9XQnbnVzY8nNsQIJurWBUvVHYf3RsHURZmBIOY78" />

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">

  <!-- bootstrap -->
  <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
        integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn"
        crossorigin="anonymous"
  />
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous"
  ></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
          integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
          crossorigin="anonymous"
  ></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"
          integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2"
          crossorigin="anonymous"
  ></script>

  <!--  -->
  <script type="text/javascript" src="../js/layout.js"></script>

  <!-- style -->
  <!-- 완료시 index.css만 -->
  <link rel="stylesheet" href="../css/index.css"/>
  <link rel="stylesheet" href="../css/commons.css"/>
  <script type="text/javascript" src="../js/index.js"></script>

  <title>풀앳홈 | Contact</title>
</head>

<body class="d-flex flex-column justify-content-between">
<!-- global-nav -->
<header class="global-nav"></header>
<!-- /global-nav -->

<section class="container py-lg-5">
  <div class="row mt-5">
    <div class="col-6 d-none d-md-block">
      <img src="../assets/image/desk.png" alt="" class="w-100"/>
    </div>
    <div class="col-12 col-md-6">
      <h1 class="font-weight-bold text-center text-lg-left text-26 text-lg-42 text-xl-56 mb-4">
        궁금하신 사항을
        <br/>
        문의해 보세요
      </h1>
      <span class="text-center text-lg-left text-gray text-14 text-lg-18 text-xl-22 d-block">
            세부 정보를 입력해 주시면 최대한 빨리 연락드리겠습니다.
      </span>
      <form id="form-create-contact" action="<@spring.url "/contact/create"/>" method="post" data-parsley-validate=""
            novalidate="">
        <div class="row mt-5">
          <div class="col-12 col-lg-6">
            <h6 class="text-14 mb-2">지역</h6>
            <input
                    type="text"
                    name="area"
                    class="rounded border border-gray-c9 w-100 mb-4 mb-lg-2"
                    value="${qna.area!}"
            />
            <h6 class="text-14 mb-2">상호명</h6>
            <input
                    type="text"
                    name="companyName"
                    class="rounded border border-gray-c9 w-100 mb-4 mb-lg-2"
                    value="${qna.companyName!}"
            />
            <h6 class="text-14 mb-2">
              담당자명 <span class="text-error">*</span>
            </h6>
            <input
                    type="text"
                    name="contactName"
                    class="rounded border border-gray-c9 w-100 mb-4 mb-lg-2"
                    value="${qna.contactName!}"
            />
            <h6 class="text-14 mb-2">
              연락처 <span class="text-error">*</span>
            </h6>
            <input
                    type="text"
                    name="phone"
                    class="rounded border border-gray-c9 w-100 mb-4 mb-lg-2"
                    value="${qna.phone!}"
            />
            <h6 class="text-14 mb-2">이메일주소</h6>
            <input
                    type="text"
                    name="email"
                    class="rounded border border-gray-c9 w-100 mb-4 mb-lg-2"
                    value="${qna.email!}"
            />
          </div>
          <div
                  class="col-12 col-lg-6 d-flex flex-column justify-content-between"
          >
            <div>
              <h6 class="text-14 mb-2">문의사항</h6>
              <textarea
                      name="content"
                      id=""
                      cols="30"
                      rows="10"
                      class="rounded border border-gray-c9 w-100"
              >${qna.content!}</textarea>
            </div>

            <!-- checkbox -->
            <div class="form-check d-flex align-items-start mt-4 mt-md-0">
              <label>
                <input
                        id="defaultCheck1"
                        class="form-check-input mt-2"
                        type="checkbox"
                />
                <input name="termsAgree" type="hidden"/>
              </label>

              <div class="form-check-label text-gray text-14 d-xxl-flex align-items-center">
                <a onclick="window.open('/terms')"
                   class="underlined text-gray font-weight-bold text-10 text-lg-14 text-nowrap">
                  개인정보 수집 및 이용 약관
                </a>
                <br/>
                <span class="d-block text-10 text-lg-14 text-nowrap">
                    에 대해 동의합니다
                </span>
              </div>
            </div>

            <button type="submit"
                    class="btn btn-block pill btn-primary text-white text-12 text-xl-14 py-2 mt-3 mb-5 mb-lg-0">
              제출
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>

<!-- footer -->
<footer class="global-footer"></footer>
<!--/footer  -->

<script>
  // $(function() {
  //    $('#defaultCheck1').on('change', function (e) {
  //        alert($(this).is(':checked'));
  //        $('[name="termsAgree"]').val($(this).is(':checked'));
  //    })
  // });
</script>
</body>
</html>
