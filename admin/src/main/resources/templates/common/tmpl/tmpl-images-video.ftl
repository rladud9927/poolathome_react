<script id="template-item-images-video" type="x-tmpl-mustache">
{{#isImage}}
<li class="box-image-uploader item-box-image" style="background-image: url({{image}});">
    <a href="javascript:void(0);" class="btn-image-uploader-delete">
        <em class="icon-close"></em>
    </a>
    <input type="hidden" name="{{inputName}}[{{index}}].url" value="{{image}}"/>
    <input type="hidden" name="{{inputName}}[{{index}}].mimeType" value="{{mimeType}}"/>
    <input type="hidden" name="{{inputName}}[{{index}}].orderAscending" value="{{index}}" data-type="sortable-order" />
</li>
{{/isImage}}
{{#isVideo}}
<li class="box-image-uploader item-box-image" >
    <a href="javascript:void(0);" class="btn-image-uploader-delete">
        <em class="icon-close"></em>
    </a>
    <video muted controls autoplay style="width: 120px;height: 120px;">
      <source src="{{image}}" type="video/mp4">
    </video>
    <input type="hidden" name="{{inputName}}[{{index}}].url" value="{{image}}"/>
    <input type="hidden" name="{{inputName}}[{{index}}].mimeType" value="{{mimeType}}"/>
    <input type="hidden" name="{{inputName}}[{{index}}].orderAscending" value="{{index}}" data-type="sortable-order" />
</li>
{{/isVideo}}
</script>