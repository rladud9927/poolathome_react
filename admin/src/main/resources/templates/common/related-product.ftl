<div>
    <label class="control-label">와인 검색 (입력시 자동검색됩니다.)</label>

    <input type="text" placeholder="와인 명 입력하세요."
           class="form-control"
           data-type="autocomplete-ajax"
           data-url="/admin/api/product-b2c"
           data-param-name="query"
           data-fn-name="callbackRelatedMagazine">
    <hr/>
    <label class="control-label">상품 목록</label>
    <#-- list -->
    <div class="panel panel-default"
         data-type="relate-product"
         data-content-id="${rpIdContent?c}"
         data-content-type="${rpType!}">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th class="text-center" width="5%">ID</th>
                    <th class="text-center" width="15%">썸네일</th>
                    <th class="text-center" width="60%">상품명</th>
                    <th class="text-center" width="5%">Action</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in list" :key="index">
                    <td class="text-center">{{item.data.id}}</td>
                    <td>
                        <img height="80" :src="item.data.thumbnail"/>
                    </td>
                    <td>{{item.data.name}}</td>
                    <td>
                        <button type="button"
                                @click="reqDelete(item.data.id)"
                                class="btn btn-sm btn-default"
                                data-toggle="tooltip" data-placement="top" title="삭제">
                            <em class="fa fa-trash"></em>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
