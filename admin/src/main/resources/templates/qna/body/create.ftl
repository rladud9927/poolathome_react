<#import "/spring.ftl" as spring/>
<#-- START widgets box-->
<div class="container-fluid qna-create-update">
    <#-- FORM -->
  <form id="form-create-qna" action="<@spring.url header.url + "/create"/>" method="post" data-parsley-validate=""
        novalidate="">
    <div class="row">
      <div class="col-md-12">

        <div class="pull-left mb-lg">
          <button type="submit" class="btn btn-primary btn-lg">
            저장
          </button>
          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </div>

        <div class="pull-right mb-lg">
          <a href="<@spring.url header.url/>" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="top"
             title="목록보기">
            <span class="icon-list"></span>
          </a>
        </div>

      </div>
    </div>
    <div class="row">
        <#-- CONTENT-->
      <div class="col-lg-7">
        <div class="panel panel-default">

          <div class="panel-body">
              <#-- WYSISWYG EDITOR -->
              <@ui.wysiswygEdior "qna.content" "내용(문의사항)" true/>
              <#-- END : WYSISWYG EDITOR -->
          </div>
        </div>

      </div>
        <#-- END : CONTENT-->
        <#-- SIDBAR -->
      <div class="col-lg-5">

        <div class="panel panel-default">
          <div class="panel-heading">작성자</div>

          <div class="panel-body">

              <@ui.formInputText "qna.area" "지역" true 100 "지역명을 입력하세요."/>
              <@ui.formInputText "qna.companyName" "상호명" true 30 "상호명을 입력하세요."/>
              <@ui.formInputText "qna.contactName" "담당자명" true 10 "담장자 성함을 입력하세요."/>
              <@ui.formInputText "qna.phone" "연락처(- 없이 번호만 입력)" true 12 "휴대폰 번호를 입력하세요." "number"/>
              <@ui.formInputText "qna.email" "이메일 주소" true 100 "이메일 주소를 입력하세요."/>

          </div>

        </div>

          <@spring.bind "qna.termsAgree"/>
        <div class="panel panel-default">
          <div class="panel-body">
            <label class="control-label">개인정보 수집 및 이용 약관 동의</label>
            <div>
              <label class="radio-inline c-radio">
                <input data-type="mode" id="inline-radio-international-1" type="radio"
                       name="${spring.status.expression}" value="true"
                       <#if spring.stringStatusValue == 'true'>checked</#if>>
                <span class="fa fa-circle"></span>활성
              </label>
              <label class="radio-inline c-radio">
                <input data-type="mode" id="inline-radio-international-2" type="radio"
                       name="${spring.status.expression}" value="false"
                       <#if spring.stringStatusValue == 'false'>checked</#if>>
                <span class="fa fa-circle"></span>비활성
              </label>
            </div>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">설정</div>
          <div class="panel-body">

              <#-- 활성 모드 -->
              <@ui.formActive "qna.active"/>

          </div>
        </div>
      </div>
        <#-- END : SIDBAR -->
    </div>
  </form>
</div>
