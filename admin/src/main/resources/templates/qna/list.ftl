<#import "/spring.ftl" as spring/>
<#import "/common/ui.ftl" as ui/>
<!DOCTYPE html>
<html lang="ko">

<head>
    <#include "/common/head-meta.ftl">
    <#include "/common/css/list.ftl">

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">

</head>
<body>
<div class="wrapper">
    <#-- top navbar-->
    <#include "/common/top-navbar.ftl">
    <#-- sidebar-->
    <#include "/common/sidebar.ftl">

    <#-- Main section -->
  <section>
      <#-- Page content-->
    <div class="content-wrapper">
        <#include "/common/header.ftl" />
        <#include "body/list.ftl">
    </div>
  </section>

    <#-- Page footer-->
    <#include "/common/footer.ftl">
</div>

<#include "/common/vendor.ftl">
<#include "/common/script/list.ftl">

</body>

</html>