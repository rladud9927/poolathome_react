<#import "/spring.ftl" as spring/>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <meta name="keywords" content=“풀앳홈, PAH, pool at home, 3PL, 설치배송, 설치, 배송, 가전설치, 가구설치, 가전배송, 가구배송, 중량물배송, 바로설치, 반품물류, AS, 이전설치, 유통가공, 리퍼비시" />
  <meta name="description" content=“풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A/S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
  <meta property="og:type" content="website"/>
  <meta property="og:title" content=“풀앳홈"/>
  <meta property="og:description" content="풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A/S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
  <meta property="og:image" content="/assets/image/about-main.png"/>
  <meta property="og:url" content="http://www.poolathome.co.kr"/>
  <meta name="naver-site-verification" content="08c6b592ad6098170ac111f8d469e4fb152c0643" />
  <meta name="google-site-verification" content="G0p9XQnbnVzY8nNsQIJurWBUvVHYf3RsHURZmBIOY78" />

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">

  <!-- bootstrap -->
  <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
          integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn"
          crossorigin="anonymous"
  />
  <script
          src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous"
  ></script>
  <script
          src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
          integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
          crossorigin="anonymous"
  ></script>
  <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"
          integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2"
          crossorigin="anonymous"
  ></script>
  <!--  -->
  <script type="text/javascript" src="../js/layout.js"></script>

  <!-- style -->
  <!-- 완료시 index.css만 -->
  <link rel="stylesheet" href="../css/index.css"/>
  <link rel="stylesheet" href="../css/commons.css"/>
  <script type="text/javascript" src="../js/index.js"></script>
  <title>풀앳홈 | Terms</title>
</head>

<body>
<!-- global-nav -->
<header class="global-nav"></header>
<!-- /global-nav -->

<section class="container py-5">
  <h1 class="text-26 text-lg-42 text-xl-56 text-center font-weight-bold mt-md-5">
      ${post.title.textKoKr!}
  </h1>

  <div class="row justify-content-center">
    <div class="px-4 py-2 mt-4 mt-md-5 mb-5">
      <span class="badge badge-pill bg-sm-none badge-light-gray text-gray font-weight-normal py-1 px-0 px-lg-2 text-12 text-lg-13 text-xl-14">
                <#if post.type?has_content && post.type == "NOTICE">
                    ${post.type.value}사항
                <#elseif post.type?has_content>
                    ${post.type.value}
                <#else>-</#if>
      </span>
    </div>
    <div class="text-center mt-4 mt-md-5 mb-5">
      <p class="px-4 py-2 text-gray text-12 text-xl-14">
          <#if post.createdDate?has_content>
              ${post.createdDate.format('yyyy.MM.dd')}
          <#else>
            -
          </#if>
      </p>
    </div>
  </div>

  <div class="layout-main">
      ${post.content.textKoKr!}
  </div>

</section>

<!-- footer -->
<footer class="global-footer"></footer>
<!--/footer  -->
</body>
</html>
