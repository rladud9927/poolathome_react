<#import "/spring.ftl" as spring/>
<#assign pageparam = ""/>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <meta name="keywords" content=“풀앳홈, PAH, pool at home, 3PL, 설치배송, 설치, 배송, 가전설치, 가구설치, 가전배송, 가구배송, 중량물배송, 바로설치, 반품물류, AS, 이전설치, 유통가공, 리퍼비시" />
  <meta name="description" content=“풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A/S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
  <meta property="og:type" content="website"/>
  <meta property="og:title" content=“풀앳홈"/>
  <meta property="og:description" content="풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A/S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
  <meta property="og:image" content="/assets/image/about-main.png"/>
  <meta property="og:url" content="http://www.poolathome.co.kr"/>
  <meta name="naver-site-verification" content="08c6b592ad6098170ac111f8d469e4fb152c0643" />
  <meta name="google-site-verification" content="G0p9XQnbnVzY8nNsQIJurWBUvVHYf3RsHURZmBIOY78" />

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">

  <!-- bootstrap -->
  <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
          integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn"
          crossorigin="anonymous"
  />
  <script
          src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous"
  ></script>
  <script
          src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
          integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
          crossorigin="anonymous"
  ></script>
  <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"
          integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2"
          crossorigin="anonymous"
  ></script>

  <!--  -->
  <script type="text/javascript" src="../js/layout.js"></script>

  <!-- style -->
  <!-- 완료시 index.css만 -->
  <link rel="stylesheet" href="../css/index.css"/>
  <link rel="stylesheet" href="../css/commons.css"/>
  <script type="text/javascript" src="../js/index.js"></script>
  <title>풀앳홈 | Notice</title>
</head>
<body>
<!-- global-nav -->
<header class="global-nav"></header>
<!-- /global-nav -->

<section class="position-relative">
  <img
          src="../assets/image/notice-bg.png"
          alt=""
          class="w-100 d-none d-lg-block"
  />
  <div class="notice-main">
    <div class="container text-center">
      <h1 class="
              font-weight-bold
              text-26 text-lg-42 text-xl-56 text-white
              mb-3 mb-lg-4
            "
      >
        공지와 알림
      </h1>
      <span class="text-light text-14 text-lg-18 text-xl-22">
            풀앳홈의 뉴스 및 공지사항을 빠르게 확인해 보세요
          </span>
      <!-- form -->
      <div class="d-flex justify-content-center">
        <div class="
                d-flex
                align-items-center
                pill
                bg-white
                px-4 px-lg-5
                mt-4 mt-lg-5
                w-sm-100p w-lg-75p
                mx-3 mx-lg-0
              "
        >
          <form class="form-inline w-100 my-lg-2 my-xl-3">
            <input id="staticEmail2" name="query" type="text" class="form-control-plaintext"
                   placeholder="검색어를 입력해 주세요" value="${data.query!}">
          </form>
          <button id="submit-list-filter" type="submit" class="btn mt-1 px-0">
            <i class="icon icon-search icon-sm-14 icon-25 mr-n2 mr-lg-0"></i>
          </button>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- ---------- -->
<section class="py-4 py-lg-5">
  <div class="container">
    <!-- tabs -->
    <section
            class="
            form-check form-check-inline
            d-flex
            justify-content-center
            mt-3
            mb-5
          "
    >
      <div>
        <button class="pill bg-white border text-gray text-14 py-1 py-lg-2 w-sm-58 w-85 mx-1 mx-lg-2<#if springMacroRequestContext.getRequestUri() == "/notice"> tab-active-primary</#if>"
                onclick="location.href = '/notice'">
          <span>전체</span>
        </button>
          <#list types as type>
            <button class=" pill bg-white border text-gray text-14 py-1 py-lg-2 w-sm-58 w-85 mx-1 mx-lg-2<#if springMacroRequestContext.getRequestUri() == "/notice/" + type?lower_case> tab-active-primary</#if>"
                    onclick="location.href = '/notice/${type?lower_case}'">
              <span>${type.value}</span>
            </button>
          </#list>
      </div>
    </section>
    <!-- /tabs -->

    <div class="row">
        <#if data?has_content>
            <#list data.page.content as item>
              <div class="col-12 col-md-6 col-lg-4 mb-4 mb-lg-0 pb-4">
                <div class="border p-4 rounded-20">
                <span class="badge badge-pill bg-sm-none badge-light-gray text-gray font-weight-normal py-1 px-0 px-lg-2 text-12 text-lg-13 text-xl-14">
                <#if item.type?has_content && item.type == "NOTICE">
                    ${item.type.value}사항
                <#elseif item.type?has_content>
                    ${item.type.value}
                <#else>-</#if>
                </span>
                  <h3 class="text-22 text-lg-28 text-xl-32 mt-3 text-truncate">
                      <#if item.title?has_content>
                          <#if item.title.textKoKr?length < 18>
                              ${item.title.textKoKr}
                          <#else>
                              ${item.title.textKoKr[0..*18]}...
                          </#if>
                      <#else>
                        -
                      </#if>
                  </h3>

                  <span class="text-14 text-lg-16 text-xl-22 text-gray mt-4 multiline-ellipsis-4"
                        style="height: 130px">
<#--                      ${item.content.textKoKr?split("p>")[1]?split("</")[0]}-->
                      ${item.content.textKoKr!}
                  </span>

                  <button class=" btn btn-block border text-gray mt-4 text-lg-12 text-xl-14 py-2 text-center pill"
                          onclick="location.href= '/notice/${item.id?c}'">
                    자세히 보기
                  </button>
                </div>
              </div>
            </#list>
        </#if>
    </div>
  </div>
</section>

<!-- pagination -->
<section class="d-none d-md-flex justify-content-center mb-5">
  <nav aria-label="...">
    <ul class="pagination">
        <#-- 이전 섹션 페이징 -->
        <#if (data.beginIndex - data.limit) gt 0>
          <li class="page-item disabled align-self-center mt-1">
            <a href="<@spring.url "${((data.beginIndex - data.limit) gt 0)?then('?page=' + ((data.beginIndex - 1)-1)?c + ('&query=' + data.query + '&size=' + data.pageSize?c)  + pageparam ,'javascript:void(0);')}"/>">
              <i class="icon icon-arrow-prev"></i>
            </a>
          </li>
        </#if>
        <#-- Number 목록 -->
        <#list data.beginIndex..data.endIndex as i>
            <#if i <= data.pageCount>
              <li class="page-item<#if data.currentIndex == i> active</#if>">
                <a class="page-link text-14"
                   href="<@spring.url "${(i == data.currentIndex)?then('javascript:void(0)', '?page=' + (i - 1)?c + ('&query=' + data.query + '&size=' + data.pageSize?c)  + pageparam )}"/>">${i?c}
                </a>
              </li>
            </#if>
        </#list>
        <#-- 다음 섹션 페이징 -->
        <#if (data.beginIndex + data.limit) lte data.pageCount>
          <li class="page-item align-self-center">
            <a href="<@spring.url "${((data.beginIndex + data.limit) lte data.pageCount)?then('?page=' + ((data.beginIndex + data.limit)-1)?c + ('&query=' + data.query + '&size=' + data.pageSize?c) + pageparam,'javascript:void(0);')}"/>">
              <i class="icon icon-arrow-next"></i>
            </a>
          </li>
        </#if>
    </ul>
  </nav>
</section>

<!-- button -->
<div class="w-100 d-flex justify-content-center mb-5">
  <button class="btn d-lg-none">
    <i class="icon icon-more icon-22"></i>
  </button>
</div>

<!-- top button -->
<button
        class="top-button btn position-fixed d-lg-none"
        style="bottom: 1rem; right: 1rem"
>
  <i class="icon icon-top icon-40"></i>
</button>

<!-- footer -->
<footer class="global-footer"></footer>
<!--/footer  -->
</body>
</html>
