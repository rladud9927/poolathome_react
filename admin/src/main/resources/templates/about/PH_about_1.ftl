<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <meta name="keywords" content=“풀앳홈, PAH, pool at home, 3PL, 설치배송, 설치, 배송, 가전설치, 가구설치, 가전배송, 가구배송, 중량물배송, 바로설치, 반품물류,
        AS, 이전설치, 유통가공, 리퍼비시
  " />
  <meta name="description" content=“풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A
  /S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
  <meta property="og:type" content="website"/>
  <meta property="og:title" content=“풀앳홈"/>
  <meta property="og:description"
        content="풀앳홈은 포워딩 및 빌트인 전문설치서비스부터 A/S, 반품관리, 리퍼비시 서비스까지 제공하는 B2B2C 종합물류서비스 중개 플랫폼입니다."/>
  <meta property="og:image" content="/assets/image/about-main.png"/>
  <meta property="og:url" content="http://www.poolathome.co.kr"/>
  <meta name="naver-site-verification" content="08c6b592ad6098170ac111f8d469e4fb152c0643"/>
  <meta name="google-site-verification" content="G0p9XQnbnVzY8nNsQIJurWBUvVHYf3RsHURZmBIOY78"/>

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
  <link rel="manifest" href="/assets/images/site.webmanifest">

  <!-- bootstrap -->
  <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
          integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn"
          crossorigin="anonymous"
  />
  <script
          src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
          integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
          crossorigin="anonymous"
  ></script>
  <script
          src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
          integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
          crossorigin="anonymous"
  ></script>
  <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js"
          integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2"
          crossorigin="anonymous"
  ></script>

  <!-- slick -->
  <link rel="stylesheet" type="text/css" href="../slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="../slick/slick-theme.css"/>
  <script type="text/javascript" src="../slick/slick.min.js"></script>

  <!--  -->

  <script type="text/javascript" src="../js/slick.js"></script>
  <script type="text/javascript" src="../js/about.js"></script>
  <script type="text/javascript" src="../js/layout.js"></script>

  <!-- style -->
  <!-- 완료시 index.css만 -->
  <link rel="stylesheet" href="../css/index.css"/>
  <link rel="stylesheet" href="../css/commons.css"/>
  <script type="text/javascript" src="../js/index.js"></script>

  <title>풀앳홈 | Aabout</title>
</head>
<body id="#ph_about">
<!-- global-nav -->
<header class="global-nav"></header>
<!-- /global-nav -->

<section class="position-relative">
  <div
          class="bg-primary d-none d-md-block d-lg-none"
          style="height: 150px"
  ></div>
  <img
          src="../assets/image/about-main.png"
          alt=""
          class="w-100 d-none d-md-block"
  />
  <img
          src="../assets/image/about-main-bg.png"
          alt=""
          class="w-100 d-md-none"
  />

  <div class="container position-absolute row about-main">
    <div class="col-12 col-lg-8">
      <h1
              class="
              font-weight-bold
              text-white text-26 text-lg-42 text-xl-56
              mb-5
            "
      >
        B2B2C SCM<br/>
        Total Logistics Service
      </h1>

      <span class="text-white text-14 text-lg-16 text-xl-22 d-block">
            포워딩부터 빌트인 전문설치서비스까지, 더 나아가 AS, 반품관리 및
            리퍼비시 생산을 <br/>
            수행하여 전문기술과 전국적인 인프라 구축이 어려운 판매자와 유통사에
            B2B2C <br/>
            종합물류서비스를 제공합니다.
          </span>
      <span class="text-white text-14 text-lg-16 text-xl-22 d-block mt-3">
            또한 고객사는 판매와 마케팅에 집중할 수 있도록 최적화된 IT시스템과
            전국네트워크를 <br/>
            구축하여 조달에서 반품까지 토탈솔루션 서비스를 제공하고 있으며 이를
            토대로 화주와 <br/>
            소비자를 모두 만족시킬 수 있는 중개플랫폼 솔루션 개발에 주력하고
            있습니다.
          </span>
    </div>
  </div>
</section>

<!-- ------------------ -->
<section>
  <nav
          id="navbar-section"
          class="
          about-nav
          bg-white
          navbar navbar-light
          border-bottom
          p-0
          overflow-hidden
        "
  >
    <ul
            class="
            nav nav-pills
            tabs
            font-weight-bold
            py-0
            mx-md-auto
            overflow-hidden
          "
    >
      <div
              class="d-flex px-3 text-nowrap"
              style="overflow-x: scroll; overflow-y: hidden"
      >
        <li class="nav-item service-link px-3 px-lg-5">
          <a
                  class="
                  nav-link
                  fw-500
                  text-nowrap
                  py-3
                  text-16 text-lg-18 text-xl-22
                "
                  href="#service"
          >
            서비스 소개
          </a>
        </li>
        <li class="nav-item system-link px-3 px-lg-5">
          <a
                  class="
                  nav-link
                  fw-500
                  text-nowrap
                  py-3
                  text-16 text-lg-18 text-xl-22
                "
                  href="#system"
          >
            전산시스템 소개
          </a>
        </li>
        <li class="nav-item history-link px-3 px-lg-5">
          <a
                  class="
                  nav-link
                  fw-500
                  text-nowrap
                  py-3
                  text-16 text-lg-18 text-xl-22
                "
                  href="#history"
          >
            히스토리
          </a>
        </li>
        <li class="nav-item map-link px-3 px-lg-5">
          <a
                  class="
                  nav-link
                  fw-500
                  text-nowrap
                  py-3
                  text-16 text-lg-18 text-xl-22
                "
                  href="#map"
          >
            오시는 길
          </a>
        </li>
      </div>
    </ul>
  </nav>
  <div
          class="sticky-margin"
          data-spy="scroll"
          data-target="#navbar-section"
          data-offset="0"
  >
    <section id="service">
      <section class="text-center">
        <div class="container h-100 pb-5">
          <h2 class="font-weight-bold text-26 text-lg-42 text-xl-56 pt-7">
            고객사는 판매와 마케팅에 집중
          </h2>
          <span
                  class="
                  text-14 text-lg-16 text-xl-22 text-gray
                  d-block
                  mt-4 mt-lg-5
                  mb-lg-5
                "
          >
                풀앳홈은 40년 종합물류그룹 로지스올의 일원으로서 오랜
                물류경험과<br class="d-none d-lg-block"/>
                노하우를 바탕으로, 고객사가 판매와 마케팅에 집중할 수 있는 모든
                물류환경을 제공하겠습니다.
              </span>
          <img
                  src="../assets/image/service-introduce.png"
                  alt=""
                  class="w-100 d-none d-md-block"
          />
          <img
                  src="../assets/image/about-intro-mobile@3x.png"
                  alt=""
                  class="w-100 d-md-none"
          />
        </div>
      </section>

      <section class="service-section position-relative">
        <section class="bg-primary" style="padding: 3rem 0 7rem 0">
          <div class="container">
            <div class="row my-5">
              <div class="col-12 col-md-6">
                <h4
                        class="
                        font-weight-bold
                        text-center
                        text-lg-left
                        text-white
                        text-lg-32
                        text-xl-40
                        mb-3
                      "
                >
                  B2B2C IT 서비스 제공
                </h4>
                <span
                        class="
                        d-block
                        text-center
                        text-lg-left
                        text-white
                        text-14
                        text-lg-18
                        text-xl-24
                      "
                >
                      B2B2C SCM Total Logistics Service
                    </span>
              </div>
              <div
                      class="
                      col-12 col-md-6
                      mt-4 mt-md-0
                      d-flex
                      justify-content-center
                      d-lg-block
                    "
              >
                <ul class="list-unstyled">
                  <li
                          class="
                          text-white text-14 text-lg-18 text-xl-22
                          d-flex
                          alingn-items-center
                          py-1
                          align-centered
                        "
                  >
                    <i class="icon icon-check icon-sm-16 icon-22 mr-2"></i>
                    국내 판매를 위한 3PL HUB 센터 역할
                  </li>
                  <li
                          class="
                          text-white text-14 text-lg-18 text-xl-22
                          d-flex
                          alingn-items-center
                          py-1
                          align-centered
                        "
                  >
                    <i class="icon icon-check icon-sm-16 icon-22 mr-2"></i>
                    해외 생산 제품에 대한 보관 기능
                  </li>
                  <li
                          class="
                          text-white text-14 text-lg-18 text-xl-22
                          d-flex
                          alingn-items-center
                          py-1
                          align-centered
                        "
                  >
                    <i class="icon icon-check icon-sm-16 icon-22 mr-2"></i>
                    보관, 가공, 간선, 설치, 리퍼비시 외 부가산업
                  </li>
                  <li
                          class="
                          text-white text-14 text-lg-18 text-xl-22
                          d-flex
                          alingn-items-center
                          py-1
                          align-centered
                        "
                  >
                    <i class="icon icon-check icon-sm-16 icon-22 mr-2"></i>
                    CS, A/S 전산개발 등의 사업 제공
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>

        <!--  -->
        <div
                class="container py-5 px-0 px-md-3 mt-lg-n7"
                style="margin-top: -9rem"
        >
          <!-- slick -->
          <div id="provide-service" class="row service-slick">
            <!-- col 1 -->
            <div class="col-md-6 col-lg-3 mb-4">
              <div
                      class="
                      border
                      bg-white
                      h-100
                      py-4
                      p-lg-4
                      d-flex
                      flex-column
                      align-items-center
                      rounded-20
                    "
              >
                <div
                        class="
                        circle-sm-96 circle-120
                        bg-light-blue
                        mb-3
                        d-flex
                        align-items-center
                        justify-content-center
                      "
                >
                  <i class="icon icon-delivery icon-sm-40 icon-50"></i>
                </div>

                <h6 class="font-weight-bold text-22 mb-3">물류대행 사업</h6>

                <div class="short-bar my-2"></div>

                <ul class="list-unstyled text-center">
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    포워딩, 컨테이너 운송 사업
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    LogisALL 물류센터 재고 보관 사업
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    택배, 운송, 라벨링, 납품 대행 사업
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    포장, 밴딩, 소포장 등 유통가공 사업
                  </li>
                </ul>
              </div>
            </div>
            <!-- /col 1 -->

            <!-- col 2 -->
            <div class="col-md-6 col-lg-3 mb-4">
              <div
                      class="
                      border
                      bg-white
                      h-100
                      py-4
                      p-lg-4
                      d-flex
                      flex-column
                      align-items-center
                      rounded-20
                    "
              >
                <div
                        class="
                        circle-sm-96 circle-120
                        bg-light-blue
                        mb-3
                        d-flex
                        align-items-center
                        justify-content-center
                      "
                >
                  <i class="icon icon-moving icon-sm-40 icon-50"></i>
                </div>

                <h6 class="font-weight-bold text-22 mb-3">설치물류 사업</h6>

                <div class="short-bar my-2"></div>

                <ul class="list-unstyled text-center">
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    방문 배송 및 비대면 배송 사업
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    가전, 가구, 헬스용품 설치 배송 사업
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    TV, 벽걸이, 빌트인 가전 전문설치 사업
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    이전 설치, 반품 회수, 회수 물류 사업
                  </li>
                </ul>
              </div>
            </div>
            <!-- /col 2 -->

            <!-- col 3 -->
            <div class="col-md-6 col-lg-3 mb-4">
              <div
                      class="
                      border
                      bg-white
                      h-100
                      py-4
                      p-lg-4
                      d-flex
                      flex-column
                      align-items-center
                      rounded-20
                    "
              >
                <div
                        class="
                        circle-sm-96 circle-120
                        bg-light-blue
                        mb-3
                        d-flex
                        align-items-center
                        justify-content-center
                      "
                >
                  <i class="icon icon-as icon-sm-40 icon-50"></i>
                </div>

                <h6 class="font-weight-bold text-22 mb-3">A/S 사업</h6>

                <div class="short-bar my-2"></div>

                <ul class="list-unstyled text-center">
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    부품관리 전산시스템 운영
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    가전제품, 가구, 헬스용품 등 방문 수리
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    가전제품 케어서비스(수거 처리 서비스)
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    부속품 교체 및 정기방문 점검 서비스
                  </li>
                </ul>
              </div>
            </div>
            <!-- /col 3-->

            <!-- col 4 -->
            <div class="col-md-6 col-lg-3 mb-4">
              <div
                      class="
                      border
                      bg-white
                      h-100
                      py-4
                      p-lg-4
                      d-flex
                      flex-column
                      align-items-center
                      rounded-20
                    "
              >
                <div
                        class="
                        circle-sm-96 circle-120
                        bg-light-blue
                        mb-3
                        d-flex
                        align-items-center
                        justify-content-center
                      "
                >
                  <i class="icon icon-file icon-sm-40 icon-50"></i>
                </div>

                <h6 class="font-weight-bold text-22 mb-3">리퍼비시 사업</h6>

                <div class="short-bar my-2"></div>

                <ul class="list-unstyled text-center">
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    반품 재고 및 초기 불량 재고 양품화
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    가전제품, 런닝머신, 안마의자, 기타
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    시리얼기준 소요부품 및 이력관리
                  </li>
                  <li
                          class="
                          about-service-list
                          text-14 text-xl-18
                          mt-2
                          text-lg-nowrap
                        "
                  >
                    부속품 교체 및 정기방문 점검 서비스
                  </li>
                </ul>
              </div>
            </div>
            <!-- /col 4 -->
          </div>
        </div>
      </section>
    </section>
    <!--  -->
    <section id="system" class="bg-light-gray py-md-5">
      <div class="container px-0 py-5 py-md-0">
        <h1
                class="
                text-center
                font-weight-bold
                text-26 text-lg-42 text-xl-56
                mt-4 mt-md-n3
                my-lg-5
                mb-3
              "
        >
          전산시스템 소개
        </h1>

        <!-- slick -->
        <div id="system-intro" class="row py-4 system-slick">
          <!-- col 1 -->
          <div class="col-md-6 mt-4">
            <div class="rounded-20 bg-white p-4 h-100">
              <div
                      class="
                      d-flex
                      justify-content-between
                      align-items-start
                      mb-4
                      mx-n3 mx-md-0
                    "
              >
                <i class="icon icon-mobile icon-100"></i>
              </div>

              <h6 class="font-weight-bold text-22 mb-3">
                Mobile & APP 서비스
              </h6>
              <ul class="list-unstyled">
                <li class="text-14 text-xl-18 py-2">
                  WMS & Mobile System 인터페이스 솔루션 제공
                </li>
                <li class="text-14 text-xl-18 py-2">
                  바코드 & 전화번호 고객 정보 조회, 해피콜, 배송 일자, 메모
                  입력
                </li>
                <li class="text-14 text-xl-18 py-2">
                  배송완료 및 AS 처리 후 시리얼번호, 사진, 서명 후 Data
                  Sending 연동
                </li>
              </ul>
            </div>
          </div>

          <!-- /col 1 -->

          <!-- col 2 -->
          <div class="col-md-6 mt-4">
            <div class="rounded-20 bg-white p-4 h-100">
              <div
                      class="
                      d-flex
                      justify-content-between
                      align-items-start
                      mb-4
                      mx-n3 mx-md-0
                    "
              >
                <i class="icon icon-shipping icon-100"></i>
                <button
                        class="btn modal-button"
                        data-toggle="modal"
                        data-target="#deliveryModal"
                >
                  <i class="icon icon-plus icon-sm-32 icon-40"></i>
                </button>
              </div>

              <h6 class="font-weight-bold text-22 mb-3">
                배송 정보 관리 서비스
              </h6>
              <ul class="list-unstyled">
                <li class="text-14 text-xl-18 py-2">
                  고객관리 : 빌주, 고객정보, 주문번호, 판매처, 제품명
                </li>
                <li class="text-14 text-xl-18 py-2">
                  배송관리 : 배송예정일, 배송 기사, 완료 사진, 시리얼 번호
                  등
                </li>
                <li class="text-14 text-xl-18 py-2">
                  AS 및 CS 접수 및 이력 관리, AS 발주 관리
                </li>
              </ul>
            </div>
          </div>

          <!-- /col 2 -->

          <!-- col 3 -->
          <div class="col-md-6 mt-4">
            <div class="rounded-20 bg-white p-4 h-100">
              <div
                      class="
                      d-flex
                      justify-content-between
                      align-items-start
                      mb-4
                      mx-n3 mx-md-0
                    "
              >
                <i class="icon icon-managemaent icon-100"></i>
                <button
                        class="btn modal-button"
                        data-toggle="modal"
                        data-target="#ectModal"
                >
                  <i class="icon icon-plus icon-sm-32 icon-40"></i>
                </button>
              </div>

              <h6 class="font-weight-bold text-22 mb-3">
                기타 관리 서비스
              </h6>
              <ul class="list-unstyled">
                <li class="text-14 text-xl-18 py-2">
                  각종 통계관리 서비스 제공
                </li>
                <li class="text-14 text-xl-18 py-2">
                  배송 기일, 해피콜 유무, 품목별 재고 현황, 일 배송수량,
                  방문율
                </li>
                <li class="text-14 text-xl-18 py-2">
                  회계관리, 전화번호 매칭 시스템, 제품별 이력 추적, 택배
                  주문 등
                </li>
              </ul>
            </div>
          </div>
          <!-- /col 3 -->

          <!-- col 4 -->
          <div class="col-md-6 mt-4">
            <div class="rounded-20 bg-white p-4 h-100">
              <div
                      class="
                      d-flex
                      justify-content-between
                      align-items-start
                      mb-4
                      mx-n3 mx-md-0
                    "
              >
                <i class="icon icon-storage icon-100"></i>
                <button
                        class="btn modal-button"
                        data-toggle="modal"
                        data-target="#inventoryModal"
                >
                  <i class="icon icon-plus icon-sm-32 icon-40"></i>
                </button>
              </div>

              <h6 class="font-weight-bold text-22 mb-3">
                기타 관리 서비스
              </h6>
              <ul class="list-unstyled">
                <li class="text-14 text-xl-18 py-2">
                  각종 통계관리 서비스 제공
                </li>
                <li class="text-14 text-xl-18 py-2">
                  배송 기일, 해피콜 유무, 품목별 재고 현황, 일 배송수량,
                  방문율
                </li>
                <li class="text-14 text-xl-18 py-2">
                  회계관리, 전화번호 매칭 시스템, 제품별 이력 추적, 택배
                  주문 등
                </li>
              </ul>
            </div>
          </div>
          <!-- /col 4 -->
        </div>

        <!--modal for col-2 -->
        <div
                class="modal"
                id="deliveryModal"
                tabindex="-1"
                aria-labelledby="deliveryModalLabel"
                aria-hidden="true"
                style="background-color: rgba(0, 0, 0, 0.7)"
        >
          <button
                  type="button"
                  class="close text-white"
                  data-dismiss="modal"
                  aria-label="Close"
                  style="position: absolute; top: 1rem; right: 1rem"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content bg-transparent border-0">
              <div class="modal-body position-relative px-5">
                <!-- slick -->
                <!-- web -->
                <div class="modal-slick my-auto d-none d-md-block">
                  <div class="slide-el">
                    <img src="../assets/image/web-5@3x.png" alt=""/>
                  </div>
                  <div class="slide-el">
                    <img src="../assets/image/web-4@3x.png" alt=""/>
                  </div>
                </div>
                <!-- mobile -->
                <div class="modal-slick my-auto d-lg-none">
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-1@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-2@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-3@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-4@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-5@3x.png"
                            alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->

        <!--modal for col-3 -->
        <div
                class="modal"
                id="ectModal"
                tabindex="-1"
                aria-labelledby="ectModalLabel"
                aria-hidden="true"
                style="background-color: rgba(0, 0, 0, 0.7)"
        >
          <button
                  type="button"
                  class="close text-white"
                  data-dismiss="modal"
                  aria-label="Close"
                  style="position: absolute; top: 1rem; right: 1rem"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content bg-transparent border-0">
              <div class="modal-body position-relative px-5">
                <!-- slick -->
                <!-- web -->
                <div class="modal-slick my-auto d-none d-md-block">
                  <div class="slide-el">
                    <img src="../assets/image/web-2@3x.png" alt=""/>
                  </div>
                  <!-- <div class="slide-el">
                    <img src="../assets/image/web-1@3x.png" alt="" />
                  </div> -->
                  <div class="slide-el">
                    <img src="../assets/image/web-3@3x.png" alt=""/>
                  </div>
                </div>
                <!-- mobile -->
                <div class="modal-slick my-auto d-lg-none">
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-1@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-2@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-3@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-4@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-5@3x.png"
                            alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->

        <!--modal for col-4 -->
        <div
                class="modal"
                id="inventoryModal"
                tabindex="-1"
                aria-labelledby="inventoryModalLabel"
                aria-hidden="true"
                style="background-color: rgba(0, 0, 0, 0.7)"
        >
          <button
                  type="button"
                  class="close text-white"
                  data-dismiss="modal"
                  aria-label="Close"
                  style="position: absolute; top: 1rem; right: 1rem"
          >
            <span aria-hidden="true">&times;</span>
          </button>
          <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content bg-transparent border-0">
              <div class="modal-body position-relative px-5">
                <!-- slick -->
                <!-- web -->
                <div class="modal-slick my-auto d-none d-md-block">
                  <div class="slide-el">
                    <img src="../assets/image/web-7@3x.png" alt=""/>
                  </div>
                </div>
                <!-- mobile -->
                <div class="modal-slick my-auto d-lg-none">
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-2@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-1@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-3@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-4@3x.png"
                            alt=""
                    />
                  </div>
                  <div class="slide-mobile-el">
                    <img
                            src="../assets/image/modal-mobile-5@3x.png"
                            alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->
      </div>
    </section>
    <!--  -->
    <section id="history" class="py-5">
      <div class="container">
        <h1
                class="
                text-center
                font-weight-bold
                text-26 text-lg-42 text-xl-56
                mt-3
                mb-5
                my-lg-5
              "
        >
          히스토리
        </h1>
        <!-- row 2022-->
        <div class="history-area row mx-0 mb-3">
          <div
                  class="
                  col-3
                  font-weight-bold
                  text-primary text-22 text-lg-42 text-xl-56
                  px-0
                "
          >
            <div class="mr-3 mr-md-3 bold-border-primary">2021</div>
          </div>
          <div class="col-9 px-0">
            <div class="bold-border-gray">
              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3 border-bottom">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">10월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>용인물류센터 내 리퍼장 확대 및 취급품목 추가</span>
                    <br/>
                    <br/>
                    <span>전문 A/S전담팀 구축</span>
                    <br/>
                    <br/>
                    <span>악성 및 반품재고 솔류션 제공 (BOM, 리퍼작업, 판매 연계)</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->
              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3 border-bottom">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">08월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>취급품목 확대 (안마의자 및 조립가구)</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->
              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3 border-bottom">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">07월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>HUB물류센터 이전 (천안 → 용인)</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->
              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">01월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>가구제품 설치물류 사업 진행</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->
            </div>
          </div>
        </div>
        <!-- /row 2022 -->

        <!-- row 2020-->
        <div class="history-area row mx-0 mb-3">
          <div
                  class="
                  col-3
                  font-weight-bold
                  text-primary text-22 text-lg-42 text-xl-56
                  px-0
                "
          >
            <div class="mr-3 mr-md-3 bold-border-primary">2020</div>
          </div>
          <div class="col-9 px-0">
            <div class="bold-border-gray">
              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3 border-bottom">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">09월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>heavy & bulk 제품 특화서비스 제공, 반품물류 대행 서비스 시행</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->

              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">08월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>로지스올그룹 B2B2C 물류전문 신규법인 풀앳홈㈜ 설립</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->
            </div>
          </div>
        </div>
        <!-- /row 2020 -->

        <!-- row 2019-->
        <div class="history-area row mx-0 mb-3">
          <div
                  class="
                  col-3
                  font-weight-bold
                  text-primary text-22 text-lg-42 text-xl-56
                  px-0
                "
          >
            <div class="mr-3 mr-md-3 bold-border-primary">2019</div>
          </div>
          <div class="col-9 px-0">
            <div class="bold-border-gray">
              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3 border-bottom">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">10월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>생활가전 및 주방가전 전문 설치서비스</span>
                    <br/>
                    <br/>
                    <span>빌트인가전 (식기세척기, 인덕션)의 One-Stop 전문설치 서비스 구현</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->

              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3 border-bottom">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">06월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>A/S 전담 운영체계 구축 및 전산시스템 개발</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->

              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">04월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>해외 직구 가전제품 물류대행 서비스</span>
                    <br/>
                    <br/>
                    <span>제주 포함 전국배송 RDC인프라 구축</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->
            </div>
          </div>
        </div>
        <!-- /row 2019 -->

        <!-- row 2018-->
        <div class="history-area row mx-0 mb-3">
          <div
                  class="
                  col-3
                  font-weight-bold
                  text-primary text-22 text-lg-42 text-xl-56
                  px-0
                "
          >
            <div class="mr-3 mr-md-3 bold-border-primary">2018</div>
          </div>
          <div class="col-9 px-0">
            <div class="bold-border-gray">
              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3 border-bottom">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">11월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>리퍼비시 사업장 구축</span>
                    <br/>
                    <br/>
                    <span>부품관리 및 리퍼관리 전산시스템 개발</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->

              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3 border-bottom">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">07월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>가전 제품 및 헬스케어 용품 A/S 사업 진행</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->

              <!-- row month -->
              <div class="row mx-0 py-2 py-lg-3">
                <div
                        class="
                        col-2 col-lg-1
                        px-0
                        font-weight-bold
                        text-14 text-lg-18 text-xl-22
                      "
                >
                  <span class="d-block px-md-2">03월</span>
                </div>
                <div class="col-10 col-lg-11 px-0">
                  <div class="text-14 xl-18">
                    <span>한국파렛트풀㈜ 가전 설치물류 사업 진행</span>
                    <br/>
                    <br/>
                    <span>HUB물류센터 개소 (천안)</span>
                    <br/>
                    <br/>
                    <span>자사 배송 전산시스템 개발</span>
                  </div>
                </div>
              </div>
              <!-- /row month -->
            </div>
          </div>
        </div>
        <!-- /row 2018 -->
      </div>
    </section>
    <!--  -->
    <section id="map" class="py-5">
      <div class="container">
        <h1
                class="
                text-center
                font-weight-bold
                text-26 text-lg-42 text-xl-56
                mt-4
                mb-5
                my-lg-5
              "
        >
          오시는 길
        </h1>

        <div class="row my-5 mx-n2">
          <div class="col-6 col-lg-7 px-2">
            <div id="map00" class="rounded-20 h-lg-400 h-md-336">
            </div>
          </div>
          <div class="col-6 col-lg-5 px-2">
            <div
                    class="rounded-20 bg-gray pb-sm-100 h-lg-400"
                    style="
                    background: center / 100% no-repeat
                      url('../assets/image/map-pic1@3x.png');
                  "
            ></div>
          </div>

          <section
                  class="
                  d-lg-flex
                  justify-content-between
                  align-items-start
                  w-100
                  px-2 px-lg-4
                  mt-3 mt-md-4
                "
          >
            <div>
              <h6 class="text-14 text-lg-18 text-xl-26 font-weight-bold">
                용인 HUB물류센터
              </h6>

              <span
                      class="
                      text-gray text-14 text-lg-18 text-xl-22
                      d-block
                      mt-2 mt-lg-3
                      d-flex
                      align-items-center
                    "
              >
                    <i class="icon icon-call icon-sm-12 icon-15 mr-2"></i>
                    <span>1833-4184</span>
                  </span>
              <span
                      class="
                      text-gray text-14 text-lg-18 text-xl-22
                      d-block
                      mt-2 mt-lg-3
                      d-flex
                      align-items-center
                    "
              >
                    <i class="icon icon-pin icon-sm-12 icon-15 mr-2"></i>
                    <span>경기 용인시 처인구 백암면 고안로51번길 151</span>
                  </span>
            </div>
            <div class="mt-3 mt-lg-0">
              <button
                      onclick="window.open('https://map.kakao.com/link/map/용인HUB물류센터,37.1446195170188, 127.411218586201')"
                      class="
                      btn btn-block
                      pill
                      py-2
                      px-4
                      btn-outline-gray
                      font-weight-bold
                      text-primary text-12 text-lg-13 text-xl-14
                    "
              >
                카카오지도 바로가기
              </button>
            </div>
          </section>
        </div>

        <!--  -->
        <div class="row my-5 mx-n2">
          <div class="col-6 col-lg-7 px-2">
            <div id="map01" class="rounded-20 h-lg-400 h-md-336">
            </div>
          </div>
          <div class="col-6 col-lg-5 px-2">
            <div
                    class="rounded-20 bg-gray pb-sm-100 h-lg-400"
                    style="
                    background: center / 100% no-repeat
                      url('../assets/image/map-pic2@3x.png');
                  "
            ></div>
          </div>

          <section
                  class="
                  d-lg-flex
                  justify-content-between
                  align-items-start
                  w-100
                  px-2 px-lg-4
                  mt-3 mt-md-4
                "
          >
            <div>
              <h6 class="text-14 text-lg-18 text-xl-26 font-weight-bold">
                본사
              </h6>

              <span
                      class="
                      text-gray text-14 text-lg-18 text-xl-22
                      d-block
                      mt-2 mt-lg-3
                      d-flex
                      align-items-center
                    "
              >
                    <i class="icon icon-call icon-sm-12 icon-15 mr-2"></i>
                    <span>02-3669-7733</span>
                  </span>
              <span
                      class="
                      text-gray text-14 text-lg-18 text-xl-22
                      d-block
                      mt-2 mt-lg-3
                      d-flex
                      align-items-center
                    "
              >
                    <i class="icon icon-pin icon-sm-12 icon-15 mr-2"></i>
                    <span>서울마포구 마포대로 63-8 삼창프라자빌딩 7층</span>
                  </span>
            </div>
            <div class="mt-3 mt-lg-0">
              <button
                      onclick="window.open('https://map.kakao.com/link/map/본사,37.5422597095372, 126.947815454166')"
                      class="
                      btn btn-block
                      pill
                      py-2
                      px-4
                      btn-outline-gray
                      font-weight-bold
                      text-primary text-12 text-lg-13 text-xl-14
                    "
              >
                카카오지도 바로가기
              </button>
            </div>
          </section>
        </div>

        <!-- Kakao Map API -->
        <script type="text/javascript"
                src="https://dapi.kakao.com/v2/maps/sdk.js?appkey=39de43dfb16cfdc96fa88f1d0af520a4&libraries=services"></script>
        <script>
          const map00Container = document.getElementById('map00');
          const map01Container = document.getElementById('map01');

          const map00Options = {
            center: new kakao.maps.LatLng(37.1446195170188, 127.411218586201),
            level: 2
          };
          const map01Options = {
            center: new kakao.maps.LatLng(37.5422597095372, 126.947815454166),
            level: 2
          };

          let map00 = new kakao.maps.Map(map00Container, map00Options);
          let map01 = new kakao.maps.Map(map01Container, map01Options);

          const marker00Position = new kakao.maps.LatLng(37.1446195170188, 127.411218586201);
          const marker01Position = new kakao.maps.LatLng(37.5422597095372, 126.947815454166);

          const marker00 = new kakao.maps.Marker({
            position: marker00Position
          });
          const marker01 = new kakao.maps.Marker({
            position: marker01Position
          });

          const infoWindow00 = new kakao.maps.InfoWindow({
            content: '<div style="width:200px;text-align:center;padding:6px 0;">경기 용인시 처인구 백암면 고안로51번길 151</div>'
          });
          const infoWindow01 = new kakao.maps.InfoWindow({
            content: '<div style="width:200px;text-align:center;padding:6px 0;">서울마포구 마포대로 63-8 삼창프라자빌딩 7층</div>'
          });

          infoWindow00.open(map00, marker00);
          infoWindow01.open(map01, marker01);

          marker00.setMap(map00);
          marker01.setMap(map01);

          $(window).resize(function () {
            map00 = new kakao.maps.Map(map00Container, map00Options);
            map01 = new kakao.maps.Map(map01Container, map01Options);

            infoWindow00.open(map00, marker00);
            infoWindow01.open(map01, marker01);

            marker00.setMap(map00);
            marker01.setMap(map01);
          });
        </script>

      </div>
    </section>
  </div>
</section>

<!-- top button -->
<button
        class="top-button btn position-fixed d-lg-none"
        style="bottom: 1rem; right: 1rem"
>
  <i class="icon icon-top icon-40"></i>
</button>

<!-- footer -->
<footer class="global-footer"></footer>
<!--/footer  -->
<style>
    .slide-el {
        padding: 3px;
    }
</style>
</body>
</html>
